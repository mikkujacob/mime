﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EpisodicMemory
{
    class TemporalMemory
    {
        internal class Node
        {
            public Guid id { get; }
            public int temporalIndex { get; }
            public Node prev { get; set; }
            public Node next { get; set; }

            public Node(Guid id, int temporalIndex)
            {
                this.id = id;
                this.temporalIndex = temporalIndex;
                this.prev = null;
                this.next = null;
            }
        }

        internal Node head;
        internal Node last;

        public void InsertAtEnd(Guid id)
        {
            int temporalIndex = last.temporalIndex + 1;
            Insert(id, temporalIndex);
        }

        public void Insert(Guid id, int temporalIndex)
        {
            Node newNode = new Node(id, temporalIndex);
            newNode.next = null;
            if(head == null)
            {
                head = newNode;
                last = newNode;
                head.prev = null;
                return;
            }

            if(head.temporalIndex > temporalIndex)
            {
                newNode.prev = null;
                head.prev = newNode;
                newNode.next = head;
                head = newNode;
                return;
            }

            if(last.temporalIndex < temporalIndex)
            {
                newNode.prev = last;
                last.next = newNode;
                last = newNode;
                return;
            }

            Node temp = head.next;
            while(temp.temporalIndex < temporalIndex)
            {
                temp = temp.next;
            }

            temp.prev.next = newNode;
            newNode.prev = temp.prev;
            temp.prev = newNode;
            newNode.next = temp;
        }

        public Node GetNode(int temporalIndex)
        {
            Node temp = head;
            while (temp.next != null && temp.temporalIndex < temporalIndex)
            {
                temp = temp.next;
            }
            return temp;
        }

        public int getTemporalIndex(Guid id)
        {
            Node temp = head;
            while (temp.next != null && temp.id != id)
            {
                temp = temp.next;
            }
            return temp.temporalIndex;
        }

        public Node GetKthNodePrev(int temporalIndex, int k)
        {
            Node temp = GetNode(temporalIndex);
            int i = 0;
            while (temp.next != null && i < k)
            {
                temp = temp.next;
                i += 1;
            }
            return temp;
        }

        public Node GetKthNodeNext(int temporalIndex, int k)
        {
            Node temp = GetNode(temporalIndex);
            int i = 0;
            while (temp.prev != null && i < k)
            {
                temp = temp.prev;
                i += 1;
            }
            return temp;
        }
    }
}
