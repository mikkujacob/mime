﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FixSpacialUIBugTemporary : MonoBehaviour {

	// Use this for initialization
	void Start () {
		GameObject head = GameObject.Find ("Head");
		Camera hmd = head.GetComponent<Camera> ();
		hmd.clearFlags = CameraClearFlags.Skybox;
		hmd.cullingMask = 1 << 0 | 1 << 1 | 1 << 2 | 1 << 4 | 1 << 5 | 1 << 8 | 1 << 9;
		hmd.stereoTargetEye = StereoTargetEyeMask.Both;
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
