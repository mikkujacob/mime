﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConcurrentPriorityQueue;

public partial class RTree<T> : ISpatialDatabase<T>, ISpatialIndex<T> where T : ISpatialData
{
    private const int DefaultMaxEntries = 9;
    private const int MinimumMaxEntries = 4;
    private const int MinimumMinEntries = 2;
    private const double DefaultFillFactor = 0.4;

    private int maxEntries;
    private int minEntries;
    internal Node root;

    public RTree() : this(DefaultMaxEntries) { }
    public RTree(int maxEntries): this(maxEntries, EqualityComparer<T>.Default) { }
    public RTree(int maxEntries, EqualityComparer<T> comparer)
    {
        this.maxEntries = Math.Max(MinimumMaxEntries, maxEntries);
        this.minEntries = Math.Max(MinimumMinEntries, (int)Math.Ceiling(this.maxEntries * DefaultFillFactor));

        this.Clear();
    }

    public int Count { get; private set; }

    public void Clear()
    {
        this.root = new Node(new List<ISpatialData>(), 1);
        this.Count = 0;
    }

    public IEnumerable<T> Search() => GetAllChildren(this.root).ToList();

    public IEnumerable<T> Search(Envelope boundingBox)
    {
        //UnityEngine.Debug.Log("Searching...");
        return DoSearch(boundingBox).Select(x => (T)x.Peek()).ToList();
    }

    public void Insert(T item)
    {
        Insert(item, this.root.Height);
        this.Count++;
    }

    public void BulkLoad(IEnumerable<T> items)
    {
        var data = items.Cast<ISpatialData>().ToList();
        if (data.Count == 0) return;

        if (this.root.IsLeaf &&
            this.root.Children.Count + data.Count < maxEntries)
        {
            foreach (var i in data)
                Insert((T)i);
            return;
        }

        if (data.Count < this.minEntries)
        {
            foreach (var i in data)
                Insert((T)i);
            return;
        }

        var dataRoot = BuildTree(data);
        this.Count += data.Count;

        if (this.root.Children.Count == 0)
            this.root = dataRoot;
        else if (this.root.Height == dataRoot.Height)
        {
            if (this.root.Children.Count + dataRoot.Children.Count <= this.maxEntries)
            {
                foreach (var isd in dataRoot.Children)
                    this.root.Add(dataRoot);
            }
            else
                SplitRoot(dataRoot);
        }
        else
        {
            if (this.root.Height < dataRoot.Height)
            {
                var tmp = this.root;
                this.root = dataRoot;
                dataRoot = tmp;
            }

            this.Insert(dataRoot, this.root.Height - dataRoot.Height);
        }
    }

    public void Delete(T item)
    {
        var candidates = DoSearch(item.Envelope);

        foreach (var c in candidates.Where(c => object.Equals(item, c.Peek())))
        {
            c.Pop();
            if(c.Count > 0)
            {
                (c.Peek() as Node).Children.Remove(item);
            }
            while (c.Count > 0)
            {
                (c.Peek() as Node).ResetEnvelope();
                c.Pop();
            }
        }
    }

    public ISpatialData NearestNeighbor(ISpatialData point)
    {
        var pQueue = new ConcurrentPriorityQueue<ISpatialData, double>();
        pQueue.Enqueue(this.root, 1 / (0.0001 + Envelope.boxDistance(point.Envelope, root.Envelope)));
        ISpatialData nearestNeighbor = root.Children[0];

        do
        {
            var current = pQueue.Dequeue() as Node;
            if (current.IsLeaf)
            {
                nearestNeighbor = current.Children[0];
                for (int i = 0; i < current.Children.Count; i++)
                {
                    if (Envelope.boxDistance(point.Envelope, current.Children[i].Envelope) < Envelope.boxDistance(point.Envelope, nearestNeighbor.Envelope))
                    {
                        nearestNeighbor = current.Children[i];
                    }
                }
                return nearestNeighbor;
            }
            else
            {
                for (int i = 0; i < current.Children.Count; i++)
                {
                    pQueue.Enqueue(current.Children[i], 1 / (0.0001 + Envelope.boxDistance(point.Envelope, current.Children[i].Envelope)));
                }
            }
        } while (pQueue.Count != 0);
        return nearestNeighbor;
    }

    public List<ISpatialData> KNearestNeighbor(ISpatialData point, int k)
    {
        var pQueue = new ConcurrentPriorityQueue<ISpatialData, double>();
        pQueue.Enqueue(this.root, 1 / (0.0001 + Envelope.boxDistance(point.Envelope, root.Envelope)));
        List<ISpatialData> nearestNeighbors = new List<ISpatialData>();

        do
        {
            ISpatialData curr = pQueue.Dequeue();
            if (curr.GetType() == typeof(Node))
            {
                Node current = (Node) curr;
                for (int i = 0; i < current.Children.Count; i++)
                {
                    pQueue.Enqueue(current.Children[i], 1 / (0.0001 + Envelope.boxDistance(point.Envelope, current.Children[i].Envelope)));
                }
            }
            else
            {
                nearestNeighbors.Add(curr);
            }
                
        } while (pQueue.Count != 0 && nearestNeighbors.Count < k);
        return nearestNeighbors;
    }

    //public string KNNLabel(ISpatialData point, int k, bool weightedDistance = false)
    //{
    //    List<ISpatialData> nearestNeighbors = KNearestNeighbor(point, k);
    //    string answer = "";
    //    if(weightedDistance == true)
    //    {
    //        var map = new Dictionary<string, double>();
    //        for (int i = 0; i < nearestNeighbors.Count; i++)
    //        {
    //            if(map.ContainsKey(nearestNeighbors[i].label))
    //            {
    //                map[nearestNeighbors[i].label] += 1 / (0.00000001 + Envelope.boxDistance(point.Envelope, nearestNeighbors[i].Envelope));
    //            }
    //            else
    //            {
    //                map.Add(nearestNeighbors[i].label, 1 / (0.00000001 + Envelope.boxDistance(point.Envelope, nearestNeighbors[i].Envelope)));
    //            }
    //            var mapList = map.ToList();
    //            mapList.Sort((pair1, pair2) => pair1.Value.CompareTo(pair2.Value));
    //            answer = mapList[0].Key;
    //        }
    //    }
    //    else
    //    {
    //        List<string> labels = new List<string>();
    //        for (int i = 0; i < nearestNeighbors.Count; i++)
    //        {
    //            labels.Add(nearestNeighbors[i].label);
    //        }
    //        var labelGroup = labels.GroupBy(x => x);
    //        var maxCount = labelGroup.Max(g => g.Count());
    //        var label = labelGroup.Where(x => x.Count() == maxCount).Select(x => x.Key).ToArray();
    //        answer = label[0];
    //    }
    //    return answer;
    //}
}
