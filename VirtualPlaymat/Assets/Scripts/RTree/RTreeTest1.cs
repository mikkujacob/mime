﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
#if UNITY_WSA
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Assets.Scripts.RTree
{

    [TestClass]
    public class RTreeTest1
    {
        private class Point : ISpatialData, IComparable<Point>
        {
            public Envelope Envelope { get; set; }

            public string label { get; set; }

            public int CompareTo(Point other)
            {
                if (this.Envelope.MinX != other.Envelope.MinX)
                    return this.Envelope.MinX.CompareTo(other.Envelope.MinX);
                if (this.Envelope.MinY != other.Envelope.MinY)
                    return this.Envelope.MinY.CompareTo(other.Envelope.MinY);
                if (this.Envelope.MaxX != other.Envelope.MaxX)
                    return this.Envelope.MaxX.CompareTo(other.Envelope.MaxX);
                if (this.Envelope.MaxY != other.Envelope.MaxY)
                    return this.Envelope.MaxY.CompareTo(other.Envelope.MaxY);
                return 0;
            }
        }

        static double[,] data =
        {
            {0, 0, 0, 0},{10, 10, 10, 10},{20, 20, 20, 20},{25, 0, 25, 0},{35, 10, 35, 10},{45, 20, 45, 20},{0, 25, 0, 25},{10, 35, 10, 35},
            {20, 45, 20, 45},{25, 25, 25, 25},{35, 35, 35, 35},{45, 45, 45, 45},{50, 0, 50, 0},{60, 10, 60, 10},{70, 20, 70, 20},{75, 0, 75, 0},
            {85, 10, 85, 10},{95, 20, 95, 20},{50, 25, 50, 25},{60, 35, 60, 35},{70, 45, 70, 45},{75, 25, 75, 25},{85, 35, 85, 35},{95, 45, 95, 45},
            {0, 50, 0, 50},{10, 60, 10, 60},{20, 70, 20, 70},{25, 50, 25, 50},{35, 60, 35, 60},{45, 70, 45, 70},{0, 75, 0, 75},{10, 85, 10, 85},
            {20, 95, 20, 95},{25, 75, 25, 75},{35, 85, 35, 85},{45, 95, 45, 95},{50, 50, 50, 50},{60, 60, 60, 60},{70, 70, 70, 70},{75, 50, 75, 50},
            {85, 60, 85, 60},{95, 70, 95, 70},{50, 75, 50, 75},{60, 85, 60, 85},{70, 95, 70, 95},{75, 75, 75, 75},{85, 85, 85, 85},{95, 95, 95, 95}
        };

        static Point[] points =
            Enumerable.Range(0, data.GetLength(0))
                .Select(i => new Point
                {
                    Envelope = new Envelope
                    {
                        MinX = data[i, 0],
                        MinY = data[i, 1],
                        MaxX = data[i, 2],
                        MaxY = data[i, 3],
                    },
                })
                .ToArray();

        private List<Point> GetPoints(int cnt) =>
            Enumerable.Range(0, cnt)
                .Select(i => new Point
                {
                    Envelope = new Envelope
                    {
                        MinX = i,
                        MinY = i,
                        MaxX = i,
                        MaxY = i,
                    },
                })
                .ToList();

        [TestMethod]
        public void InsertTestData()
        {
            var tree = new RTree<Point>();
            foreach (var p in points)
                tree.Insert(p);

            Assert.AreEqual(points.Length, tree.Count);
            Assert.IsTrue(points.OrderBy(x => x).SequenceEqual(tree.Search().OrderBy(x => x)));
        }

        [TestMethod]
        public void BulkLoadTestData()
        {
            var tree = new RTree<Point>();
            tree.BulkLoad(points);

            Assert.AreEqual(points.Length, tree.Count);
            Assert.IsTrue(points.OrderBy(x => x).ToList().SequenceEqual(tree.Search().OrderBy(x => x).ToList()));
        }

        [TestMethod]
        public void BulkLoadSplitsTreeProperly()
        {
            var tree = new RTree<Point>(maxEntries: 4);
            tree.BulkLoad(points);
            tree.BulkLoad(points);

            Assert.AreEqual(points.Length * 2, tree.Count);
        }

        [TestMethod]
        public void BulkLoadMergesTreesProperly()
        {
            var smaller = GetPoints(10);
            var tree1 = new RTree<Point>(maxEntries: 4);
            tree1.BulkLoad(smaller);
            tree1.BulkLoad(points);

            var tree2 = new RTree<Point>(maxEntries: 4);
            tree2.BulkLoad(points);
            tree2.BulkLoad(smaller);

            Assert.IsTrue(tree1.Count == tree2.Count);

            var allPoints = points.Concat(smaller).OrderBy(x => x).ToList();
            var list1 = tree1.Search().OrderBy(x => x).ToList();
            var list2 = tree1.Search().OrderBy(x => x).ToList();

            bool equal = true;
            for (int i = 0; i < allPoints.Count; i++)
            {
                if (allPoints[i] != list1[i] || allPoints[i] != list2[i])
                    equal = false;
            }

            Assert.IsTrue(equal);
        }

        [TestMethod]
        public void SearchWorks()
        {
            var tree = new RTree<Point>();
            tree.BulkLoad(points);
            var searchEnvelope = new Envelope { MinX = 40, MinY = 20, MaxX = 80, MaxY = 70 };
        }

        [TestMethod]
        public void SearchReturnsMatchingResults()
        {
            var tree = new RTree<Point>(maxEntries: 4);
            tree.BulkLoad(points);

            var searchEnvelope = new Envelope { MinX = 40, MinY = 20, MaxX = 80, MaxY = 70 };
            var shouldFindPoints = points
                .Where(p => p.Envelope.Intersects(searchEnvelope))
                .OrderBy(x => x)
                .ToList();
            var foundPoints = tree.Search(searchEnvelope)
                .OrderBy(x => x)
                .ToList();

            Assert.IsTrue(shouldFindPoints.SequenceEqual(foundPoints));
        }

        //[TestMethod]
        //public void BasicRemoveTest()
        //{
        //    var tree = new RTree<Point>(maxEntries: 4);
        //    tree.BulkLoad(points);

        //    var len = points.Length;

        //    tree.Delete(points[0]);
        //    tree.Delete(points[1]);
        //    tree.Delete(points[2]);

        //    tree.Delete(points[len - 1]);
        //    tree.Delete(points[len - 2]);
        //    tree.Delete(points[len - 3]);

        //    var shouldFindPoints = points
        //        .Skip(3).Take(len - 6)
        //        .OrderBy(x => x)
        //        .ToList();
        //    var foundPoints = tree.Search()
        //        .OrderBy(x => x)
        //        .ToList();

        //    bool equal = true;

        //    for (int i = 0; i < shouldFindPoints.Count; i++)
        //    {
        //        if (shouldFindPoints[i] != foundPoints[i]) equal = false;
        //    }

        //    Assert.IsTrue(equal);
        //}

        [TestMethod]
        public void NonExistentItemCanBeDeleted()
        {
            var tree = new RTree<Point>(maxEntries: 4);
            tree.BulkLoad(points);

            tree.Delete(new Point
            {
                Envelope = new Envelope
                {
                    MinX = 13,
                    MinY = 13,
                    MaxX = 13,
                    MaxY = 13,
                },
            });
        }

        [TestMethod]
        public void ClearWorks()
        {
            var tree = new RTree<Point>(maxEntries: 4);
            tree.BulkLoad(points);
            tree.Clear();

            Assert.AreEqual(0, tree.Count);
        }

        [TestMethod]
        public void NearestNeighborWorks()
        {
            var tree = new RTree<Point>(maxEntries: 4);
            tree.BulkLoad(points);
            var nearestPoint = tree.KNearestNeighbor(points[0], 2);

            Assert.AreEqual(nearestPoint[1].Envelope, points[1].Envelope);
        }

        [TestMethod]
        public void KNearestNeighborWorks()
        {
            var tree = new RTree<Point>(maxEntries: 4);
            tree.BulkLoad(points);
            var nearestPoints = tree.KNearestNeighbor(points[0], 4);

            Assert.AreEqual(nearestPoints[0].Envelope, points[0].Envelope);
            Assert.AreEqual(nearestPoints[1].Envelope, points[1].Envelope);
            Assert.AreEqual(nearestPoints[2].Envelope, points[3].Envelope);
            Assert.AreEqual(nearestPoints[3].Envelope, points[6].Envelope);
        }
    }
}
#endif