﻿using System.Collections.Generic;


public interface ISpatialIndex<T>
{
	IEnumerable<T> Search();
	IEnumerable<T> Search(Envelope boundingBox);
}