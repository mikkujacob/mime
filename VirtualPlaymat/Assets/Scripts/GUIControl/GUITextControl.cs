﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent (typeof(Text))]
public class GUITextControl : MonoBehaviour
{
	public string text;
	Image panelImage;
	Text textMessage;
	Dictionary<string, DateTime> messageTimes;
	List<string> messageList;
	public bool toggleHiding = true;

	// Use this for initialization
	void Start ()
	{
		text = "";
		textMessage = gameObject.GetComponent<Text> ();
		panelImage = gameObject.transform.parent.gameObject.GetComponent<Image> ();
		messageTimes = new Dictionary<string, DateTime> ();
		messageList = new List<string> ();
	}
	
	// Update is called once per frame
	void Update ()
	{
		expireMessages ();
		text = parseMessageList ();
		if (toggleHiding)
		{
			if (text == "" && IsActive ())
			{
				Deactivate ();
			}
			else if (text != "" && !IsActive ())
			{
				Activate ();
			}
		}

		textMessage.text = text;
	}

	void expireMessages()
	{
		for (int i = 0; i < messageList.Count;)
		{
			if (messageTimes [messageList [i]].CompareTo (System.DateTime.Now) <= 0)
			{
				messageTimes.Remove (messageList [i]);
				messageList.RemoveAt (i);
			}
			else
			{
				i++;
			}
		}
	}

	string parseMessageList()
	{
		string text = "";
		foreach (string message in messageList)
		{
			text += "+" + message + "\n";
		}
		return text;
	}

	void Activate()
	{
		panelImage.enabled = true;
		textMessage.enabled = true;
	}

	void Deactivate()
	{
		textMessage.enabled = false;
		panelImage.enabled = false;
	}

	bool IsActive()
	{
		return textMessage.enabled && panelImage.enabled;
	}

	public void SetTimedMessage(string message, int time)
	{
//		text = message;
//		Invoke ("ClearText", time);

		if (!messageTimes.ContainsKey (message))
		{
			messageList.Add (message);
		}
		messageTimes [message] = System.DateTime.Now.AddSeconds (time);
	}

	void ClearText()
	{
		text = "";
	}
}
