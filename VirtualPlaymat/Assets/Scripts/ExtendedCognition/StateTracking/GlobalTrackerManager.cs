using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlobalTrackerManager : MonoBehaviour
{
	public List<ObjectStateTracker> objectStateTrackers;
	//private List<KinectStateTracker> kinectStateTrackers;
	//	private MImEGENIIOSCBridge oscBridge;
	public Dictionary<string,string> idToName;

	public bool hasActionStarted;

	// Use this for initialization
	public void Start ()
	{
		objectStateTrackers = new List<ObjectStateTracker> ();
		//kinectStateTrackers = new List<KinectStateTracker>();
		idToName = new Dictionary<string, string> ();
		GameObject[] gameObjects = GameObject.FindGameObjectsWithTag ("ObjectStateTracker");

		foreach (GameObject gameObject in gameObjects)
		{
			if (gameObject.GetComponent<ObjectStateTracker> () == null)
			{
				gameObject.AddComponent<ObjectStateTracker> ();
			}
			gameObject.GetComponent<ObjectStateTracker> ().setGlobalTrackerManager (this);
			objectStateTrackers.Add (gameObject.GetComponent<ObjectStateTracker> ());
			gameObject.layer = LayerMask.NameToLayer ("ObjectStateTracker");
			gameObject.GetComponent<ObjectStateTracker> ().initializeObjectStateDefinition ();
			idToName.Add (gameObject.GetComponent<ObjectStateTracker> ().state.id, gameObject.name);
		}


		/*gameObjects = GameObject.FindGameObjectsWithTag("KinectStateTracker");
		foreach(GameObject gameObject in gameObjects)
		{
			gameObject.AddComponent<KinectStateTracker>();
			kinectStateTrackers.Add(gameObject.GetComponent<KinectStateTracker>());
		}*/

//		oscBridge = gameObject.GetComponent<MImEGENIIOSCBridge>();

		hasActionStarted = false;

		//StartAction();
	}

	// Update is called once per frame
	public void Update ()
	{

	}

	public ObjectStateTracker GetObjectStateTrackerFromObjectID (string id)
	{
		foreach (ObjectStateTracker tracker in objectStateTrackers)
		{
			if (tracker.state.id == id)
			{
				return tracker;
			}
		}

		return null;
	}

	public void StartAction ()
	{
//		oscBridge.SendMessage("StartAction.Start", "StartAction");
		foreach (ObjectStateTracker tracker in objectStateTrackers)
		{
			tracker.sendStateJSON ();
		}
//		oscBridge.SendMessage("StartAction.End", "StartAction");

		hasActionStarted = true;

		/*foreach(KinectStateTracker tracker in kinectStateTrackers)
		{
			tracker.Activate();
		}*/
	}

	public void EndAction ()
	{
		/*foreach(KinectStateTracker tracker in kinectStateTrackers)
		{
			tracker.Deactivate();
		}*/

		hasActionStarted = false;

//		oscBridge.SendMessage("EndAction.Start", "EndAction");
		foreach (ObjectStateTracker tracker in objectStateTrackers)
		{
			tracker.sendStateJSON ();
			if (tracker.IsActive ())
			{
				tracker.Deactivate ();
			}
		}
//		oscBridge.SendMessage("EndAction.End", "EndAction");
	}
}
