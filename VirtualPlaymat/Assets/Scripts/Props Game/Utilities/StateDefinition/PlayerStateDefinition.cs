﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PlayerStateDefinition
{
	public string timestamp;

	public Vector3Definition headPosition;
	public Vector4Definition headRotation;

	public Vector3Definition leftHandPosition;
	public Vector4Definition leftHandRotation;

	public Vector3Definition rightHandPosition;
	public Vector4Definition rightHandRotation;

	public Vector3Definition objectPosition;
	public Vector4Definition objectRotation;

	public bool leftControllerGripButtonDown;
	public bool leftControllerTriggerButtonDown;

	public bool rightControllerGripButtonDown;
	public bool rightControllerTriggerButtonDown;

	public bool leftControllerGripButtonUp;
	public bool leftControllerTriggerButtonUp;

	public bool rightControllerGripButtonUp;
	public bool rightControllerTriggerButtonUp;

	//Joints: [root, pelvis, spine, chest, neck, head, leftShoulder, leftUpperArm, leftForearm, leftHand, rightShoulder, rightUpperArm, rightForearm, rightHand, leftThigh, leftCalf, leftFoot, leftToes, rightThigh, rightCalf, rightFoot, rightToes]
	public Vector3Definition[] skeletalPositions;
	public Vector4Definition[] skeletalRotations;

	public PlayerStateDefinition ()
	{
		this.timestamp = System.DateTime.Now.ToString ("yyyy-MM-dd-HH-mm-ss-fff");
		this.headPosition = new Vector3Definition ();
		this.headRotation = new Vector4Definition ();
		this.leftHandPosition = new Vector3Definition ();
		this.leftHandRotation = new Vector4Definition ();
		this.rightHandPosition = new Vector3Definition ();
		this.rightHandRotation = new Vector4Definition ();
		this.skeletalPositions = new Vector3Definition[22];
		this.skeletalRotations = new Vector4Definition[22];
	}

	public PlayerStateDefinition 
	(
		string timestampString,
		Vector3Definition headPosition, 
		Vector4Definition headRotation, 
		Vector3Definition leftHandPosition, 
		Vector4Definition leftHandRotation, 
		Vector3Definition rightHandPosition,
		Vector4Definition rightHandRotation,
		bool leftControllerGripButtonDown,
		bool leftControllerTriggerButtonDown,
		bool rightControllerGripButtonDown,
		bool rightControllerTriggerButtonDown,
		bool leftControllerGripButtonUp,
		bool leftControllerTriggerButtonUp,
		bool rightControllerGripButtonUp,
		bool rightControllerTriggerButtonUp,
		//Joints: [root, pelvis, spine, chest, neck, head, leftShoulder, leftUpperArm, leftForearm, leftHand, rightShoulder, rightUpperArm, rightForearm, rightHand, leftThigh, leftCalf, leftFoot, leftToes, rightThigh, rightCalf, rightFoot, rightToes]
		Vector3Definition[] skeletalPositions,
		Vector4Definition[] skeletalRotations
	)
	{
		this.timestamp = timestampString;
		this.headPosition = headPosition;
		this.headRotation = headRotation;
		this.leftHandPosition = leftHandPosition;
		this.leftHandRotation = leftHandRotation;
		this.rightHandPosition = rightHandPosition;
		this.rightHandRotation = rightHandRotation;
		this.leftControllerGripButtonDown = leftControllerGripButtonDown;
		this.leftControllerTriggerButtonDown = leftControllerTriggerButtonDown;
		this.rightControllerGripButtonDown = rightControllerGripButtonDown;
		this.rightControllerTriggerButtonDown = rightControllerTriggerButtonDown;
		this.leftControllerGripButtonUp = leftControllerGripButtonUp;
		this.leftControllerTriggerButtonUp = leftControllerTriggerButtonUp;
		this.rightControllerGripButtonUp = rightControllerGripButtonUp;
		this.rightControllerTriggerButtonUp = rightControllerTriggerButtonUp;
		this.skeletalPositions = skeletalPositions;
		this.skeletalRotations = skeletalRotations;
	}

	public string Serialize()
	{
		return JsonUtility.ToJson (this);
	}

	public object Deserialize(string stringObj)
	{
		return JsonUtility.FromJson<PlayerStateDefinition> (stringObj);
	}

    /// <summary>
    /// Method to interpolate between two PlayerStateDefinition objects by an amount from 0 and 1, where 0 is state 1, 1 is state 2, 
    /// and between the two are interpolations.
    /// </summary>
    /// <param name="state1">The first PlayerStateDefinition object</param>
    /// <param name="state2">The second PlayerStateDefinition object</param>
    /// <param name="amount">An amount between 0 and 1 (including 0 and 1)</param>
    /// <returns></returns>
    public static PlayerStateDefinition Interpolate(PlayerStateDefinition state1, PlayerStateDefinition state2, double amount)
    {
        PlayerStateDefinition newState = new PlayerStateDefinition();

        //Interpolating Timestamp
        DateTime time1 = DateTime.ParseExact(state1.timestamp, "yyyy-MM-dd-HH-mm-ss-fff", System.Globalization.CultureInfo.InvariantCulture);
        DateTime time2 = DateTime.ParseExact(state2.timestamp, "yyyy-MM-dd-HH-mm-ss-fff", System.Globalization.CultureInfo.InvariantCulture);
        DateTime time = new DateTime(time1.Ticks + (long)(Math.Round((time2.Ticks - time1.Ticks) * amount)));
        newState.timestamp = time.ToString("yyyy-MM-dd-HH-mm-ss-fff");

        //Interpolating Vive joints
        newState.headPosition = new Vector3Definition(Vector3.Lerp(state1.headPosition.ToVector3(), state2.headPosition.ToVector3(), (float)amount));
        newState.headRotation = new Vector4Definition(Quaternion.Slerp(state1.headRotation.ToQuaternion(), state2.headRotation.ToQuaternion(), (float)amount));
        newState.leftHandPosition = new Vector3Definition(Vector3.Lerp(state1.leftHandPosition.ToVector3(), state2.leftHandPosition.ToVector3(), (float)amount));
        newState.leftHandRotation = new Vector4Definition(Quaternion.Slerp(state1.leftHandRotation.ToQuaternion(), state2.leftHandRotation.ToQuaternion(), (float)amount));
        newState.rightHandPosition = new Vector3Definition(Vector3.Lerp(state1.rightHandPosition.ToVector3(), state2.rightHandPosition.ToVector3(), (float)amount));
        newState.rightHandRotation = new Vector4Definition(Quaternion.Slerp(state1.rightHandRotation.ToQuaternion(), state2.rightHandRotation.ToQuaternion(), (float)amount));

        //Interpolating Vive buttons
        if (amount < 0.5)
        {
            newState.leftControllerGripButtonDown = state1.leftControllerGripButtonDown;
            newState.leftControllerTriggerButtonDown = state1.leftControllerTriggerButtonDown;
            newState.rightControllerGripButtonDown = state1.rightControllerGripButtonDown;
            newState.rightControllerTriggerButtonDown = state1.rightControllerTriggerButtonDown;
            newState.leftControllerGripButtonUp = state1.leftControllerGripButtonUp;
            newState.leftControllerTriggerButtonUp = state1.leftControllerTriggerButtonUp;
            newState.rightControllerGripButtonUp = state1.rightControllerGripButtonUp;
            newState.rightControllerTriggerButtonUp = state1.rightControllerTriggerButtonUp;
        }
        else
        {
            newState.leftControllerGripButtonDown = state2.leftControllerGripButtonDown;
            newState.leftControllerTriggerButtonDown = state2.leftControllerTriggerButtonDown;
            newState.rightControllerGripButtonDown = state2.rightControllerGripButtonDown;
            newState.rightControllerTriggerButtonDown = state2.rightControllerTriggerButtonDown;
            newState.leftControllerGripButtonUp = state2.leftControllerGripButtonUp;
            newState.leftControllerTriggerButtonUp = state2.leftControllerTriggerButtonUp;
            newState.rightControllerGripButtonUp = state2.rightControllerGripButtonUp;
            newState.rightControllerTriggerButtonUp = state2.rightControllerTriggerButtonUp;
        }

        //Interpolating IK skeletal joints
        List<Vector3Definition> positions = new List<Vector3Definition>();
        for (int i = 0; i < state1.skeletalPositions.Length; i++)
        {
            if (state1.skeletalPositions[i] != null && state2.skeletalPositions[i] != null)
            {
                positions.Add(new Vector3Definition(Vector3.Lerp(state1.skeletalPositions[i].ToVector3(), state2.skeletalPositions[i].ToVector3(), (float)amount)));
            }
            else
            {
                positions.Add(new Vector3Definition());
            }
        }
        newState.skeletalPositions = positions.ToArray();

        List<Vector4Definition> rotations = new List<Vector4Definition>();
        for (int i = 0; i < state1.skeletalRotations.Length; i++)
        {
            if (state1.skeletalRotations[i] != null && state2.skeletalRotations[i] != null)
            {
                rotations.Add(new Vector4Definition(Quaternion.Slerp(state1.skeletalRotations[i].ToQuaternion(), state2.skeletalRotations[i].ToQuaternion(), (float)amount)));
            }
            else
            {
                rotations.Add(new Vector4Definition());
            }
        }
        newState.skeletalRotations = rotations.ToArray();

        return newState;
	}

    public PlayerStateDefinition DeepClone()
    {
        PlayerStateDefinition clone = (PlayerStateDefinition) this.MemberwiseClone();

        clone.timestamp = String.Copy(this.timestamp);
        
        clone.headPosition = this.headPosition.DeepCopy();
        clone.headRotation = this.headRotation.DeepCopy();

        clone.leftHandPosition = this.leftHandPosition.DeepCopy();
        clone.leftHandRotation = this.leftHandRotation.DeepCopy();

        clone.rightHandPosition = this.rightHandPosition.DeepCopy();
        clone.rightHandRotation = this.rightHandRotation.DeepCopy();

        clone.objectPosition = this.objectPosition.DeepCopy();
        clone.objectRotation = this.objectRotation.DeepCopy();
        
        clone.skeletalPositions = new Vector3Definition[this.skeletalPositions.Length];
        for (int i = 0; i < clone.skeletalPositions.Length; i ++)
        {
            clone.skeletalPositions[i] = (Vector3Definition)this.skeletalPositions[i].DeepCopy();
        }

        clone.skeletalRotations = new Vector4Definition[this.skeletalRotations.Length];
        for (int i = 0; i < clone.skeletalPositions.Length; i ++)
        {
            clone.skeletalRotations[i] = (Vector4Definition)this.skeletalRotations[i].DeepCopy();
        }

        return clone;
    }
}
