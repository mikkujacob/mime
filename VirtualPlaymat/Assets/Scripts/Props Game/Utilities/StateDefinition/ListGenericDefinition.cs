﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ListGenericDefinition<T> : List<T>
{
	public ListGenericDefinition () : base ()
	{
		
	}

	public ListGenericDefinition (System.Collections.Generic.IEnumerable<T> obj) : base (obj)
	{

	}

	public string Serialize()
	{
		// return JsonUtility.ToJson (this);

		string serialized = "";

		foreach (T obj in this)
		{
			serialized += JsonUtility.ToJson (obj) + "@";
		}

		serialized = serialized.Substring (0, serialized.Length - 1);

		return serialized;
	}

	public object Deserialize(string stringObj)
	{
		// return JsonUtility.FromJson<SessionDefinition> (stringObj);

		string[] data = stringObj.Split ('@');
		ListGenericDefinition<T> list = new ListGenericDefinition<T> ();

		foreach (string datum in data)
		{
			list.Add (JsonUtility.FromJson<T> (datum));
		}

		return list;
	}
}

