﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class Vector4Definition
{
	public float x;
	public float y;
	public float z;
	public float w;

    public Vector4Definition()
    {
        w = 1;
    }

    public Vector4Definition(float x, float y, float z, float w)
    {
        this.x = x;
        this.y = y;
        this.z = z;
        this.w = w;
    }

    public Vector4Definition(Quaternion orientation)
    {
        this.x = orientation.x;
        this.y = orientation.y;
        this.z = orientation.z;
        this.w = orientation.w;
	}

	public Quaternion ToQuaternion()
	{
		return new Quaternion (x, y, z, w);
	}

    public Vector4Definition DeepCopy()
    {
        return (Vector4Definition)this.MemberwiseClone();
    }
}
