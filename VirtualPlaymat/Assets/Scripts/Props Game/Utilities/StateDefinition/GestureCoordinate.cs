﻿using System;
using System.Collections.Generic;
using System.Linq;

[System.Serializable]
public class GestureCoordinate : ISpatialData
{
    public Envelope Envelope {get;}
    public Guid Guid {get;}
    public string action_label { get; set; }
    public string prop_label { get; set; }
    public string prop_Name { get; set; }

    public GestureCoordinate() { }

    public GestureCoordinate(Envelope envelope) {
        this.Envelope = envelope;
    }

    public GestureCoordinate(Envelope envelope, Guid guid)
    {
        this.Envelope = envelope;
        this.Guid = guid;
        this.action_label = "";
        this.prop_label = "";
    }

    public GestureCoordinate(Envelope envelope, Guid guid, string action_label, string prop_label, string prop_Name)
    {
        this.Envelope = envelope;
        this.Guid = guid;
        this.action_label = action_label;
        this.prop_label = prop_label;
        this.prop_Name = prop_Name;
    }

    public GestureCoordinate DeepCopy()
    {
        Envelope cloneEnvelope;
        if (this.Envelope != null) {
            cloneEnvelope = new Envelope(this.Envelope.MinX, this.Envelope.MinY, this.Envelope.MaxX, this.Envelope.MaxY);
        } else
        {
            cloneEnvelope = null;
        }

        Guid cloneGuid;
        if (this.Guid != null) {
            cloneGuid = new Guid(this.Guid.ToByteArray());
        }

        GestureCoordinate clone = new GestureCoordinate(cloneEnvelope, cloneGuid, String.Copy(this.label));
        
        return clone;
    }
}
