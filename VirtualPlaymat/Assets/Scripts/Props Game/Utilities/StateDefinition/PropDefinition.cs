﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
/// <summary>
/// Definition class for props
/// </summary>
public class PropDefinition {
    
    private Dictionary<string, float[]> propNameAffordanceLookup = new Dictionary<string, float[]> {
        {"Squid w Tentacles Partless", new float[]{0,0.3333333F,0,0,0.3333333F,0,0,0,0,0,0,0.3333333F,0.3333333F,0,0,0,0,0,0,0,0,0,0,0,0}},
        {"Lolliop w Cube Ends Partless", new float[]{0.666666F,0,0,0,0,0,0,0.3333333F,0,0.3333333F,0,0.3333333F,0,0.3333333F,0,0,0,0,0.666666F,0,0,0,0,0,0}},
        {"Giant U w Flat Base Partless", new float[]{0,0.3333333F,0,0.3333333F,0,0,0,0.3333333F,0.3333333F,0,0,0,0,0.666666F,0.3333333F,0,0,0,0.666666F,0.3333333F,0,0,0,0,0}},
        {"Big Curved Axe Partless", new float[]{0,0.3333333F,0.3333333F,0,0,0,0,0.3333333F,0.3333333F,0,0,0,0.3333333F,0.3333333F,0.3333333F,0,0,0,0.3333333F,0,0,0,0,0,0}},
        {"Sunflower Partless", new float[]{0,0.3333333F,0,0.3333333F,0,0,0,0,0,0.666666F,0,0,0.3333333F,0,0.3333333F,0,0.3333333F,0,0.666666F,0.3333333F,0,0,0,0,0}},
        {"Air Horn", new float[]{0,0,0,0.3333333F,0.3333333F,0,0,0,0,0.666666F,0,0,0,0,0,0.3333333F,0,0,0.3333333F,0,0,0,0,0,0}},
        {"Corn Dog", new float[]{0,0.666666F,0,0,0,0,0,0.3333333F,0.3333333F,0,0,0.3333333F,0,0.3333333F,0,0,0,0,0.666666F,0,0,0,0,0,0}},
        {"Giant Electric Plug", new float[]{0.3333333F,0.666666F,0,0,0,0,0,0.3333333F,0.3333333F,0.3333333F,0,0,0.3333333F,0.3333333F,0.3333333F,0,0,0,0,0.3333333F,0,0,0,0,0}},
        {"Ring w Tail End", new float[]{0,0.3333333F,0,0,0,0.3333333F,0,0,0.666666F,0,0,0,0.3333333F,0,0.666666F,0,0,0.3333333F,0,0,0,0,0.3333333F,0,0}},
        {"Ring w Inward Spokes", new float[]{0,0.3333333F,0,0,0,0.666666F,0,0.3333333F,0.666666F,0,0,0,0,1,1,0,0,0,0,0,0,0,0,0,0}},
        {"Large Ring w Outward Spokes", new float[]{0,0.3333333F,0,0,0,0.3333333F,0,0.3333333F,0,0.3333333F,0,0,0.3333333F,0,0.3333333F,0,0,0,0,0,0,0.3333333F,0,0,0}},
        {"Tube w Cones", new float[]{0,0.3333333F,0,0,0.3333333F,0,0,0.3333333F,0,0.3333333F,0,0,0.3333333F,0,0,0,0,0,0,0.3333333F,0,0,0.3333333F,0,0}},
        {"Thin Stick w Hammer End", new float[]{0,0.666666F,0,0,0,0,0,0.666666F,0,0,0,0.3333333F,0,0.3333333F,0,0,0,0,0.666666F,0,0,0,0,0,0}},
        {"Horseshoe", new float[]{0,0.3333333F,0,0,0.3333333F,0,0,0,0,0,0,0.3333333F,0.3333333F,0,0,0,0,0,0,0,0,0,0,0,0}},
        {"Helix", new float[]{0,0,0,0,0,0,0.3333333F,0,0.3333333F,0,0,0,0.3333333F,0,0,0,0,0,0,0,0,0,0.3333333F,0,0}},
        {"Giant Golf T", new float[]{0,0,0,0.3333333F,0.3333333F,0,0,0,0.3333333F,0.3333333F,0,0,0.3333333F,0,0.3333333F,0,0.3333333F,0,0.666666F,0,0,0,0,0,0}},
        {"Circle With Ring", new float[]{0,0,0,0.3333333F,0,0.3333333F,0,0,0,0.666666F,0,0,0,0,0.666666F,0,0,0,0.666666F,0,0,0,0,0,0}},
        {"Palm Tree Partless", new float[]{0,0,0,0,0.666666F,0,0,0,0.3333333F,0.3333333F,0,0.3333333F,0.3333333F,0,0,0,0,0,0.666666F,0,0,0,0,0,0}},
        {"Ladle", new float[]{0,0.3333333F,0,0.3333333F,0,0,0,0.3333333F,0.3333333F,0,0,0,0.3333333F,0,0.3333333F,0,0.3333333F,0,0.666666F,0,0,0,0,0,0}},
        {"Plunger", new float[]{0,0.666666F,0,0,0.3333333F,0,0,0,0.3333333F,0.3333333F,0,0,0,0,0,1,0,0,0.666666F,0,0,0,0,0,0}}
    };

    public string Name { get; set; }
    public string Size { get; set; }
    public string Color { get; set; }
    public float[] Affordance { get; set; }

    public PropDefinition()
    {

    }

    public PropDefinition(string name)
    {
        this.Name = name;
    }

    public PropDefinition(string name, float[] affordance)

    {
        this.Name = name;
        this.Affordance = affordance;
    }

    public void SetName(string name)
    {
        this.Name = name;
        this.Affordance = propNameAffordanceLookup[name];
    }

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
