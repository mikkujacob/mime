﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum DataEnum {
    Data27000 = 27000,
    Data16000 = 16000
}

public class RecordedGesture
{
    public Guid id;
	private GestureDefinition gesture;
	private DateTime playStartTime;
	public TimeSpan currentTimeSpan;
	private SortedList<TimeSpan, PlayerStateDefinition> gestureFrames;
	public bool isInit;
	private IEnumerator<KeyValuePair<TimeSpan, PlayerStateDefinition>> gestureFrameEnumerator;
	public bool isPlaying;
	public bool isLooping;
    public static bool isGestureTooShort;
    public float gestureDuration;

    private DataEnum dataType;

	public RecordedGesture(string fileName, DataEnum dataE = DataEnum.Data27000)
    {
        dataType = dataE;
        gesture = new GestureDefinition ();
		gestureFrames = new SortedList<TimeSpan, PlayerStateDefinition> ();
		isInit = false;
		isPlaying = false;
        isGestureTooShort = false;
        isInit = LoadGesture (fileName);
    }

    public RecordedGesture(GestureDefinition gesture, DataEnum dataE = DataEnum.Data27000)
    {
        dataType = dataE;
        this.gesture = gesture;
        gestureFrames = new SortedList<TimeSpan, PlayerStateDefinition>();
        isInit = false;
        isPlaying = false;
        isGestureTooShort = false;
        isInit = LoadGesture(gesture);
    }

    public RecordedGesture(RecordedGesture recording, DataEnum dataE = DataEnum.Data27000)
    {
        dataType = dataE;
        isInit = false;
        isPlaying = false;
        isGestureTooShort = false;

        try
        {
            gesture = new GestureDefinition(recording.gesture, dataType);
            gestureFrames = new SortedList<TimeSpan, PlayerStateDefinition>(recording.gestureFrames);
            gestureFrameEnumerator = gestureFrames.GetEnumerator();
        }
        catch (Exception)
        {
            return;
        }

        isInit = true;
    }

    public RecordedGesture(string fileName, int skipN, DataEnum dataE = DataEnum.Data27000)
    {
        dataType = dataE;
        gesture = new GestureDefinition(dataType);
        gestureFrames = new SortedList<TimeSpan, PlayerStateDefinition>();
        isInit = false;
        isPlaying = false;
        isGestureTooShort = false;
        isInit = LoadGesture(fileName, skipN);
    }

    public RecordedGesture(List<string> fileNameList, DataEnum dataE = DataEnum.Data27000)
    {
        dataType = dataE;
        gesture = new GestureDefinition(dataType);
        gestureFrames = new SortedList<TimeSpan, PlayerStateDefinition>();
        isInit = false;
        isPlaying = false;
        isGestureTooShort = false;
        isInit = LoadGesture(fileNameList);
    }

    public RecordedGesture(float[,] jointData, string propName, bool reMap, DataEnum dataE = DataEnum.Data27000) : this(jointData, propName, reMap, 0, dataE){}

    public RecordedGesture(float[,] jointData, string propName, bool reMap, int skipN, DataEnum dataE = DataEnum.Data27000)
	{
		Debug.Log ("Init recorded gesture");
        dataType = dataE;
        gesture = new GestureDefinition(dataType);
		gestureFrames = new SortedList<TimeSpan, PlayerStateDefinition> ();
		isInit = false;
		isPlaying = false;
        isGestureTooShort = false;
        isInit = LoadGesture (jointData, propName, reMap, skipN);
	}
    
    /// <summary>
    /// Loads the gesture data
    /// </summary>
    /// <param name="jointData">The joint data for the gesture</param>
    /// <param name="propName">The name of the prop associated with the gesture</param>
    /// <param name="reMap">Remaps jointData if true</param>
    /// <param name="skipN"></param>
    /// <returns>True if gesture loads without errors, otherwise false</returns>
    public bool LoadGesture(float[,] jointData, string propName, bool reMap, int skipN)
	{
        //Debug.Log("Loading gesture");
        //JointData  is formatted as: HeadPosition, HeadRotation, LeftHandPosition, LeftHandRotation, RightHandPosition, RightHandRotation, PelvisPosition, PelvisRotation, LeftControllerGripButton, RightControllerGripButton
        try
		{
            isGestureTooShort = LoadToDefinition(jointData, propName, reMap, skipN);
            LoadToRecording(gesture);

        }
		catch (Exception e)
		{
			Debug.LogException (e);
			Debug.Log ("Recorded gesture failed to load.");
			return false;
		}

        //Debug.Log("Recorded gesture loaded");
        //if (isGestureTooShort == true)
        //{
        //    return false;
        //}
        return !isGestureTooShort;
	}

    /// <summary>
    /// Loads data into a GestureDefinition
    /// </summary>
    /// <param name="jointData"></param>
    /// <param name="propName"></param>
    /// <param name="reMap"></param>
    /// <param name="skipN"></param>
    /// <returns>True if gesture duration is too short
    ///          False if duration is satisfactory</returns>
    public bool LoadToDefinition(float[,] jointData, string propName, bool reMap, int skipN)
    {
        try
        {
            gesture = new GestureDefinition(dataType);
            gesture.LoadStates(jointData, propName, reMap, skipN);
        }
        catch (Exception e)
        {
            throw e;
        }
        
        return gesture.IsGestureTooShort;
    }

    /// <summary>
    /// Loads the data from the gesture into the RecordedGesture
    /// </summary>
    /// <param name="gesture">A loaded GestureDefinition</param>
    public void LoadToRecording(GestureDefinition gesture)
    {
        try
        {
            if (!isGestureTooShort)
            {
                int frameCount = 0;
                int skipCount = 0;
                int skipN = gesture.skipN;

                foreach (PlayerStateDefinition state in gesture.playerStates)
                {
                    if (skipN > 0)
                    {
                        if (skipCount != 0)
                        {
                            skipCount = (skipCount + 1) % (skipN + 1);
                            frameCount++;
                            continue;
                        }
                        else
                        {
                            skipCount++;
                        }
                    }
                    TimeSpan time = new TimeSpan(frameCount * 111111);//Measured in ticks or 1/10,000 of a millisecond.
                    gestureFrames.Add(time, state);
                    frameCount++;
                }
                gestureDuration = gesture.gestureDuration;
                gestureFrameEnumerator = gestureFrames.GetEnumerator();
            }
        } catch (Exception e)
        {
            throw e;
        }
    }

    public bool LoadGesture(GestureDefinition gesture)
    {
        try
        {
            isGestureTooShort = gesture.IsGestureTooShort;
            LoadToRecording(gesture);
        }
        catch (Exception e)
        {
            Debug.LogException(e);
            Debug.Log("Recorded gesture failed to load.");
            return false;
        }
        
        return !isGestureTooShort;
    }

 //   /// <summary>
 //   /// Checks to see if the joint data is near zero
 //   /// </summary>
 //   /// <param name="jointData">The gesture's joint data</param>
 //   /// <param name="index">The index of jointData</param>
 //   /// <param name="epsilon">Values below this amount will round to zero.</param>
 //   /// <returns>True if all the joint data is close to zero, otherwise false.</returns>
	//private bool IsZeros(float[,] jointData, int index, float epsilon)
	//{
 //      //JointData  is formatted as: HeadPosition, HeadRotation, LeftHandPosition, LeftHandRotation, RightHandPosition, RightHandRotation, PelvisPosition, PelvisRotation, LeftControllerGripButton, RightControllerGripButton
 //       if (jointData[0, index + 0] < epsilon &&      //HEAD POSITION X,Z
 //            jointData[0, index + 7] < epsilon &&     //LEFT HAND POSITION X
 //          jointData[0, index + 21] < epsilon )       //PELVIS POSITION X
 //       {
 //           return true;
 //       }
 //       ////If all joints are below 0 on Y axis
 //       //if (jointData[0, index + 1] < 0 &&
 //       //    jointData[0, index + 8] < 0 &&
 //       //    jointData[0, index + 15] < 0 &&
 //       //    jointData[0, index + 22] < 0)
 //       //{
 //       //    return true;
 //       //}
 //       return false;
	//}

	//private double RemapValue(double oldValue, double oldMin, double oldMax, double newMin, double newMax)
	//{
	//	return (newMax - newMin) * (oldValue - oldMin) / (oldMax - oldMin) + newMin;
	//}

    /// <summary>
    /// Loads the gesture data
    /// </summary>
    /// <param name="fileName">The name of the file containing the gesture data</param>
    /// <returns>True if gesture loads without errors, otherwise false</returns>
	public bool LoadGesture(string fileName)
	{
		int errors = 0;
        try
		{
			string gestureContent = System.IO.File.ReadAllText (fileName);
			gesture = (GestureDefinition) (gesture.Deserialize (gestureContent));
			DateTime startTime = DateTime.ParseExact (gesture.timestamp, "yyyy-MM-dd-HH-mm-ss-fff", System.Globalization.CultureInfo.InvariantCulture);
			foreach(PlayerStateDefinition state in gesture.playerStates)
			{
				DateTime timeStamp = DateTime.ParseExact (state.timestamp, "yyyy-MM-dd-HH-mm-ss-fff", System.Globalization.CultureInfo.InvariantCulture);
				TimeSpan time = timeStamp.Subtract (startTime);
//				Debug.Log ("Timespan: " + time + ", DateTime: " + timeStamp.ToString ("yyyy-MM-dd-HH-mm-ss-fff"));
				if(gestureFrames.ContainsKey(time))
				{
					Debug.Log("Duplicate Key: " + time);
					errors++;
					continue;

				}
				gestureFrames.Add (time, state);
			}

			gestureFrameEnumerator = gestureFrames.GetEnumerator ();
        }
		catch (Exception e)
		{
			Debug.LogException (e);
			Debug.Log ("Recorded gesture failed to load.");
			return false;
		}

		Debug.Log ("Recorded gesture loaded with " + errors + " corrupted frames.");

		return true;
	}

    /// <summary>
    /// Loads the gesture data
    /// </summary>
    /// <param name="fileName">The name of the file containing the gesture data</param>
    /// <param name="skipN"></param>
    /// <returns>True if gesture loads without errors, otherwise false</returns>
    public bool LoadGesture(string fileName, int skipN)
    {
        int errors = 0;

        try
        {
            string gestureContent = System.IO.File.ReadAllText(fileName);
            gesture = (GestureDefinition)(gesture.Deserialize(gestureContent));
            int skipCount = 0;
            DateTime startTime = DateTime.ParseExact(gesture.timestamp, "yyyy-MM-dd-HH-mm-ss-fff", System.Globalization.CultureInfo.InvariantCulture);
            foreach (PlayerStateDefinition state in gesture.playerStates)
            {
                if (skipN > 0)
                {
                    if (skipCount != 0)
                    {
                        skipCount = (skipCount + 1) % (skipN + 1);
                        continue;
                    }
                    else
                    {
                        skipCount++;
                    }
                }
                DateTime timeStamp = DateTime.ParseExact(state.timestamp, "yyyy-MM-dd-HH-mm-ss-fff", System.Globalization.CultureInfo.InvariantCulture);
                TimeSpan time = timeStamp.Subtract(startTime);
                //				Debug.Log ("Timespan: " + time + ", DateTime: " + timeStamp.ToString ("yyyy-MM-dd-HH-mm-ss-fff"));
                if (gestureFrames.ContainsKey(time))
                {
                    Debug.Log("Duplicate Key: " + time);
                    errors++;
                    continue;

                }
                gestureFrames.Add(time, state);
            }
            Debug.Log("Loaded Frames: " + gestureFrames.Count);
            gestureFrameEnumerator = gestureFrames.GetEnumerator();

            gesture.playerStates = new ListGenericDefinition<PlayerStateDefinition>();
            foreach (PlayerStateDefinition state in gestureFrames.Values)
            {
                gesture.AddState(state);
            }
        }
        catch (Exception e)
        {
            Debug.LogException(e);
            Debug.Log("Recorded gesture failed to load.");
            return false;
        }

        Debug.Log("Recorded gesture loaded with " + errors + " corrupted frames.");

        return true;
    }

    /// <summary>
    /// Loads the gesture data
    /// </summary>
    /// <param name="fileNameList">A list of files containing gesture data</param>
    /// <returns>True if gesture loads without errors, otherwise false</returns>
    public bool LoadGesture(List<string> fileNameList)
    {
        int errors = 0;

        try
        {
            bool firstFile = true;
            DateTime startTime = System.DateTime.Now;
            foreach (string fileName in fileNameList)
            {
                Debug.Log("Loading: " + fileName);
                string gestureContent = System.IO.File.ReadAllText(fileName);
                GestureDefinition currentGesture = new GestureDefinition();
                if (firstFile)
                {
                    gesture = (GestureDefinition)(gesture.Deserialize(gestureContent));
                    currentGesture = gesture;

                    startTime = DateTime.ParseExact(gesture.timestamp, "yyyy-MM-dd-HH-mm-ss-fff", System.Globalization.CultureInfo.InvariantCulture);
                }
                else
                {
                    currentGesture = (GestureDefinition)(gesture.Deserialize(gestureContent));
                }

                foreach (PlayerStateDefinition state in currentGesture.playerStates)
                {
                    DateTime timeStamp = DateTime.ParseExact(state.timestamp, "yyyy-MM-dd-HH-mm-ss-fff", System.Globalization.CultureInfo.InvariantCulture);
                    TimeSpan time = timeStamp.Subtract(startTime);
                    //Debug.Log("Timespan: " + time + ", DateTime: " + timeStamp.ToString("yyyy-MM-dd-HH-mm-ss-fff"));
                    if (gestureFrames.ContainsKey(time))
                    {
                        //Debug.Log("Duplicate Key: " + time);
                        errors++;
                        continue;

                    }
                    gestureFrames.Add(time, state);
                }

                if (firstFile)
                {
                    firstFile = false;
                }
                else
                {
                    gesture.playerStates.AddRange(currentGesture.playerStates);
                }
            }
            
            gestureFrameEnumerator = gestureFrames.GetEnumerator();
        }
        catch (Exception e)
        {
            Debug.LogException(e);
            Debug.Log("Recorded gesture failed to load.");
            return false;
        }

        Debug.Log("Recorded gesture loaded with " + errors + " corrupted frames.");

        return true;
    }

    /// <summary>
    /// Sets isPlaying to true
    /// </summary>
    public void Play()
	{
		playStartTime = System.DateTime.Now;
		currentTimeSpan = TimeSpan.MinValue;
		isPlaying = true;
	}

    /// <summary>
    /// Sets isPlaying to false
    /// </summary>
	public void Stop()
	{
		playStartTime = System.DateTime.MinValue;
		currentTimeSpan = TimeSpan.MinValue;
		isPlaying = false;
	}

    /// <summary>
    /// Returns the gesture definition information
    /// </summary>
    /// <returns>The gesture definition</returns>
	public GestureDefinition GetGesture()
	{
		if (isInit)
		{
			return gesture;
		}
		else
		{
			return null;
		}
	}

    /// <summary>
    /// Returns the next frame within the action sequence.
    /// </summary>
    /// <returns>The next frame</returns>
	public PlayerStateDefinition GetNextFrame()
	{
		try
		{
			if(isInit)
			{
				if(gestureFrameEnumerator.MoveNext ())
				{
					currentTimeSpan = gestureFrameEnumerator.Current.Key;
					return gestureFrameEnumerator.Current.Value;
				}
				else if(isLooping)
				{
					gestureFrameEnumerator.Reset ();
					if(gestureFrameEnumerator.MoveNext ())
					{
						currentTimeSpan = gestureFrameEnumerator.Current.Key;
						return gestureFrameEnumerator.Current.Value;
					}
				}
			}

		}
		catch(Exception e)
		{
			currentTimeSpan = TimeSpan.MinValue;
			return null;
		}

		currentTimeSpan = TimeSpan.MinValue;
		return null;
	}

    /// <summary>
    /// Finds the frame closest to the timestamp
    /// </summary>
    /// <param name="time">The timestamp</param>
    /// <returns>The frame closest to the time</returns>
	public PlayerStateDefinition GetNearestFrame(DateTime time)
	{
		if (!isPlaying)
		{
			return null;
		}
		return GetNearestFrameDelta (time.Subtract (playStartTime));
	}

    /// <summary>
    /// Finds the frame closest to the delta
    /// </summary>
    /// <param name="delta">The amount of time passed since the recording started playing</param>
    /// <returns>The closest frame to the delta</returns>
	public PlayerStateDefinition GetNearestFrameDelta(TimeSpan delta)
	{
//		if (!isPlaying)
//		{
//			return null;
//		}
		currentTimeSpan = delta;
		return gestureFrames [FindNearestTimeSpan (delta)];
	}

    /// <summary>
    /// Retrieves the closest frame to the specified time
    /// </summary>
    /// <param name="time">The time of the desired frame</param>
    /// <returns>The closest frame</returns>
	public PlayerStateDefinition GetNearestFrameInterpolated(DateTime time)
	{
		if (!isPlaying)
		{
			return null;
		}
		return GetNearestFrameDeltaInterpolated(time.Subtract (playStartTime));
	}

    /// <summary>
    /// Finds the closest frame to the specified time
    /// </summary>
    /// <param name="delta">The amount of time passed since the recording started playing</param>
    /// <returns>The gesture frame closest to the delta</returns>
	public PlayerStateDefinition GetNearestFrameDeltaInterpolated(TimeSpan delta)
	{
//		if (!isPlaying)
//		{
//			return null;
//		}
		currentTimeSpan = delta;

		TimeSpan[] ftcTimes = FindFloorTargetCeilingTimeSpans (delta);

		if (ftcTimes[1] != TimeSpan.MinValue) // Exact match was found
		{
			return gestureFrames[ftcTimes[1]];
		}
		else if(ftcTimes[0] != TimeSpan.MinValue && ftcTimes[2] != TimeSpan.MinValue) // Exact match NOT found. Floor and ceiling found
		{
			double amount = (delta.Subtract (ftcTimes[0]).TotalMilliseconds) / (ftcTimes[2].Subtract (ftcTimes[0]).TotalMilliseconds);
			return PlayerStateDefinition.Interpolate (gestureFrames [ftcTimes [0]], gestureFrames [ftcTimes [2]], amount);
		}
		else if(ftcTimes[0] != TimeSpan.MinValue && ftcTimes[1] == TimeSpan.MinValue && ftcTimes[2] == TimeSpan.MinValue) // Exact match NOT found. Floor found
		{
			return gestureFrames [ftcTimes [0]];
		}
		else if(ftcTimes[0] == TimeSpan.MinValue && ftcTimes[1] == TimeSpan.MinValue && ftcTimes[2] != TimeSpan.MinValue) // Exact match NOT found. Ceiling found
		{
			return gestureFrames [ftcTimes [2]];
		}

		return null;
	}

    /// <summary>
    /// Finds frames between two defined times and returns a list containing the frames
    /// </summary>
    /// <param name="start">The starting time</param>
    /// <param name="end">The ending time</param>
    /// <returns>A list of frames between the start and end time</returns>
    public List<PlayerStateDefinition> GetNearestFramesBetweenDeltas(TimeSpan start, TimeSpan end)
    {
        List<PlayerStateDefinition> resultStates = new List<PlayerStateDefinition>();
        TimeSpan nearestStart = FindNearestTimeSpan(start);
        int startIndex = gestureFrames.IndexOfKey(nearestStart);
        TimeSpan nearestEnd = FindNearestTimeSpan(end);
        int endIndex = gestureFrames.IndexOfKey(nearestEnd);

        List<PlayerStateDefinition> values = new List<PlayerStateDefinition>(gestureFrames.Values);

        for (int index = startIndex; index <= endIndex; index++)
        {
            resultStates.Add(values[index]);
        }

        return resultStates;
    }

    /// <summary>
    /// Finds the timespan closest to the targeted time
    /// </summary>
    /// <param name="targetTimeSpan">The targeted time</param>
    /// <returns>The timespan closest to the targetTimeSpan</returns>
	private TimeSpan FindNearestTimeSpan(TimeSpan targetTimeSpan)
	{
		try
		{
			if (targetTimeSpan <= gestureFrames.Keys [0]) // Target is less than or equal to first timespan
			{
				return gestureFrames.Keys [0]; // Return first element.
			}
			else if (gestureFrames.Keys [gestureFrames.Keys.Count - 1] <= targetTimeSpan) // Last timespan is less than or equal to target
			{
				return gestureFrames.Keys [gestureFrames.Keys.Count - 1]; // Return last element.
			}
			else if (gestureFrames.ContainsKey (targetTimeSpan)) // Target is found
			{
				return targetTimeSpan; // Return target.
			}

			List<TimeSpan> times = new List<TimeSpan>(gestureFrames.Keys);
			int targetIndex = times.BinarySearch (targetTimeSpan);

			// Not found. Result of BinarySearch is bitwise complement of next largest element.
			int ceilIndex = ~targetIndex;

			int floorIndex = ceilIndex - 1;

			TimeSpan floorTimeSpan = targetTimeSpan.Subtract (times [floorIndex]);
			TimeSpan ceilTimeSpan = times [ceilIndex].Subtract (targetTimeSpan);

			return (floorTimeSpan < ceilTimeSpan) ? times [floorIndex] : times [ceilIndex];
		}
		catch(Exception e)
		{
			return TimeSpan.MinValue;
		}
	}

    /// <summary>
    /// Finds the targeted or closest time within the range of the gesture frames
    /// </summary>
    /// <param name="targetTimeSpan">The targeted time</param>
    /// <returns>An array with the floor, target, and ceiling states</returns>
	private TimeSpan[] FindFloorTargetCeilingTimeSpans(TimeSpan targetTimeSpan) //Returns array with floor, target, and ceiling states.
	{
		TimeSpan[] ftcTimes = new TimeSpan[3];

		try
		{
			if (targetTimeSpan <= gestureFrames.Keys [0]) // Target is less than or equal to first timespan
			{
				ftcTimes [0] = TimeSpan.MinValue;
				ftcTimes [1] = TimeSpan.MinValue;
				ftcTimes [2] = gestureFrames.Keys [0];
				return ftcTimes;
			}
			else if (gestureFrames.Keys [gestureFrames.Keys.Count - 1] <= targetTimeSpan) // Last timespan is less than or equal to target
			{
				ftcTimes [0] = gestureFrames.Keys [gestureFrames.Keys.Count - 1];
				ftcTimes [1] = TimeSpan.MinValue;
				ftcTimes [2] = TimeSpan.MinValue;
				return ftcTimes;
			}
			else if (gestureFrames.ContainsKey (targetTimeSpan))
			{
				ftcTimes [0] = TimeSpan.MinValue;
				ftcTimes [1] = targetTimeSpan;
				ftcTimes [2] = TimeSpan.MinValue;
				return ftcTimes;
			}

			List<TimeSpan> times = new List<TimeSpan>(gestureFrames.Keys);

			int targetIndex = times.BinarySearch (targetTimeSpan);

			// Not found. Result of BinarySearch is bitwise complement of next largest element.
			int ceilIndex = ~targetIndex;

			int floorIndex = ceilIndex - 1;

			ftcTimes [0] = times [floorIndex];
			ftcTimes [1] = TimeSpan.MinValue;
			ftcTimes [2] = times [ceilIndex];
			return ftcTimes;
		}
		catch(Exception e)
		{
			ftcTimes [0] = TimeSpan.MinValue;
			ftcTimes [1] = TimeSpan.MinValue;
			ftcTimes [2] = TimeSpan.MinValue;
			return ftcTimes;
		}
	}

    public void InsertFrames(int num)
    {
        //string gestureContent = System.IO.File.ReadAllText(fileName);
        //gesture = (GestureDefinition)(gesture.Deserialize(gestureContent));
        gestureFrames = new SortedList<TimeSpan, PlayerStateDefinition>();
        DateTime startTime = DateTime.ParseExact(gesture.timestamp, "yyyy-MM-dd-HH-mm-ss-fff", System.Globalization.CultureInfo.InvariantCulture);
        for (int k = 0; k < gesture.playerStates.Count - 1; k ++)
        {
            PlayerStateDefinition state_1 = gesture.playerStates[k];
            PlayerStateDefinition state_2 = gesture.playerStates[k + 1];
            DateTime timeStamp_1 = DateTime.ParseExact(state_1.timestamp, "yyyy-MM-dd-HH-mm-ss-fff", System.Globalization.CultureInfo.InvariantCulture);
            DateTime timeStamp_2 = DateTime.ParseExact(state_2.timestamp, "yyyy-MM-dd-HH-mm-ss-fff", System.Globalization.CultureInfo.InvariantCulture);
            TimeSpan time = timeStamp_1.Subtract(startTime);
            TimeSpan duration = timeStamp_2.Subtract(timeStamp_1);
            TimeSpan interframe = new TimeSpan(duration.Ticks / num);
            for (int i = 0; i < num; i++)
            {
                PlayerStateDefinition new_inter_state = PlayerStateDefinition.Interpolate(state_1, state_2, (float)i / num);
                TimeSpan currentTime = time.Add(interframe);
                gestureFrames.Add(time, new_inter_state);
            }
        }

        gestureFrameEnumerator = gestureFrames.GetEnumerator();
    }

    public void ChangeRotation(Quaternion Rotation)
    {
        //string gestureContent = System.IO.File.ReadAllText(fileName);
        //gesture = (GestureDefinition)(gesture.Deserialize(gestureContent));
        foreach(KeyValuePair<TimeSpan, PlayerStateDefinition> entry in gestureFrames) {
            entry.Value.objectRotation = new Vector4Definition(entry.Value.objectRotation.ToQuaternion()*Rotation);
        }

        gestureFrameEnumerator = gestureFrames.GetEnumerator();
    }
}
