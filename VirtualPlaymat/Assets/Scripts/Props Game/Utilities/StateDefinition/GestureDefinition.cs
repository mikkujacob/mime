﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;
using System.IO;

[System.Serializable]
public class GestureDefinition
{
    public string gestureID;

    public string timestamp;

    public string objectName;
    public string objectSize;
    public Vector3Definition objectStartingPosition;
    public Vector4Definition objectStartingRotation;

    public string pretendActionName;
    public string pretendObjectName;

    public float gestureDuration;
    public bool IsGestureTooShort;
    public int skipN;
    private DataEnum dataType;

    public PropDefinition propObject;
    public GestureCoordinate coordinateIndex;

    public ListGenericDefinition<PlayerStateDefinition> playerStates;

	public GestureDefinition (DataEnum dataE = DataEnum.Data27000) : this(Guid.NewGuid().ToString(), System.DateTime.Now.ToString("yyyy-MM-dd-HH-mm-ss-fff"), "", "", new Vector3Definition(), new Vector4Definition(), new ListGenericDefinition<PlayerStateDefinition>(), " "," ", dataE) {}

    public GestureDefinition(GestureDefinition gesture, DataEnum dataE = DataEnum.Data27000) : this(gesture.gestureID, gesture.timestamp, gesture.objectName, gesture.objectSize, gesture.objectStartingPosition, gesture.objectStartingRotation, gesture.playerStates,gesture.pretendActionName,gesture.pretendObjectName, dataE) { }

    public GestureDefinition(string id, string timeString, string objName, string objSize, Vector3Definition objectPosition, Vector4Definition objectRotation, ListGenericDefinition<PlayerStateDefinition> states, string pActionName, string pObjectName, DataEnum dataE = DataEnum.Data27000)

    {
        gestureID = id;
        timestamp = timeString;
        objectName = objName;
        objectSize = objSize;
        objectStartingPosition = objectPosition;
        objectStartingRotation = objectRotation;
        playerStates = new ListGenericDefinition<PlayerStateDefinition>(states);
        Debug.Log(objName);
        propObject = new PropDefinition(objName);
        propObject.Size = objSize;
        IsGestureTooShort = false;
        dataType = dataE;
        skipN = 0;
        pActionName = pretendActionName;
        pObjectName = pretendObjectName;
    }

    public void AddState(PlayerStateDefinition state)
    {
        playerStates.Add(state);
    }

    /// <summary>
    /// Loads the frames into the gesture definition object
    /// </summary>
    /// <param name="jointData"></param>
    /// <param name="reMap"></param>
    /// <param name="skipN"></param>
    public void LoadStates(float[,] jointData, string propName, bool reMap, int skipN)
    {
        objectName = propName;
        this.skipN = skipN;
        //JointData 27000 is formatted as: HeadPosition, HeadRotation, LeftHandPosition, LeftHandRotation, RightHandPosition, RightHandRotation, PelvisPosition, PelvisRotation, LeftControllerGripButton, RightControllerGripButton
        //JointData 16000 is formatted as: HeadPosition, HeadRotation, LeftHandPosition, LeftHandRotation, RightHandPosition, RightHandRotation, PelvisPosition, PelvisRotation, PropPosition, PropRotation
        try
        {
            int frameCount = 0;
            int skipCount = 0;
            PlayerStateDefinition state;
            int dataLength = (int) dataType;
            for (int index = 0; index < dataLength;)
            {
                if (skipN > 0)
                {
                    if (skipCount != 0)
                    {
                        skipCount = (skipCount + 1) % (skipN + 1);
                        frameCount++;
                        continue;
                    }
                    else
                    {
                        skipCount++;
                    }
                }
                if (IsZeros(jointData, index, 0.45f))
                {
                    gestureDuration = frameCount * 0.0111111f;
                    if (gestureDuration < 3.0f)
                    {
                        IsGestureTooShort = true;
                    }
                    else
                    {
                        //Debug.Log("End of gesture at " + gestureDuration + " seconds.");
                    }
                    break;
                }
                state = new PlayerStateDefinition();
                if (reMap)
                {
                    state.headPosition = new Vector3Definition((float)RemapValue(jointData[0, index + 0], 0, 1, -2, 2), (float)RemapValue(jointData[0, index + 1], 0, 1, -2, 2), (float)RemapValue(jointData[0, index + 2], 0, 1, -2, 2));
                    state.headRotation = new Vector4Definition((float)RemapValue(jointData[0, index + 3], 0, 1, -2, 2), (float)RemapValue(jointData[0, index + 4], 0, 1, -2, 2), (float)RemapValue(jointData[0, index + 5], 0, 1, -2, 2), (float)RemapValue(jointData[0, index + 6], 0, 1, -2, 2));
                    state.leftHandPosition = new Vector3Definition((float)RemapValue(jointData[0, index + 7], 0, 1, -2, 2), (float)RemapValue(jointData[0, index + 8], 0, 1, -2, 2), (float)RemapValue(jointData[0, index + 9], 0, 1, -2, 2));
                    state.leftHandRotation = new Vector4Definition((float)RemapValue(jointData[0, index + 10], 0, 1, -2, 2), (float)RemapValue(jointData[0, index + 11], 0, 1, -2, 2), (float)RemapValue(jointData[0, index + 12], 0, 1, -2, 2), (float)RemapValue(jointData[0, index + 13], 0, 1, -2, 2));
                    state.rightHandPosition = new Vector3Definition((float)RemapValue(jointData[0, index + 14], 0, 1, -2, 2), (float)RemapValue(jointData[0, index + 15], 0, 1, -2, 2), (float)RemapValue(jointData[0, index + 16], 0, 1, -2, 2));
                    state.rightHandRotation = new Vector4Definition((float)RemapValue(jointData[0, index + 17], 0, 1, -2, 2), (float)RemapValue(jointData[0, index + 18], 0, 1, -2, 2), (float)RemapValue(jointData[0, index + 19], 0, 1, -2, 2), (float)RemapValue(jointData[0, index + 20], 0, 1, -2, 2));
                    state.skeletalPositions[1] = new Vector3Definition((float)RemapValue(jointData[0, index + 21], 0, 1, -2, 2), (float)RemapValue(jointData[0, index + 22], 0, 1, -2, 2), (float)RemapValue(jointData[0, index + 23], 0, 1, -2, 2));
                    state.skeletalRotations[1] = new Vector4Definition((float)RemapValue(jointData[0, index + 24], 0, 1, -2, 2), (float)RemapValue(jointData[0, index + 25], 0, 1, -2, 2), (float)RemapValue(jointData[0, index + 26], 0, 1, -2, 2), (float)RemapValue(jointData[0, index + 27], 0, 1, -2, 2));
                }
                else
                {
                    state.headPosition = new Vector3Definition(jointData[0, index + 0], jointData[0, index + 1], jointData[0, index + 2]);
                    state.headRotation = new Vector4Definition(jointData[0, index + 3], jointData[0, index + 4], jointData[0, index + 5], jointData[0, index + 6]);
                    state.leftHandPosition = new Vector3Definition(jointData[0, index + 7], jointData[0, index + 8], jointData[0, index + 9]);
                    state.leftHandRotation = new Vector4Definition(jointData[0, index + 10], jointData[0, index + 11], jointData[0, index + 12], jointData[0, index + 13]);
                    state.rightHandPosition = new Vector3Definition(jointData[0, index + 14], jointData[0, index + 15], jointData[0, index + 16]);
                    state.rightHandRotation = new Vector4Definition(jointData[0, index + 17], jointData[0, index + 18], jointData[0, index + 19], jointData[0, index + 20]);
                    state.skeletalPositions[1] = new Vector3Definition(jointData[0, index + 21], jointData[0, index + 22], jointData[0, index + 23]);
                    state.skeletalRotations[1] = new Vector4Definition(jointData[0, index + 24], jointData[0, index + 25], jointData[0, index + 26], jointData[0, index + 27]);
                }

                switch (dataType)
                {
                    case DataEnum.Data27000:
                        state.leftControllerGripButtonDown = jointData[0, index + 28] > 0.5;
                        state.leftControllerGripButtonUp = !state.leftControllerGripButtonDown;
                        state.rightControllerGripButtonDown = jointData[0, index + 29] > 0.5;
                        state.rightControllerGripButtonUp = !state.rightControllerGripButtonDown;
                        if (state.rightControllerGripButtonDown && state.leftControllerGripButtonUp)
                        {
                            state.objectPosition = new Vector3Definition(state.rightHandPosition.x, state.rightHandPosition.y, state.rightHandPosition.z);
                            state.objectRotation = new Vector4Definition(state.rightHandRotation.x, state.rightHandRotation.y, state.rightHandRotation.z, state.rightHandRotation.w);
                        }
                        else if (state.rightControllerGripButtonUp && state.leftControllerGripButtonDown)
                        {
                            state.objectPosition = new Vector3Definition(state.leftHandPosition.x, state.leftHandPosition.y, state.leftHandPosition.z);
                            state.objectRotation = new Vector4Definition(state.leftHandRotation.x, state.leftHandRotation.y, state.leftHandRotation.z, state.leftHandRotation.w);
                        }
                        index += 30;
                        break;

                    case DataEnum.Data16000:
                        state.objectPosition = new Vector3Definition(jointData[0, index + 28], jointData[0, index + 29], jointData[0, index + 30]);
                        state.objectRotation = new Vector4Definition(jointData[0, index + 31], jointData[0, index + 32], jointData[0, index + 33], jointData[0, index + 34]);
                        index += 35;
                        break;
                }

                AddState(state);
                frameCount++;
            }
        }
        catch (Exception e)
        {
            throw e;
        }
    }

	public string Serialize()
	{
        //		return JsonUtility.ToJson (this);

        string serialized = gestureID + "$" + timestamp + "$" + objectName + "$" + objectSize + "$" + JsonUtility.ToJson(objectStartingPosition) + "$" + JsonUtility.ToJson(objectStartingRotation) + "$" + pretendActionName + "$" + pretendObjectName + "$";

        foreach (PlayerStateDefinition state in playerStates)
        {
            serialized += JsonUtility.ToJson(state) + "^";
        }

        serialized = serialized.Substring(0, serialized.Length - 1);

        return serialized;
    }

    public object Deserialize(string stringObj)
    {
        //		return JsonUtility.FromJson<GestureDefinition> (stringObj);

		string[] data = stringObj.Split ('$');
		string[] states = data [8].Split ('^');
		ListGenericDefinition<PlayerStateDefinition> stateDefs = new ListGenericDefinition<PlayerStateDefinition> ();
		foreach (string state in states)
		{
			stateDefs.Add (JsonUtility.FromJson<PlayerStateDefinition> (state));
		}

		return new GestureDefinition (data [0], data [1], data [2], data [3], JsonUtility.FromJson<Vector3Definition> (data [4]), JsonUtility.FromJson<Vector4Definition> (data [5]), stateDefs, data[6], data[7]);
	}

    /// <summary>
    /// Checks to see if the joint data is near zero
    /// </summary>
    /// <param name="jointData">The gesture's joint data</param>
    /// <param name="index">The index of jointData</param>
    /// <param name="epsilon">Values below this amount will round to zero.</param>
    /// <returns>True if all the joint data is close to zero, otherwise false.</returns>
    private bool IsZeros(float[,] jointData, int index, float epsilon)
    {
        //JointData  is formatted as: HeadPosition, HeadRotation, LeftHandPosition, LeftHandRotation, RightHandPosition, RightHandRotation, PelvisPosition, PelvisRotation, LeftControllerGripButton, RightControllerGripButton
        if (jointData[0, index + 0] < epsilon &&      //HEAD POSITION X,Z
             jointData[0, index + 7] < epsilon &&     //LEFT HAND POSITION X
           jointData[0, index + 21] < epsilon)       //PELVIS POSITION X
        {
            return true;
        }
        ////If all joints are below 0 on Y axis
        //if (jointData[0, index + 1] < 0 &&
        //    jointData[0, index + 8] < 0 &&
        //    jointData[0, index + 15] < 0 &&
        //    jointData[0, index + 22] < 0)
        //{
        //    return true;
        //}
        return false;
    }

    private double RemapValue(double oldValue, double oldMin, double oldMax, double newMin, double newMax)
    {
        return (newMax - newMin) * (oldValue - oldMin) / (oldMax - oldMin) + newMin;
    }

    public ListGenericDefinition<PlayerStateDefinition> GetPlayerStateDefinitions()
    {
        return playerStates;
    }

    public void SetName(string name)
    {
        objectName = name;
        propObject.SetName(name);
    }

    public void SetDuration()
    {
        gestureDuration = (float) DateTime.Now.Subtract(StringToDateTime(timestamp)).TotalSeconds;
        if (gestureDuration <= 3.0f)
        {
            IsGestureTooShort = true;
        }
    }

    public GestureDefinition DeepCopy()
    {
        GestureDefinition clone = (GestureDefinition) MemberwiseClone();

        string temp = String.Copy(this.gestureID);

        clone.gestureID = String.Copy(this.gestureID);
        clone.timestamp = String.Copy(this.timestamp);

        clone.objectName = String.Copy(this.objectName);
        clone.objectSize = String.Copy(this.objectSize);
        clone.objectStartingPosition = this.objectStartingPosition.DeepCopy();
        clone.objectStartingRotation = this.objectStartingRotation.DeepCopy();

        clone.propObject = new PropDefinition(this.objectName, this.propObject.Affordance)
        {
            Size = this.objectSize
        };

        if (this.coordinateIndex != null)
        {
            clone.coordinateIndex = this.coordinateIndex.DeepCopy();
        }

        clone.playerStates = new ListGenericDefinition<PlayerStateDefinition>(this.playerStates.ConvertAll(playerState => playerState.DeepClone()));
        
        return clone;
    }

    /// <summary>
    /// Converts a string representation of a date into a DateTime object.
    /// The format of the string must match "yyyy-MM-dd-HH-mm-ss-fff"
    /// </summary>
    /// <param name="timestamp">The string with the proper date/time format</param>
    /// <returns>A DateTime object form of the string</returns>
    private DateTime StringToDateTime(string timestamp)
    {
        string format = "yyyy-MM-dd-HH-mm-ss-fff";
        DateTime time = DateTime.ParseExact(timestamp, format, System.Globalization.CultureInfo.InvariantCulture);
        return time;
    }
}
