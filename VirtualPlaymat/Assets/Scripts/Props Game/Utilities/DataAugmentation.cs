﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.Props_Game.Utilities
{
    class DataAugmentation
    {
        static void Main() {
            //Shorten gesture by skipping the every other frame
            //RecordedGesture origin_gesture = new RecordedGesture("2018-04-11-16-15-32-644$2018-04-11-16-15-33-571$frisbee$throw.gesture", 1);

            //Increase gesture by inserting frames
            //RecordedGesture origin_gesture = new RecordedGesture("2018-04-11-16-15-32-644$2018-04-11-16-15-33-571$frisbee$throw.gesture");
            //origin_gesture.InsertFrames(2);

            //Add object rotation
            RecordedGesture origin_gesture = new RecordedGesture("2018-04-11-16-15-32-644$2018-04-11-16-15-33-571$frisbee$throw.gesture");
            origin_gesture.ChangeRotation(Quaternion.Euler(0, 90, 0));
        }
    }
}
