﻿using LiteDB;
using System;
using System.Linq;
using System.IO;
using System.Collections.Generic;
using UnityEngine;
using EpisodicMemory;

public class LoadGesture: MonoBehaviour
{ 
    private static string _strConnection = "Filename=test1.litedb4; Mode=Exclusive; upgrade=true";
    static RTree<GestureCoordinate> rTree;
    static TemporalMemory tMemory;
    private static FrozenGraphLoader model = DeepIMAGINATIONInterface.model;
    private static int temporalIndex = 0;
    RecordedGesture gesture;
    LiteCollection<RecordedGesture> recordedGestures;
    float[,] param;
    float[] affordance;
    List<float> vals;
    bool labelsNTimeSeriesLoaded = false;

    public void Start()
    {
        rTree = new RTree<GestureCoordinate>();
        tMemory = new TemporalMemory();
        using (var db = new LiteDatabase(_strConnection))
        {
            //Load gestures from LitDB
            var gestures = db.GetCollection<RecordedGesture>("gestures");

            //If no gesture in LiteDB
            if (gestures.Count() == 0)
            {
                // Look in StreamingAssets for
                if (Directory.Exists(Application.streamingAssetsPath))
                {
                    // .timeseries file
                    // affordance .labels file
                    // pretend action .labels file
                    // pretend object .labels file
                    if (Directory.GetFiles(Application.streamingAssetsPath, "*.labels").Length >= 3 && Directory.GetFiles(Application.streamingAssetsPath, "*.timeseries").Length >= 1)
                    {
                        // Load gestures from timeseries and labels file
                        string[] timeSeriesFiles = Directory.GetFiles(Application.streamingAssetsPath, "*.timeseries");
                        StreamReader timeseriesReader = new StreamReader(timeSeriesFiles[0]);
                        StreamReader labelsReader = new StreamReader(Application.streamingAssetsPath + "/train-labels-float32.labels");
                        StreamReader pretendObjectReader = new StreamReader(Application.streamingAssetsPath + "/CleanedGestureObjectLabels-450-3.33-10.0-2018-11-17-19-51-10-885.labels");
                        StreamReader pretendActionReader = new StreamReader(Application.streamingAssetsPath + "/CleanedGestureActionLabels-450-3.33-10.0-2018-11-13.labels");
                        string pretendObjectLabel;
                        while ((pretendObjectLabel = pretendObjectReader.ReadLine()) != null)
                        {
                            float[] inputTimeSeries = Array.ConvertAll<string, float>(timeseriesReader.ReadLine().Split(','), StringToFloat);
                            affordance = Array.ConvertAll<string, float>(labelsReader.ReadLine().Split(','), StringToFloat);

                            //string pretendObjectLabel = pretendObjectReader.ReadLine();
                            string pretendActionLabel = pretendActionReader.ReadLine();
                            pretendActionLabel = pretendActionLabel.Substring(0, pretendActionLabel.IndexOf("."));

                            param = new float[1, inputTimeSeries.Length + affordance.Length];
                            float[] values = Concatenate1DArrays<float>(inputTimeSeries, affordance);

                            for (int i = 0; i < values.Length; i++)
                            {
                                param[0, i] = values[i];
                            }

                            Dictionary<string, dynamic> generatorInputs = new Dictionary<string, dynamic>()
                            {
                                { "concat", param },
                                { "keep_prob", 1.0f }
                            };

                            // Generating output inference
                            float[,] outputMeans = model.GenerateOutput(generatorInputs, "encoder/dense/BiasAdd") as float[,];
                            //Debug.Log("Result successfully obtained. Results in form: " + outputMeans.GetLength(0) + "x" + outputMeans.GetLength(1));
                            float[,] outputSTDevs = model.GenerateOutput(generatorInputs, "encoder/dense_2/BiasAdd") as float[,];
                            //Debug.Log("Result successfully obtained. Results in form: " + outputSTDevs.GetLength(0) + "x" + outputSTDevs.GetLength(1));

                            // Finding the input's latent space value
                            vals = new List<float>();
                            foreach (float val in outputMeans)
                            {
                                vals.Add(val);
                            }
                            foreach (float val in outputSTDevs)
                            {
                                vals.Add(val);
                            }
                            vals.AddRange(affordance);
                            // Generate RecorededGesture

                            // TODO: Load actions into RTree, Episodic Memory
                            recordedGestures = db.GetCollection<RecordedGesture>("recordedGesture");
                            var affordanceKey = affordance;
                            //Debug.Log("KEYYYYY  " + toStringFloat(affordanceKey));
                            gesture = new RecordedGesture(param, propAffordanceNameLookup[toStringFloat(affordanceKey)], false);

                            var guid = System.Guid.NewGuid();

                            //// Insert into LiteDB
                            if (gesture == null)
                            {
                                Debug.Log("gesture is null");
                            }
                            recordedGestures.Insert(guid, gesture);
                            var gestureCoordinate = new GestureCoordinate(new Envelope(vals[0], vals[1], vals[0], vals[1]), guid, pretendActionLabel, pretendObjectLabel, propAffordanceNameLookup[toStringFloat(affordanceKey)]);

                            //// insert into RTree
                            //rTree = UnityEngine.Object.FindObjectOfType<Executive>().rTree;
                            rTree.Insert(gestureCoordinate);

                            //// insert into Episodic Memory
                            tMemory.Insert(guid, temporalIndex);
                            temporalIndex += 1;
                        }
                    }
                    else
                    {
                        throw new Exception("No gestures files exists in StreamingAssets Folder");
                    }

                }
            }
        }
        //return null;
    }

    //public class MyEqualityComparer : IEqualityComparer<float[]>
    //{
    //    public bool Equals(float[] x, float[] y)
    //    {
    //        if (x.Length != y.Length)
    //        {
    //            return false;
    //        }
    //        for (int i = 0; i < x.Length; i++)
    //        {
    //            if (x[i] != y[i])
    //            {
    //                return false;
    //            }
    //        }
    //        return true;
    //    }

    //    public int GetHashCode(float[] obj)
    //    {
    //        int result = 17;
    //        for (int i = 0; i < obj.Length; i++)
    //        {
    //            unchecked
    //            {
    //                result = result * 23 + (int)(obj[i]*375) + i;
    //            }
    //        }
    //        return result;
    //        //return System.Guid.NewGuid().GetHashCode();
    //    }
    //}

    //private static Dictionary<float[], string> propAffordanceNameLookup1 = new Dictionary<float[], string>(new MyEqualityComparer()) {
    //    {new float[]{0,0.3333333F,0,0,0.3333333F,0,0,0,0,0,0,0.3333333F,0.3333333F,0,0,0,0,0,0,0,0,0,0,0,0}, "Squid w Tentacles Partless"},
    //    {new float[]{0.6666667F,0,0,0,0,0,0,0.3333333F,0,0.3333333F,0,0.3333333F,0,0.3333333F,0,0,0,0,0.6666667F,0,0,0,0,0,0}, "Lolliop w Cube Ends Partless"},
    //    {new float[]{0,0.3333333F,0,0.3333333F,0,0,0,0.3333333F,0.3333333F,0,0,0,0,0.6666667F,0.3333333F,0,0,0,0.6666667F,0.3333333F,0,0,0,0,0}, "Giant U w Flat Base Partless"},
    //    {new float[]{0,0.3333333F,0.3333333F,0,0,0,0,0.3333333F,0.3333333F,0,0,0,0.3333333F,0.3333333F,0.3333333F,0,0,0,0.3333333F,0,0,0,0,0,0}, "Big Curved Axe Partless"},
    //    {new float[]{0,0.3333333F,0,0.3333333F,0,0,0,0,0,0.6666667F,0,0,0.3333333F,0,0.3333333F,0,0.3333333F,0,0.6666667F,0.3333333F,0,0,0,0,0}, "Sunflower Partless"},
    //    {new float[]{0,0,0,0.3333333F,0.3333333F,0,0,0,0,0.6666667F,0,0,0,0,0,0.3333333F,0,0,0.3333333F,0,0,0,0,0,0}, "Air Horn"},
    //    {new float[]{0,0.6666667F,0,0,0,0,0,0.3333333F,0.3333333F,0,0,0.3333333F,0,0.3333333F,0,0,0,0,0.6666667F,0,0,0,0,0,0}, "Corn Dog"},
    //    {new float[]{0.3333333F,0.6666667F,0,0,0,0,0,0.3333333F,0.3333333F,0.3333333F,0,0,0.3333333F,0.3333333F,0.3333333F,0,0,0,0,0.3333333F,0,0,0,0,0}, "Giant Electric Plug"},
    //    {new float[]{0,0.3333333F,0,0,0,0.3333333F,0,0,0.6666667F,0,0,0,0.3333333F,0,0.6666667F,0,0,0.3333333F,0,0,0,0,0.3333333F,0,0}, "Ring w Tail End"},
    //    {new float[]{0,0.3333333F,0,0,0,0.6666667F,0,0.3333333F,0.6666667F,0,0,0,0,1,1,0,0,0,0,0,0,0,0,0,0}, "Ring w Inward Spokes"},
    //    {new float[]{0,0.3333333F,0,0,0,0.3333333F,0,0.3333333F,0,0.3333333F,0,0,0.3333333F,0,0.3333333F,0,0,0,0,0,0,0.3333333F,0,0,0}, "Large Ring w Outward Spokes"},
    //    {new float[]{0,0.3333333F,0,0,0.3333333F,0,0,0.3333333F,0,0.3333333F,0,0,0.3333333F,0,0,0,0,0,0,0.3333333F,0,0,0.3333333F,0,0}, "Tube w Cones"},
    //    {new float[]{0,0.6666667F,0,0,0,0,0,0.6666667F,0,0,0,0.3333333F,0,0.3333333F,0,0,0,0,0.6666667F,0,0,0,0,0,0}, "Thin Stick w Hammer End"},
    //    {new float[]{0,0.3333333F,0,0,0.3333333F,0,0,0,0,0,0,0.3333333F,0.3333333F,0,0,0,0,0,0,0,0,0,0,0,0}, "Horseshoe"},
    //    {new float[]{0,0,0,0,0,0,0.3333333F,0,0.3333333F,0,0,0,0.3333333F,0,0,0,0,0,0,0,0,0,0.3333333F,0,0}, "Helix"},
    //    {new float[]{0,0,0,0.3333333F,0.3333333F,0,0,0,0.3333333F,0.3333333F,0,0,0.3333333F,0,0.3333333F,0,0.3333333F,0,0.6666667F,0,0,0,0,0,0}, "Giant Golf T"},
    //    {new float[]{0,0,0,0.3333333F,0,0.3333333F,0,0,0,0.6666667F,0,0,0,0,0.6666667F,0,0,0,0.6666667F,0,0,0,0,0,0}, "Circle With Ring"},
    //    {new float[]{0,0,0,0,0.6666667F,0,0,0,0.3333333F,0.3333333F,0,0.3333333F,0.3333333F,0,0,0,0,0,0.6666667F,0,0,0,0,0,0}, "Palm Tree Partless"},
    //    {new float[]{0,0.3333333F,0,0.3333333F,0,0,0,0.3333333F,0.3333333F,0,0,0,0.3333333F,0,0.3333333F,0,0.3333333F,0,0.6666667F,0,0,0,0,0,0}, "Ladle"},
    //    {new float[]{0,0.6666667F,0,0,0.3333333F,0,0,0,0.3333333F,0.3333333F,0,0,0,0,0,1,0,0,0.6666667F,0,0,0,0,0,0}, "Plunger"}
    //};

    private static Dictionary<string, string> propAffordanceNameLookup = new Dictionary<string, string> {
        {"0,0.3333333,0,0,0.3333333,0,0,0,0,0,0,0.3333333,0.3333333,0,0,0,0,0,0,0,0,0,0,0,0", "Squid w Tentacles Partless OR Horseshoe"},
        //{"0,0.3333333,0,0,0.3333333,0,0,0,0,0,0,0.3333333,0.3333333,0,0,0,0,0,0,0,0,0,0,0,0", "Horseshoe"},
        {"0.6666667,0,0,0,0,0,0,0.3333333,0,0.3333333,0,0.3333333,0,0.3333333,0,0,0,0,0.6666667,0,0,0,0,0,0", "Lolliop w Cube Ends Partless"},
        {"0,0.3333333,0,0.3333333,0,0,0,0.3333333,0.3333333,0,0,0,0,0.6666667,0.3333333,0,0,0,0.6666667,0.3333333,0,0,0,0,0", "Giant U w Flat Base Partless"},
        {"0,0.3333333,0.3333333,0,0,0,0,0.3333333,0.3333333,0,0,0,0.3333333,0.3333333,0.3333333,0,0,0,0.3333333,0,0,0,0,0,0", "Big Curved Axe Partless"},
        {"0,0.3333333,0,0.3333333,0,0,0,0,0,0.6666667,0,0,0.3333333,0,0.3333333,0,0.3333333,0,0.6666667,0.3333333,0,0,0,0,0", "Sunflower Partless"},
        {"0,0,0,0.3333333,0.3333333,0,0,0,0,0.6666667,0,0,0,0,0,0.3333333,0,0,0.3333333,0,0,0,0,0,0", "Air Horn"},
        {"0,0.6666667,0,0,0,0,0,0.3333333,0.3333333,0,0,0.3333333,0,0.3333333,0,0,0,0,0.6666667,0,0,0,0,0,0", "Corn Dog"},
        {"0.3333333,0.6666667,0,0,0,0,0,0.3333333,0.3333333,0.3333333,0,0,0.3333333,0.3333333,0.3333333,0,0,0,0,0.3333333,0,0,0,0,0", "Giant Electric Plug"},
        {"0,0.3333333,0,0,0,0.3333333,0,0,0.6666667,0,0,0,0.3333333,0,0.6666667,0,0,0.3333333,0,0,0,0,0.3333333,0,0", "Ring w Tail End"},
        {"0,0.3333333,0,0,0,0.6666667,0,0.3333333,0.6666667,0,0,0,0,1,1,0,0,0,0,0,0,0,0,0,0", "Ring w Inward Spokes"},
        {"0,0.3333333,0,0,0,0.3333333,0,0.3333333,0,0.3333333,0,0,0.3333333,0,0.3333333,0,0,0,0,0,0,0.3333333,0,0,0", "Large Ring w Outward Spokes"},
        {"0,0.3333333,0,0,0.3333333,0,0,0.3333333,0,0.3333333,0,0,0.3333333,0,0,0,0,0,0,0.3333333,0,0,0.3333333,0,0", "Tube w Cones"},
        {"0,0.6666667,0,0,0,0,0,0.6666667,0,0,0,0.3333333,0,0.3333333,0,0,0,0,0.6666667,0,0,0,0,0,0", "Thin Stick w Hammer End"},
        {"0,0,0,0,0,0,0.3333333,0,0.3333333,0,0,0,0.3333333,0,0,0,0,0,0,0,0,0,0.3333333,0,0", "Helix"},
        {"0,0,0,0.3333333,0.3333333,0,0,0,0.3333333,0.3333333,0,0,0.3333333,0,0.3333333,0,0.3333333,0,0.6666667,0,0,0,0,0,0", "Giant Golf T"},
        {"0,0,0,0.3333333,0,0.3333333,0,0,0,0.6666667,0,0,0,0,0.6666667,0,0,0,0.6666667,0,0,0,0,0,0", "Circle With Ring"},
        {"0,0,0,0,0.6666667,0,0,0,0.3333333,0.3333333,0,0.3333333,0.3333333,0,0,0,0,0,0.6666667,0,0,0,0,0,0", "Palm Tree Partless"},
        {"0,0.3333333,0,0.3333333,0,0,0,0.3333333,0.3333333,0,0,0,0.3333333,0,0.3333333,0,0.3333333,0,0.6666667,0,0,0,0,0,0", "Ladle"},
        {"0,0.6666667,0,0,0.3333333,0,0,0,0.3333333,0.3333333,0,0,0,0,0,0.3333333,0,0,0.6666667,0,0,0,0,0,0", "Plunger"}
        //{"0,0.6666667,0,0,0.3333333,0,0,0,0.3333333,0.3333333,0,0,0,0,0,1,0,0,0.6666667,0,0,0,0,0,0", "Plunger"}
    };

    //public void Start()
    //{
    //    rTree = UnityEngine.Object.FindObjectOfType<Executive>().rTree;
    //}

    private static float StringToFloat(String s)
    {
        return float.Parse(s);
    }
    //Convert.ToSingle(Math.Round((decimal)affordanceKey[1], 7))
    private static float[] RoundFloat(float[] v, int dec)
    {
        float[] answer = new float[v.Length];
        for (int i = 0; i < v.Length; i++)
        {
            answer[i] = Convert.ToSingle(Math.Round((decimal)v[i], dec));
        }
        return answer;
    }

    private static string toStringFloat(float[] f_arr)
    {
        String answer = "";
        float[] rounded = RoundFloat(f_arr, 7);
        for (int i = 0; i < f_arr.Length - 1; i++)
        {
            answer += rounded[i].ToString();
            answer += ",";
        }
        answer += rounded[f_arr.Length - 1].ToString();
        return answer;
    }

    private static T[] Concatenate1DArrays<T>(T[] a, T[] b)
    {
        T[] result = new T[a.Length + b.Length];
        Array.Copy(a, 0, result, 0, a.Length);
        Array.Copy(b, 0, result, a.Length, b.Length);
        return result;
    }

 //   public RecordedGesture loadAGesture()
 //   {
        
	//}

    //public RecordedGesture loadAGesture()
    //{
    //    if(param == null)
    //    {
    //        Debug.Log("param is null");
    //    }

    //    if (param != null)
    //    {
    //        gesture = new RecordedGesture(param, propAffordanceNameLookup[affordance], false);
    //    }
    //    var guid = System.Guid.NewGuid();

    //    //// Insert into LiteDB
    //    if (gesture == null)
    //    {
    //        Debug.Log("gesture is null");
    //    }
    //    recordedGestures.Insert(guid, gesture);
    //    var gestureCoordinate = new GestureCoordinate(new Envelope(vals[0], vals[1]), guid);

    //    //// insert into RTree
    //    rTree.Insert(gestureCoordinate);

    //    //// insert into Episodic Memory
    //    tMemory.Insert(guid, temporalIndex);
    //    temporalIndex += 1;

    //    // Create labels
    //    var id = gesture.id;
    //    var gestureDefinition = gesture.GetGesture();

    //    //Assign the labels for pretended action and pretended object.
    //    gestureDefinition.pretendActionName = KNNLabel(gestureCoordinate, 5, false, true);
    //    gestureDefinition.pretendObjectName = KNNLabel(gestureCoordinate, 5, false, false);
    //    return gesture;
    //}

    public string getPretendActionName()
    {
        return gesture.GetGesture().pretendActionName;
    }

    public string getPretendObjectName()
    {
        return gesture.GetGesture().pretendObjectName;
    }

    public static string KNNLabel(ISpatialData point, int k, bool weightedDistance = false, bool action = false)
    {
        List<ISpatialData> nearestNeighbors = rTree.KNearestNeighbor(point, k);
        string answer = "";
        if (weightedDistance == true)
        {
            var map = new Dictionary<string, double>();
            for (int i = 0; i < nearestNeighbors.Count; i++)
            {
                GestureCoordinate gc = (GestureCoordinate)nearestNeighbors[i];
                var label = gc.prop_label;
                if(action)
                {
                    label = gc.action_label;
                }
                if (map.ContainsKey(label))
                {
                    map[label] += 1 / (0.00000001 + Envelope.boxDistance(point.Envelope, gc.Envelope));
                }
                else
                {
                    map.Add(label, 1 / (0.00000001 + Envelope.boxDistance(point.Envelope, gc.Envelope)));
                }
                var mapList = map.ToList();
                mapList.Sort((pair1, pair2) => pair1.Value.CompareTo(pair2.Value));
                answer = mapList[0].Key;
            }
        }
        else
        {
            List<string> labels = new List<string>();
            for (int i = 0; i < nearestNeighbors.Count; i++)
            {
                GestureCoordinate gc = (GestureCoordinate)nearestNeighbors[i];
                if(action)
                {
                    labels.Add(gc.action_label);
                } else
                {
                    labels.Add(gc.prop_label);
                }   
            }
            var labelGroup = labels.GroupBy(x => x);
            var maxCount = labelGroup.Max(g => g.Count());
            var label = labelGroup.Where(x => x.Count() == maxCount).Select(x => x.Key).ToArray();
            answer = label[0];
        }

        return answer;
    }

    //public static void Save()
    //{
    //    using (var db = new LiteDatabase(_strConnection))
    //    {
    //        // Save RTree content
    //        var spatialIndecies = db.GetCollection<GestureCoordinate>("spatialData");
    //        Queue<ISpatialData> gQueue = new Queue<ISpatialData>();
    //        gQueue.Enqueue(rTree.root);
    //        var curr = gQueue.dequeue();
    //        while (curr.isLeaf() == false)
    //        {
    //            gQueue.enqueue(curr.children);
    //            curr = gQueue.dequeue();
    //        }
    //        while (gQueue.length != 0)
    //        {
    //            spatialIndecies.Insert(gQueue.dequeue());
    //            spatialIndecies.EnsureIndex(x => x.Guid);
    //        }

    //        // Save Episodic Memory
    //        var temporalIndecies = db.GetCollection<Node>("temporalData");
    //        var temp = tMemory.head;
    //        while(temp.next != null)
    //        {
    //            temporalIndecies.insert(temp);
    //            temp = temp.next;
    //        }
    //    }
    //}
}
