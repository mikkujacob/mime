﻿using System;
using System.Collections;
using System.Collections.Generic;
using TensorFlow;

/// <summary>
/// Class to load in a frozen TensorFlow graph and generate inferences from it.
/// </summary>
public class FrozenGraphLoader
{
	/// <summary>
	/// The TFGraph object.
	/// </summary>
	private TFGraph graph;

	/// <summary>
	/// The TFSession object.
	/// </summary>
	private TFSession session;

	/// <summary>
	/// The TFSession runner object.
	/// </summary>
	private TFSession.Runner runner;

//	/// <summary>
//	/// The random number generator.
//	/// </summary>
//	private System.Random random = new System.Random ();

	/// <summary>
	/// Initializes a new instance of the <see cref="FrozenGraphLoader"/> class.
	/// </summary>
	public FrozenGraphLoader() {}

	/// <summary>
	/// Initializes the model.
	/// </summary>
	/// <returns><c>true</c>, if model was initialized, <c>false</c> otherwise.</returns>
	/// <param name="modelBytes">A byte array representation of frozen model graph.</param>
	/// <param name="initializationData">Initialization data. Must be of correct types for each input node in model graph. No type checking is performed.</param>
	/// <param name="outputNodeName">Output node name to initialize results for future inference.</param>
	public bool InitializeModel (byte[] modelBytes, Dictionary<string, dynamic> initializationData, string outputNodeName)
	{
		dynamic outputTensor = null;
		try
		{
			graph = new TFGraph ();
			graph.Import (modelBytes);
			session = new TFSession (graph);
			runner = session.GetRunner ();
			foreach (KeyValuePair<string, dynamic> input in initializationData)
			{
				runner.AddInput (graph [input.Key] [0], input.Value);
			}
			runner.Fetch (graph[outputNodeName][0]);
			outputTensor = runner.Run () [0].GetValue ();
			if(outputTensor != null)
			{
				return true;
			}
		}
		catch(Exception)
		{
			return false;
		}
		return false;
	}

    /// <summary>
    /// Generates the output inference from the loaded graph.
    /// </summary>
    /// <returns>The output as an object that must be converted to the required type.</returns>
    /// <param name="generationInputs">Inputs for generating an inference.</param>
    /// <param name="outputNodeName">Name of the output node from which to draw an output/inference.</param>
    public dynamic GenerateOutput(Dictionary<string, dynamic> generationInputs, string outputNodeName)
    {
        runner = session.GetRunner();
        foreach (KeyValuePair<string, dynamic> input in generationInputs)
        {
            runner.AddInput(graph[input.Key][0], input.Value);
        }
        runner.Fetch(graph[outputNodeName][0]);
        return runner.Run()[0].GetValue();
    }
}
