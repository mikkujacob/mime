﻿using System.Collections;
using System.Collections.Generic;
using NewtonVR;
using RootMotion.FinalIK;
using UnityEngine;

public class AgentPlayer : Player {

    public RecordedGesture recording;

    private Executive executive;

    public string aa;

    PropManager propManager;
    private GameObject prop;
    private string propName;

    private bool isInit;
    private bool isPlaying = false;

    private GameObject IKTargetsObject;
    private Transform headTransform;
    private Transform pelvisTransform;
    private Transform leftHandTransform;
    private Transform rightHandTransform;

    private Transform initialRobotPartnerTransform;
    private Transform initialHeadTransform;
    private Transform initialPelvisTransform;
    private Transform initialLeftHandTransform;
    private Transform initialRightHandTransform;

    private GameObject IKObject;
    private VRIK IK;
    private Transform[] skeletalTransforms;

    private Vector3 partnerScale;
    private Vector3 partnerMovementOriginPosition;

    private GestureDefinition currentGesture;
    private bool busy;
    private int dataIndex = 0;
    private List<PlayerStateDefinition> frames = null;

    public bool objectPositionGenerated;
    public SpeechBubble speechBubble;
    // Use this for initialization
    void Start () {
        propManager = gameObject.GetComponent<PropManager>();

        executive = gameObject.GetComponent<Executive>();

        //prop = propManager.GetCurrentProp();
        //propName = propManager.GetCurrentPropName();

        isInit = false;
        InitializeTracking();

        objectPositionGenerated = false;
        speechBubble = new SpeechBubble();
    }

    void FixedUpdate()
    {
        if (isInit && isPlaying)
        {
            //need a way to tell when gesture is done
            //compare currentTimeSpan to gestureDuration in Recorded Gesture

            GestureDefinition tempGesture;
            if (!busy) {
                if (executive.OutputQueue.Count != 0)
                {
                    tempGesture = executive.ReadFromOutputQueue();
                    if (tempGesture != null)
                    {
                        if (!tempGesture.IsGestureTooShort)
                        {
                            currentGesture = tempGesture;
                            Debug.Log("GOT NEW GESTURE");
                        }
                    }
                }
            }

            if (currentGesture != null && !busy)
            {

                recording = new RecordedGesture(currentGesture);
                busy = true;
                recording.Play();

                frames = currentGesture.GetPlayerStateDefinitions();
                dataIndex = 0;
            }
            
            if (currentGesture != null && frames != null && busy && dataIndex < frames.Count)
            {
                PlayerStateDefinition currentPlayerState = frames[dataIndex];
                ApplyRecordedData(currentPlayerState);
                dataIndex++;

                // Gesture is done playing
                if (dataIndex >= frames.Count)
                {
                    currentGesture = null;
                    frames = null;
                    busy = false;
                }
            }
        }
    }

    void Update()
    {
        if (isInit)
        {
            HandleInput();
        }
    }

    /// <summary>
    /// Initializes the tracking.
    /// </summary>
    void InitializeTracking()
    {
        IKTargetsObject = GameObject.FindGameObjectWithTag("IKTargets");

        IKObject = GameObject.FindGameObjectWithTag("Performer");
        IK = IKObject.GetComponent<VRIK>();

        initialRobotPartnerTransform = IKObject.GetComponent<Transform>();

        partnerScale = initialRobotPartnerTransform.localScale;

        //Joints: [root, pelvis, spine, chest, neck, head, leftShoulder, leftUpperArm, leftForearm, leftHand, rightShoulder, rightUpperArm, rightForearm, rightHand, leftThigh, leftCalf, leftFoot, leftToes, rightThigh, rightCalf, rightFoot, rightToes]
        skeletalTransforms = IK.references.GetTransforms();

        partnerMovementOriginPosition = GameObject.Find("PartnerMovementWorldOrigin").GetComponent<Transform>().position;

        headTransform = IKTargetsObject.transform.Find("IKHead").transform;
        pelvisTransform = IKTargetsObject.transform.Find("IKPelvis").transform;
        leftHandTransform = IKTargetsObject.transform.Find("IKLeftHand").transform;
        rightHandTransform = IKTargetsObject.transform.Find("IKRightHand").transform;

        initialHeadTransform = GameObject.Find("InitialIKHead").GetComponent<Transform>();
        initialPelvisTransform = GameObject.Find("InitialIKPelvis").GetComponent<Transform>();
        initialLeftHandTransform = GameObject.Find("InitialIKLeftHand").GetComponent<Transform>();
        initialRightHandTransform = GameObject.Find("InitialIKRightHand").GetComponent<Transform>();

        Debug.Log("Tracking Initialized");

        isInit = true;
    }

    public void StartPlay()
    {
        prop = propManager.GetCurrentProp();
        propName = propManager.GetCurrentPropName();

        propManager.SpawnProp();
        if(objectPositionGenerated)
        {
            prop.transform.GetChild(0).gameObject.transform.position = recording.GetGesture().objectStartingPosition.ToVector3();
            prop.transform.GetChild(0).gameObject.transform.rotation = recording.GetGesture().objectStartingRotation.ToQuaternion();
            propManager.SetCurrentPropSize(propManager.GetCurrentPropSizeString());
        }
        else
        {
            prop.transform.GetChild(0).gameObject.GetComponent<MeshCollider>().enabled = false;
            prop.transform.GetChild(0).gameObject.GetComponent<Rigidbody>().isKinematic = true;

            Vector3 position = new Vector3(rightHandTransform.position.x, rightHandTransform.position.y, rightHandTransform.position.z);
            Quaternion rotation = new Quaternion(rightHandTransform.rotation.x, rightHandTransform.rotation.y, rightHandTransform.rotation.z, rightHandTransform.rotation.w);
            prop.transform.parent = rightHandTransform;
            prop.transform.localPosition = new Vector3();
            prop.transform.localRotation = new Quaternion();
            prop.transform.localScale = propManager.GetCurrentPropSize();
            prop.transform.GetChild(0).gameObject.transform.localPosition = new Vector3();
            prop.transform.GetChild(0).gameObject.transform.localRotation = new Quaternion();

            rightHandTransform.position = position;
            rightHandTransform.rotation = rotation;
        }
        /*recording = new RecordedGesture(executive.ReadFromOutputQueue());
        Debug.Log(recording);
        recording.Play();*/
        recording = null;
        busy = false;

        Debug.Log("Playback Started...");
        isPlaying = true;
        speechBubble.ChangeDisplayText();
        speechBubble.Speak();
      
    }

    public void StopPlay()
    {
        busy = true;
        isPlaying = false;
        recording.Stop();
        Reset();
        propManager.ResetProp();
        Debug.Log("Playback Stopped...");
        SpeechBubble.speechTextAudienceView.text = " ";
        SpeechBubble.speechTextVRView.text = " ";
    }

    public void Reset()
    {
        headTransform.position = initialHeadTransform.position;
        headTransform.rotation = initialHeadTransform.rotation;
        pelvisTransform.position = initialPelvisTransform.position;
        pelvisTransform.rotation = initialPelvisTransform.rotation;
        leftHandTransform.position = initialLeftHandTransform.position;
        leftHandTransform.rotation = initialLeftHandTransform.rotation;
        rightHandTransform.position = initialRightHandTransform.position;
        rightHandTransform.rotation = initialRightHandTransform.rotation;

        if(!objectPositionGenerated)
        {
            prop.transform.GetChild(0).gameObject.GetComponent<MeshCollider>().enabled = true;
            prop.transform.GetChild(0).gameObject.GetComponent<Rigidbody>().isKinematic = false;

            prop.transform.parent = propManager.GetPropsObject().transform;
            prop.transform.localPosition = new Vector3();
            prop.transform.localRotation = new Quaternion();
            prop.transform.localScale = propManager.GetCurrentPropSize();
            prop.transform.GetChild(0).gameObject.transform.localPosition = new Vector3();
            prop.transform.GetChild(0).gameObject.transform.localRotation = new Quaternion();

        }
    }

    //used in LevelManager to know if the agent is playing or not
    public bool IsPlaying()
    {
        return isPlaying;
    }

    public void SetPlaying(bool play)
    {
        isPlaying = play;
    }

    void ApplyRecordedData(PlayerStateDefinition nextState)
    {

        headTransform.position = Vector3.Scale(nextState.headPosition.ToVector3(), partnerScale) + partnerMovementOriginPosition;
        headTransform.rotation = nextState.headRotation.ToQuaternion();

        leftHandTransform.position = Vector3.Scale(nextState.leftHandPosition.ToVector3(), partnerScale) + partnerMovementOriginPosition;
        leftHandTransform.rotation = nextState.leftHandRotation.ToQuaternion();

        rightHandTransform.position = Vector3.Scale(nextState.rightHandPosition.ToVector3(), partnerScale) + partnerMovementOriginPosition;
        rightHandTransform.rotation = nextState.rightHandRotation.ToQuaternion();

        pelvisTransform.position = Vector3.Scale(nextState.skeletalPositions[1].ToVector3(), partnerScale) + partnerMovementOriginPosition;
        pelvisTransform.rotation = nextState.skeletalRotations[1].ToQuaternion();

        if (objectPositionGenerated && prop != null && prop.activeInHierarchy && nextState.objectPosition != null)
        {
            prop.transform.GetChild(0).gameObject.transform.position = nextState.objectPosition.ToVector3();
            prop.transform.GetChild(0).gameObject.transform.rotation = nextState.objectRotation.ToQuaternion();
        }
    }

    void HandleInput()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            ToggleActionPlayback();
        }
    }

    public void ToggleActionPlayback()
    {
        if (!isPlaying)
        {
            StartPlay();
        }
        else
        {
            StopPlay();
        }
    }

}
