﻿using System.Collections;
using System.Collections.Generic;
using System;
using NewtonVR;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class PropManager : MonoBehaviour
{
    private ParticleSystem poof;

    private string currentPropName;
    private GameObject propsObject;
    private GameObject currentProp;
    private string currentPropSize;

    private Dictionary<string, GameObject> propDictionary;

    private List<GameObject> propKeyList;
    private Transform propSpawnLocation;

    //set the sizes for the props as Small, Medium, and Large
    private Dictionary<string, Vector3> propSizes =
        new Dictionary<string, Vector3> { { "SMALL", new Vector3(0.25f, 0.25f, 0.25f) },
            { "MEDIUM", new Vector3(0.5f, 0.5f, 0.5f) },
            { "LARGE", new Vector3(1f, 1f, 1f) } };

    // Use this for initialization
    public void Start()
    {
        poof = GameObject.Find("SmokeSystem").GetComponent<ParticleSystem>();

        //find the location for props to generate
        propSpawnLocation = GameObject.Find("PropSpawnLocation").transform;
        //find the folder of props
        propsObject = GameObject.Find("Props");
        propsObject.SetActive(true);
        propKeyList = new List<GameObject>();
        //Add each of the props in the folder to the list
        GameObject[] gameObjects = GameObject.FindGameObjectsWithTag("ObjectStateTracker");
        InitializeInteractableProps(gameObjects);
        propKeyList.AddRange(gameObjects);
        //create dictionary in order to reference prop by name and get object
        propDictionary = new Dictionary<string, GameObject>();
        //add each of the props to the dictionary
        foreach (GameObject gameObj in propKeyList)
        {
            propDictionary.Add(gameObj.name, gameObj);
            gameObj.SetActive(false);
        }
    }

    public void InitializeInteractableProps(GameObject[] gameObjects)
    {

        foreach (GameObject gameObj in gameObjects)
        {
            if (gameObj.transform.childCount == 0)
            {
                continue;
            }

            // Allows the objects to follow Unity's physics engine
            if (gameObj.transform.GetChild(0).gameObject.GetComponent<Rigidbody>() == null)
            {
                gameObj.transform.GetChild(0).gameObject.AddComponent<Rigidbody>();
            }

            // Fixes the lighting on objects
            if (gameObj.transform.GetChild(0).gameObject.GetComponent<ReverseNormals>() == null)
            {
                gameObj.transform.GetChild(0).gameObject.AddComponent<ReverseNormals>();
            }

            // Enables more acurate collision on objects
            if (gameObj.transform.GetChild(0).gameObject.GetComponent<Collider>() == null)
            {
                gameObj.transform.GetChild(0).gameObject.AddComponent<MeshCollider>();
                gameObj.transform.GetChild(0).gameObject.GetComponent<MeshCollider>().convex = true;
                gameObj.transform.GetChild(0).gameObject.GetComponent<MeshCollider>().inflateMesh = true;
                gameObj.transform.GetChild(0).gameObject.GetComponent<MeshCollider>().skinWidth = 0.01f;
            }

            // Makes object interactable with VR controllers
            if (gameObj.transform.GetChild(0).gameObject.GetComponent<NVRInteractableItem>() == null)
            {
                gameObj.transform.GetChild(0).gameObject.AddComponent<NVRInteractableItem>();
            }
        }
    }

    // Update is called once per frame
    public void Update()
    {

    }

    public void GenerateProp()
    {
        //pick a random prop from the list
        int index = Random.Range(0, propKeyList.Count);
        currentPropName = propKeyList[index].name;
        currentProp = propDictionary[currentPropName];
        SetPropProperties();
    }

    public void SpawnProp()
    {
        if(currentProp != null)
        {
            currentProp.SetActive(true);
            currentProp.transform.position = propSpawnLocation.position;
            currentProp.transform.rotation = propSpawnLocation.transform.rotation;
        }
        else
        {
            Debug.Log("currentProp is null");
        }
    }

    public void ResetProp() //TODO: @Lauren, Redundant code? Can this be replaced by a call to SpawnProp()?
    {
        if(currentProp != null)
        {
            currentProp.transform.position = propSpawnLocation.position;
            currentProp.transform.rotation = propSpawnLocation.transform.rotation;
            currentProp.SetActive(false);
        }
        else
        {
            Debug.Log("currentProp is null");
        }
    }

    public ParticleSystem GetPoof()
    {
        return poof;
    }

    public GameObject GetCurrentProp()
    {
        return currentProp;
    }

    public string GetCurrentPropName()
    {
        return currentPropName;
    }

    public void SetPropName(string newName)
    {
        currentPropName = newName;
    }

    public Vector3 GetCurrentPropSize()
    {
        return propSizes[currentPropSize];
    }

    public string GetCurrentPropSizeString()
    {
        return currentPropSize;
    }

    public void SetCurrentPropSize(string size)
    {
        currentProp.transform.GetChild(0).localScale = propSizes[size];
    }

    private void SetPropProperties()
    {
        currentProp.transform.GetChild(0).gameObject.GetComponent<Renderer>().material.SetColor("_Color", Random.ColorHSV(0f, 1f, 0.9f, 1f, 0.7f, 1f, 0.999f, 1f));

        List<string> propSizes = new List<string>{"SMALL", "MEDIUM", "LARGE"};
        int sizeIndex = Random.Range(0, propSizes.Count);
        currentPropSize = propSizes[sizeIndex];
        SetCurrentPropSize(currentPropSize);
    }

    public GameObject GetPropsObject()
    {
        return propsObject;
    }
}
