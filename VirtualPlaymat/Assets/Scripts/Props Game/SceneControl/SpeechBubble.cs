﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Crosstales.RTVoice;
using UnityEngine.UI;
public class SpeechBubble : MonoBehaviour {

    public string VoiceName;
    public static Text speechTextAudienceView;
    public static Text speechTextVRView;
    private string pretendActionText;
    private string pretendObjectText;
    private AgentPlayer playback;

    public void Start()
    {
    }
    
    // Update the position of the speech bubble after the Agent updates its position.
    void LateUpdate () {
        transform.position = GameObject.FindGameObjectWithTag("Agent").transform.position;
        transform.position = transform.position + new Vector3(0.315008f, 4.03f, 1.443972f);
    }

    public void ChangeDisplayText()
    {
        if (playback == null)
        {
            playback = GameObject.FindGameObjectWithTag("MImEGlobalObject").GetComponent<AgentPlayer>();

        }

        if (speechTextAudienceView == null || speechTextVRView == null)
        {
            speechTextAudienceView = GameObject.FindGameObjectWithTag("SpeechTextAudienceView").GetComponent<Text>();
            speechTextVRView = GameObject.FindGameObjectWithTag("SpeechTextVRView").GetComponent<Text>();

        }
        //Debug.Log("SPPECHTEXT "+ speechText.text);
        speechTextVRView.text = "I " + playback.recording.GetGesture().pretendActionName  + " with " + playback.recording.GetGesture().pretendObjectName;
        speechTextAudienceView.text = speechTextVRView.text;

    }
    public void Speak()
    {
        if(speechTextVRView != null)
        {
            Speaker.Speak(speechTextVRView.text, null, Speaker.VoiceForName(VoiceName));
        }
    }
}
