﻿using System;
using System.Collections;
using System.Collections.Generic;
using NewtonVR;
using RootMotion.FinalIK;
using UnityEngine;

/// <summary>
/// Main class for recording the human player's gestures
/// </summary>
public class PlayerStateRecording : MonoBehaviour
{
	//GUITextControl textControl;
	private bool spaceTogglesRecording = false;

	private bool isInit;
	private bool isRecording;

	private SessionDefinition session;
    private GestureDefinition gesture = new GestureDefinition();
    private PlayerStateDefinition state;
    private Executive executive;

	private GameObject playerObject;
	private Transform headTransform;
	private Transform leftHandTransform;
	private Transform rightHandTransform;
	private NVRHand leftHand;
	private NVRHand rightHand;

	private GameObject IKObject;
	private VRIK IK;
	private Transform[] skeletalTransforms;

    private GameObject currentProp;

    //private GeneratedActionPlayback playback;
    private PropManager propManager;

	private JobQueue<InsertDataJob> queue;

	/// <summary>
	/// Start this instance.
	/// </summary>
	void Start ()
	{
		isInit = false;
		session = new SessionDefinition ();
		state = new PlayerStateDefinition ();

        //textControl = GameObject.Find ("Text").GetComponent<GUITextControl> ();

        //playback = GameObject.FindGameObjectWithTag ("MImEGlobalObject").GetComponent<GeneratedActionPlayback> ();
        propManager = gameObject.GetComponent<PropManager>();

        executive = gameObject.GetComponent<Executive>();


        //		Debug.Log ("Started");
        //		InitializeDatabase ();
        //		Invoke ("InitializeDatabase", 2);

        queue = new JobQueue<InsertDataJob> (2);

		Invoke ("InitializeTracking", 2);
	}

	/// <summary>
	/// Initializes the tracking.
	/// </summary>
	void InitializeTracking ()
	{
		playerObject = GameObject.FindGameObjectWithTag("NVR Player");
        leftHand = playerObject.transform.Find ("LeftHand").GetComponent<NVRHand> ();
		rightHand = playerObject.transform.Find ("RightHand").GetComponent<NVRHand> ();
		headTransform = playerObject.transform.Find ("Head").transform;
		leftHandTransform = playerObject.transform.Find ("LeftHand").transform;
		rightHandTransform = playerObject.transform.Find ("RightHand").transform;

		IKObject = GameObject.FindGameObjectWithTag ("Performer");
		IK = IKObject.GetComponent<VRIK> ();
		//Joints: [root, pelvis, spine, chest, neck, head, leftShoulder, leftUpperArm, leftForearm, leftHand, rightShoulder, rightUpperArm, rightForearm, rightHand, leftThigh, leftCalf, leftFoot, leftToes, rightThigh, rightCalf, rightFoot, rightToes]
		skeletalTransforms = IK.references.GetTransforms ();

		Debug.Log ("Tracking Initialized");
		//textControl.SetTimedMessage ("Tracking Initialized", 1);

		isInit = true;

		//if (!spaceTogglesRecording)
		//{
		//	ToggleRecording ();

		//	InvokeRepeating ("SplitRecording", 30, 30);
		//}
	}

	/// <summary>
	/// Fixed update method.
	/// </summary>
	void FixedUpdate ()
	{
		if (isInit)
		{
			HandleInput ();
			queue.Update ();
		}

		if (isInit && isRecording)
		{

			Vector3Definition[] jointPositions = new Vector3Definition[22];
			Vector4Definition[] jointRotations = new Vector4Definition[22];
			for (int i = 0; i < 22; i++)
			{
				if (skeletalTransforms == null || skeletalTransforms [i] == null)
				{
					jointPositions [i] = new Vector3Definition ();
					jointRotations [i] = new Vector4Definition ();
					continue;
				}

				jointPositions [i] = new Vector3Definition (skeletalTransforms [i].position);
				jointRotations [i] = new Vector4Definition (skeletalTransforms [i].rotation);
			}

			state = new PlayerStateDefinition ();
			state.timestamp = System.DateTime.Now.ToString ("yyyy-MM-dd-HH-mm-ss-fff");
			state.headPosition = new Vector3Definition (headTransform.position);
			state.headRotation = new Vector4Definition (headTransform.rotation);
			state.leftHandPosition = new Vector3Definition (leftHandTransform.position);
			state.leftHandRotation = new Vector4Definition (leftHandTransform.rotation);
			state.rightHandPosition = new Vector3Definition (rightHandTransform.position);
			state.rightHandRotation = new Vector4Definition (rightHandTransform.rotation);

			state.objectPosition = new Vector3Definition(currentProp.transform.GetChild (0).gameObject.transform.position);
			state.objectRotation = new Vector4Definition(currentProp.transform.GetChild (0).gameObject.transform.rotation);

			state.leftControllerGripButtonDown = leftHand.HoldButtonDown;
			state.leftControllerTriggerButtonDown = leftHand.UseButtonDown;
			state.rightControllerGripButtonDown = rightHand.HoldButtonDown;
			state.rightControllerTriggerButtonDown = rightHand.UseButtonDown;

			state.leftControllerGripButtonUp = leftHand.HoldButtonUp;
			state.leftControllerTriggerButtonUp = leftHand.UseButtonUp;
			state.rightControllerGripButtonUp = rightHand.HoldButtonUp;
			state.rightControllerTriggerButtonUp = rightHand.UseButtonUp;
				
			state.skeletalPositions = jointPositions;
			state.skeletalRotations = jointRotations;

			gesture.AddState (state);

//			Debug.Log ("State: " + JsonUtility.ToJson (state));
		}
	}

	//TODO: MOVE TO CENTRAL KEYCONTROLLER
	void HandleInput()
	{
		if (spaceTogglesRecording && Input.GetKeyUp (KeyCode.Space))
		{
			ToggleRecording ();
		}

//		if (!spaceTogglesRecording && (Input.GetKeyDown (KeyCode.RightArrow) || rightHand.UseButtonDown))
		//if (!spaceTogglesRecording && Input.GetKeyDown (KeyCode.RightArrow))
		//{
		//	ToggleRecording ();
		//	if (!isRecording)
		//	{
 //              controller.NextObject();
		//		ToggleRecording ();
		//	}
		//}

		//if (!spaceTogglesRecording && Input.GetKeyDown (KeyCode.KeypadMinus))
		//{
		//	ToggleRecording ();
		//}
	}

    /// <summary>
    /// Stops current recording and starts a new one.
    /// </summary>
	public void SplitRecording()
	{
		if (!spaceTogglesRecording && isRecording)
		{
			ToggleRecording ();
			if (!isRecording)
			{
				ToggleRecording ();
			}
		}
	}

    /// <summary>
    /// Begins recording the player's actions
    /// </summary>
    public void StartRecording()
    {
        if (!isRecording)
        {
            gesture = new GestureDefinition();
            currentProp = propManager.GetCurrentProp();
            gesture.SetName(propManager.GetCurrentPropName());
            gesture.objectSize = propManager.GetCurrentPropSizeString();
            gesture.objectStartingPosition = new Vector3Definition(currentProp.transform.GetChild(0).gameObject.transform.position);
            gesture.objectStartingRotation = new Vector4Definition(currentProp.transform.GetChild(0).gameObject.transform.rotation);


            Debug.Log("Recording Started...");
            //textControl.SetTimedMessage ("Recording Started...", 1);
            isRecording = true;
        }
    }

    /// <summary>
    /// Stops the current recording and adds recording to the job queue
    /// </summary>
    public void StopRecording()
    {
        if (isRecording)
        {
            isRecording = false;

            Debug.Log("Recording Stopped...");
            //textControl.SetTimedMessage ("Recording stopped...", 1);


            session.AddGestureTimeStamp(gesture.timestamp);

            queue.AddJob(new InsertDataJob(gesture, Application.streamingAssetsPath));
            //Debug.Log("Number of active jobs: " + queue.CountActiveJobs());
            gesture.SetDuration();
            executive.AddNewGesture(gesture);
            Debug.Log(gesture); 
            //gesture = new GestureDefinition();
        }
    }

    /// <summary>
    /// Starts recording if the value isRecording is true, otherwise stops
    /// </summary>
	public void ToggleRecording()
	{
		if (!isRecording)
		{
			//gesture = new GestureDefinition ();
			gesture.SetName(propManager.GetCurrentPropName());
			gesture.objectSize = propManager.GetCurrentPropSizeString();
			gesture.objectStartingPosition = new Vector3Definition(currentProp.transform.GetChild (0).gameObject.transform.position);
			gesture.objectStartingRotation = new Vector4Definition(currentProp.transform.GetChild (0).gameObject.transform.rotation);


			Debug.Log ("Recording Started...");
			//textControl.SetTimedMessage ("Recording Started...", 1);
			isRecording = true;
		}
		else
		{
			isRecording = false;

			Debug.Log ("Recording Stopped...");
			//textControl.SetTimedMessage ("Recording stopped...", 1);


			session.AddGestureTimeStamp (gesture.timestamp);

			queue.AddJob (new InsertDataJob(gesture, Application.streamingAssetsPath));
            //Debug.Log ("Number of active jobs: " + queue.CountActiveJobs ());
            executive.AddNewGesture(gesture);
            Debug.Log(gesture);
            //gesture = new GestureDefinition();
        }
	}

	void OnApplicationQuit()
	{
//		var db = NoSqliteScript.Get ("propsgame");
		if (isInit && isRecording)
		{
			session.AddGestureTimeStamp (gesture.timestamp);

			queue.AddJob (new InsertDataJob(gesture, Application.streamingAssetsPath));
//			Debug.Log ("Gesture: " + gesture.Serialize ());

			Debug.Log ("Recording Stopped...");
			//textControl.SetTimedMessage ("Recording stopped...", 1);

			isRecording = false;
		}

		if (gesture == null || gesture.playerStates.Count == 0)	
		{
			return;
		}

		System.IO.File.WriteAllText (System.IO.Path.Combine (Application.streamingAssetsPath, System.DateTime.Now.ToString ("yyyy-MM-dd-HH-mm-ss-fff") + ".session"), session.Serialize ());
//		Debug.Log ("Session: " + JsonUtility.ToJson (session));

		//Debug.Log ("Number of active jobs: " + queue.CountActiveJobs ());
//		while (queue.CountActiveJobs () > 0)
//		{
//			queue.Update ();
//		}
	}

	public class InsertDataJob : JobItem
	{
		string path;
		GestureDefinition gesture;

		public InsertDataJob(GestureDefinition gesture, string path)
		{
			this.gesture = gesture;
			this.path = path;
		}

		protected override void DoWork()
		{
			//DateTime startTime = DateTime.ParseExact (gesture.timestamp, "yyyy-MM-dd-HH-mm-ss-fff", System.Globalization.CultureInfo.InvariantCulture);
			//System.IO.File.WriteAllText (System.IO.Path.Combine (path, startTime.ToString ("yyyy-MM-dd-HH-mm-ss-fff") + ".gesture"), gesture.Serialize ());
		}
	}
}
