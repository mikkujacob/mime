﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;

/// <summary>
/// The main class for calculating the latent space value of a gesture.
/// </summary>
public class AnalyzeDataSetLatentSpace : MonoBehaviour
{
    public TextAsset graphAsset;

    public string fileListFileName;
    public string timeseriesFileName;
    public string labelsFileName;
    private StreamReader timeseriesReader;
    private StreamReader labelsReader;
    public bool isFileList = false;
    private List<string> gestureFileNames;
    private int currentFileIndex = 0;

    private FrozenGraphLoader model = new FrozenGraphLoader();
    private bool isInit = false;
    private bool generate = false;
    private System.Random random = new System.Random();

    public int nInputs = 27000;

    private Dictionary<string, float[]> propNameAffordanceLookup = new Dictionary<string, float[]> {
        {"Squid w Tentacles Partless", new float[]{0,0.3333333F,0,0,0.3333333F,0,0,0,0,0,0,0.3333333F,0.3333333F,0,0,0,0,0,0,0,0,0,0,0,0}},
        {"Lolliop w Cube Ends Partless", new float[]{0.6666667F,0,0,0,0,0,0,0.3333333F,0,0.3333333F,0,0.3333333F,0,0.3333333F,0,0,0,0,0.6666667F,0,0,0,0,0,0}},
        {"Giant U w Flat Base Partless", new float[]{0,0.3333333F,0,0.3333333F,0,0,0,0.3333333F,0.3333333F,0,0,0,0,0.6666667F,0.3333333F,0,0,0,0.6666667F,0.3333333F,0,0,0,0,0}},
        {"Big Curved Axe Partless", new float[]{0,0.3333333F,0.3333333F,0,0,0,0,0.3333333F,0.3333333F,0,0,0,0.3333333F,0.3333333F,0.3333333F,0,0,0,0.3333333F,0,0,0,0,0,0}},
        {"Sunflower Partless", new float[]{0,0.3333333F,0,0.3333333F,0,0,0,0,0,0.6666667F,0,0,0.3333333F,0,0.3333333F,0,0.3333333F,0,0.6666667F,0.3333333F,0,0,0,0,0}},
        {"Air Horn", new float[]{0,0,0,0.3333333F,0.3333333F,0,0,0,0,0.6666667F,0,0,0,0,0,0.3333333F,0,0,0.3333333F,0,0,0,0,0,0}},
        {"Corn Dog", new float[]{0,0.6666667F,0,0,0,0,0,0.3333333F,0.3333333F,0,0,0.3333333F,0,0.3333333F,0,0,0,0,0.6666667F,0,0,0,0,0,0}},
        {"Giant Electric Plug", new float[]{0.3333333F,0.6666667F,0,0,0,0,0,0.3333333F,0.3333333F,0.3333333F,0,0,0.3333333F,0.3333333F,0.3333333F,0,0,0,0,0.3333333F,0,0,0,0,0}},
        {"Ring w Tail End", new float[]{0,0.3333333F,0,0,0,0.3333333F,0,0,0.6666667F,0,0,0,0.3333333F,0,0.6666667F,0,0,0.3333333F,0,0,0,0,0.3333333F,0,0}},
        {"Ring w Inward Spokes", new float[]{0,0.3333333F,0,0,0,0.6666667F,0,0.3333333F,0.6666667F,0,0,0,0,1,1,0,0,0,0,0,0,0,0,0,0}},
        {"Large Ring w Outward Spokes", new float[]{0,0.3333333F,0,0,0,0.3333333F,0,0.3333333F,0,0.3333333F,0,0,0.3333333F,0,0.3333333F,0,0,0,0,0,0,0.3333333F,0,0,0}},
        {"Tube w Cones", new float[]{0,0.3333333F,0,0,0.3333333F,0,0,0.3333333F,0,0.3333333F,0,0,0.3333333F,0,0,0,0,0,0,0.3333333F,0,0,0.3333333F,0,0}},
        {"Thin Stick w Hammer End", new float[]{0,0.6666667F,0,0,0,0,0,0.6666667F,0,0,0,0.3333333F,0,0.3333333F,0,0,0,0,0.6666667F,0,0,0,0,0,0}},
        {"Horseshoe", new float[]{0,0.3333333F,0,0,0.3333333F,0,0,0,0,0,0,0.3333333F,0.3333333F,0,0,0,0,0,0,0,0,0,0,0,0}},
        {"Helix", new float[]{0,0,0,0,0,0,0.3333333F,0,0.3333333F,0,0,0,0.3333333F,0,0,0,0,0,0,0,0,0,0.3333333F,0,0}},
        {"Giant Golf T", new float[]{0,0,0,0.3333333F,0.3333333F,0,0,0,0.3333333F,0.3333333F,0,0,0.3333333F,0,0.3333333F,0,0.3333333F,0,0.6666667F,0,0,0,0,0,0}},
        {"Circle With Ring", new float[]{0,0,0,0.3333333F,0,0.3333333F,0,0,0,0.6666667F,0,0,0,0,0.6666667F,0,0,0,0.6666667F,0,0,0,0,0,0}},
        {"Palm Tree Partless", new float[]{0,0,0,0,0.6666667F,0,0,0,0.3333333F,0.3333333F,0,0.3333333F,0.3333333F,0,0,0,0,0,0.6666667F,0,0,0,0,0,0}},
        {"Ladle", new float[]{0,0.3333333F,0,0.3333333F,0,0,0,0.3333333F,0.3333333F,0,0,0,0.3333333F,0,0.3333333F,0,0.3333333F,0,0.6666667F,0,0,0,0,0,0}},
        {"Plunger", new float[]{0,0.6666667F,0,0,0.3333333F,0,0,0,0.3333333F,0.3333333F,0,0,0,0,0,1,0,0,0.6666667F,0,0,0,0,0,0}}
    };
    private Dictionary<string, float[]> propDefaultNameAffordanceLookup = new Dictionary<string, float[]> {
        {"NULL", new float[]{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}}
    };

    private Dictionary<float[], string> propAffordanceNameLookup = new Dictionary<float[], string> {
        {new float[]{0,0.3333333F,0,0,0.3333333F,0,0,0,0,0,0,0.3333333F,0.3333333F,0,0,0,0,0,0,0,0,0,0,0,0}, "Squid w Tentacles Partless"},
        {new float[]{0.6666667F,0,0,0,0,0,0,0.3333333F,0,0.3333333F,0,0.3333333F,0,0.3333333F,0,0,0,0,0.6666667F,0,0,0,0,0,0}, "Lolliop w Cube Ends Partless"},
        {new float[]{0,0.3333333F,0,0.3333333F,0,0,0,0.3333333F,0.3333333F,0,0,0,0,0.6666667F,0.3333333F,0,0,0,0.6666667F,0.3333333F,0,0,0,0,0}, "Giant U w Flat Base Partless"},
        {new float[]{0,0.3333333F,0.3333333F,0,0,0,0,0.3333333F,0.3333333F,0,0,0,0.3333333F,0.3333333F,0.3333333F,0,0,0,0.3333333F,0,0,0,0,0,0}, "Big Curved Axe Partless"},
        {new float[]{0,0.3333333F,0,0.3333333F,0,0,0,0,0,0.6666667F,0,0,0.3333333F,0,0.3333333F,0,0.3333333F,0,0.6666667F,0.3333333F,0,0,0,0,0}, "Sunflower Partless"},
        {new float[]{0,0,0,0.3333333F,0.3333333F,0,0,0,0,0.6666667F,0,0,0,0,0,0.3333333F,0,0,0.3333333F,0,0,0,0,0,0}, "Air Horn"},
        {new float[]{0,0.6666667F,0,0,0,0,0,0.3333333F,0.3333333F,0,0,0.3333333F,0,0.3333333F,0,0,0,0,0.6666667F,0,0,0,0,0,0}, "Corn Dog"},
        {new float[]{0.3333333F,0.6666667F,0,0,0,0,0,0.3333333F,0.3333333F,0.3333333F,0,0,0.3333333F,0.3333333F,0.3333333F,0,0,0,0,0.3333333F,0,0,0,0,0}, "Giant Electric Plug"},
        {new float[]{0,0.3333333F,0,0,0,0.3333333F,0,0,0.6666667F,0,0,0,0.3333333F,0,0.6666667F,0,0,0.3333333F,0,0,0,0,0.3333333F,0,0}, "Ring w Tail End"},
        {new float[]{0,0.3333333F,0,0,0,0.6666667F,0,0.3333333F,0.6666667F,0,0,0,0,1,1,0,0,0,0,0,0,0,0,0,0}, "Ring w Inward Spokes"},
        {new float[]{0,0.3333333F,0,0,0,0.3333333F,0,0.3333333F,0,0.3333333F,0,0,0.3333333F,0,0.3333333F,0,0,0,0,0,0,0.3333333F,0,0,0}, "Large Ring w Outward Spokes"},
        {new float[]{0,0.3333333F,0,0,0.3333333F,0,0,0.3333333F,0,0.3333333F,0,0,0.3333333F,0,0,0,0,0,0,0.3333333F,0,0,0.3333333F,0,0}, "Tube w Cones"},
        {new float[]{0,0.6666667F,0,0,0,0,0,0.6666667F,0,0,0,0.3333333F,0,0.3333333F,0,0,0,0,0.6666667F,0,0,0,0,0,0}, "Thin Stick w Hammer End"},
        {new float[]{0,0.3333333F,0,0,0.3333333F,0,0,0,0,0,0,0.3333333F,0.3333333F,0,0,0,0,0,0,0,0,0,0,0,0}, "Horseshoe"},
        {new float[]{0,0,0,0,0,0,0.3333333F,0,0.3333333F,0,0,0,0.3333333F,0,0,0,0,0,0,0,0,0,0.3333333F,0,0}, "Helix"},
        {new float[]{0,0,0,0.3333333F,0.3333333F,0,0,0,0.3333333F,0.3333333F,0,0,0.3333333F,0,0.3333333F,0,0.3333333F,0,0.6666667F,0,0,0,0,0,0}, "Giant Golf T"},
        {new float[]{0,0,0,0.3333333F,0,0.3333333F,0,0,0,0.6666667F,0,0,0,0,0.6666667F,0,0,0,0.6666667F,0,0,0,0,0,0}, "Circle With Ring"},
        {new float[]{0,0,0,0,0.6666667F,0,0,0,0.3333333F,0.3333333F,0,0.3333333F,0.3333333F,0,0,0,0,0,0.6666667F,0,0,0,0,0,0}, "Palm Tree Partless"},
        {new float[]{0,0.3333333F,0,0.3333333F,0,0,0,0.3333333F,0.3333333F,0,0,0,0.3333333F,0,0.3333333F,0,0.3333333F,0,0.6666667F,0,0,0,0,0,0}, "Ladle"},
        {new float[]{0,0.6666667F,0,0,0.3333333F,0,0,0,0.3333333F,0.3333333F,0,0,0,0,0,1,0,0,0.6666667F,0,0,0,0,0,0}, "Plunger"}
    };
    private Dictionary<float[], string> propDefaultAffordanceNameLookup = new Dictionary<float[], string> {
        {new float[]{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, "NULL"}
    };

    private float[] initialRandomVariables;

    private List<List<float>> latentSpacePoints = new List<List<float>>();

    private bool startWritingResults = false;

    /// <summary>
    /// Start this instance.
    /// </summary>
    public void Start()
    {
        initialRandomVariables = new float[nInputs];

        if (isFileList)
        {
            string[] lines = System.IO.File.ReadAllLines(System.IO.Path.Combine(Application.streamingAssetsPath, fileListFileName));
            gestureFileNames = new List<string>(lines[0].Split(new char[] { ',' }));
        }
        else
        {
            timeseriesReader = new StreamReader(System.IO.Path.Combine(Application.streamingAssetsPath, timeseriesFileName));
            labelsReader = new StreamReader(System.IO.Path.Combine(Application.streamingAssetsPath, labelsFileName));
        }

        float[,] param = new float[1, initialRandomVariables.Length + propDefaultNameAffordanceLookup["NULL"].Length];
        float[] values = Concatenate1DArrays<float>(initialRandomVariables, propDefaultNameAffordanceLookup["NULL"]);
        for (int i = 0; i < values.Length; i++)
        {
            param[0, i] = values[i];
        }
        Dictionary<string, dynamic> initInputs = new Dictionary<string, dynamic>() {
            { "concat", param},
            { "keep_prob", 1.0F }
        };
        string outputNodeName = "encoder/dense/BiasAdd";
        isInit = model.InitializeModel(graphAsset.bytes, initInputs, outputNodeName);
        if (isInit)
        {
            Debug.Log("Model initialized");
        }
        else
        {
            Debug.Log("Model not initialized");
        }
    }

    /// <summary>
    /// Update this instance.
    /// </summary>
    public void Update()
    {
        if (isInit)
        {
            try
            {
                float[] inputTimeSeries = null;
                float[] affordance = null;
                if (isFileList)
                {
                    string gestureContent = System.IO.File.ReadAllText(gestureFileNames[currentFileIndex++]);
                    GestureDefinition gesture = new GestureDefinition();
                    gesture = (GestureDefinition)(gesture.Deserialize(gestureContent));
                    GestureVector vector = new GestureVector(gesture, 0, true, true, nInputs, 0);
                    inputTimeSeries = vector.To1DFloat();
                    affordance = propNameAffordanceLookup[gesture.objectName];
                }
                else
                {
                    inputTimeSeries = Array.ConvertAll<string, float>(timeseriesReader.ReadLine().Split(','), StringToFloat);
                    affordance = Array.ConvertAll<string, float>(labelsReader.ReadLine().Split(','), StringToFloat);
                }
                //string propName = propAffordanceNameLookup[affordance];
                //Debug.Log("Prop Name: " + propName);
                float[,] param = new float[1, inputTimeSeries.Length + affordance.Length];
                float[] values = Concatenate1DArrays<float>(inputTimeSeries, affordance);
                for (int i = 0; i < values.Length; i++)
                {
                    param[0, i] = values[i];
                }
                //string input = Rank2ArrayToString<float>(param, ", ");
                Dictionary<string, dynamic> generatorInputs = new Dictionary<string, dynamic>() {
                    { "concat", param },
                    { "keep_prob", 1.0F }
                };

                // Generating output inference
                float[,] outputMeans = model.GenerateOutput(generatorInputs, "encoder/dense/BiasAdd") as float[,];
                Debug.Log("Result successfully obtained. Results in form: " + outputMeans.GetLength(0) + "x" + outputMeans.GetLength(1));
                float[,] outputSTDevs = model.GenerateOutput(generatorInputs, "encoder/dense_2/BiasAdd") as float[,];
                Debug.Log("Result successfully obtained. Results in form: " + outputSTDevs.GetLength(0) + "x" + outputSTDevs.GetLength(1));

                // Finding the input's latent space value
                List<float> vals = new List<float>();
                foreach (float val in outputMeans)
                {
                    Debug.Log("" + val);
                    vals.Add(val);
                }
                foreach (float val in outputSTDevs)
                {
                    Debug.Log("" + val);
                    vals.Add(val);
                }
                vals.AddRange(affordance);
                latentSpacePoints.Add(vals);
                Debug.Log("Got latent space value for input.");
            }
            catch (Exception e)
            {
                Debug.Log("Exception or File Read Finished.");
                isInit = false;
                if (latentSpacePoints.Count > 0)
                {
                    startWritingResults = true;
                }
            }
        }

        if (startWritingResults)
        {
            System.IO.File.WriteAllText(System.IO.Path.Combine(Application.streamingAssetsPath, System.DateTime.Now.ToString("yyyy-MM-dd-HH-mm-ss-fff") + ".points"), ToString(latentSpacePoints, ",", "\n"));
            startWritingResults = false;
        }
    }

    private string ToString(List<List<float>> floatListList, string itemSeparator, string lineSeparator)
    {
        string result = "";
        foreach (List<float> floatList in floatListList)
        {
            foreach (float value in floatList)
            {
                result += "" + value + itemSeparator;
            }

            result = result.Substring(0, result.Length - itemSeparator.Length) + lineSeparator;
        }

        return result.Substring(0, result.Length - lineSeparator.Length);
    }

    private static float StringToFloat(String s)
    {
        return float.Parse(s);
    }

    private string Rank2ArrayToString<T>(T[,] array, string separator)
    {
        string result = "[";
        foreach (T cell in array)
        {
            result += "" + cell.ToString() + separator;
        }
        return result.Substring(0, result.Length - separator.Length) + "]";
    }

    /// <summary>
    /// Handle input.
    /// </summary>
    private void HandleInput()
    {
        if (Input.GetKeyDown(KeyCode.Return))
        {
            generate = true;
        }
    }

    /// <summary>
    /// Concatenates the 1D arrays.
    /// </summary>
    /// <returns>The concatenated 1D array.</returns>
    /// <param name="a">The first 1D array.</param>
    /// <param name="b">The second 1D array.</param>
    private T[] Concatenate1DArrays<T>(T[] a, T[] b)
    {
        T[] result = new T[a.Length + b.Length];
        Array.Copy(a, 0, result, 0, a.Length);
        Array.Copy(b, 0, result, a.Length, b.Length);
        return result;
    }

    /// <summary>
    /// Gets a random value from dictionary.
    /// </summary>
    /// <returns>The random value from dictionary.</returns>
    /// <param name="dictionary">Dictionary.</param>
    /// <typeparam name="TKey">The key's type parameter.</typeparam>
    /// <typeparam name="TValue">The value's type parameter.</typeparam>
    public TValue GetRandomValueFromDictionary<TKey, TValue>(IDictionary<TKey, TValue> dictionary)
    {
        List<TValue> values = Enumerable.ToList(dictionary.Values);
        int size = dictionary.Count;
        return values[random.Next(size)];
    }

    /// <summary>
    /// Gets a random key from dictionary.
    /// </summary>
    /// <returns>The random key from dictionary.</returns>
    /// <param name="dictionary">Dictionary.</param>
    /// <typeparam name="TKey">The key's type parameter.</typeparam>
    /// <typeparam name="TValue">The value's type parameter.</typeparam>
    public TKey GetRandomKeyFromDictionary<TKey, TValue>(IDictionary<TKey, TValue> dictionary)
    {
        List<TKey> keys = Enumerable.ToList(dictionary.Keys);
        int size = dictionary.Count;
        return keys[random.Next(size)];
    }
}
