﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;
using NewtonVR;

//TODO: Extra Scoreboard for the Audience?
//BUG: During Demo, if on Agent's turn during demo and press enter, does not spawn prop for player
public class LevelManager : MonoBehaviour
{
    //The sound effect for the countdown timer
    private AudioSource timerSound;
    //The sound effect for the poof to vanish prop
    private AudioSource poofSound;

    //Get the smoke for the poof
    private GameObject smoke;

    private GameObject instructionText;
    private GameObject audienceInstructionText;
    private GameObject audienceTitleText;

	private GameObject turnText;
	private GameObject timerText;
	private GameObject roundText;
    private GameObject roundNumberText;

    private GameObject instructionPanel;

    private GameObject audienceInstructionPanel;
    private GameObject audienceTitlePanel;

    private GameObject turnPanel;
    private GameObject timerPanel;
    private GameObject roundPanel;
    private GameObject roundNumberPanel;

    private GameObject agentScoreText;
    private GameObject agentScoreNumber;

    private GameObject playerScoreText;
    private GameObject playerScoreNumber;

    public enum StateType { START, INSTRUCTIONS, DEMO, ROUND, END };
    public enum TurnType { ROUNDOVER, PLAYER, AGENT}

    private StateType currentState;
    private StateType previousState;
    private StateType[] stateTypes;
    private int stateTypeCount;
    private int currentStateIndex;

    private TurnType currentTurn;
    private TurnType previousTurn;
    private TurnType[] turnTypes;
    private int turnTypeCount;
    private int currentTurnIndex;

	private int time;
	public int agentTimeLimit = 30;
	public int playerTimeLimit = 30;

    int playerScore = 0;
    int agentScore = 0;

    private string agentScoreTextString;
    private string playerScoreTextString;

    private string startString;
	private string instructionsString;
	private string endString;
	private string playerTurnString;
	private string agentTurnString;
	private string roundString;
    private string demoTurnString;
    private string audienceTitleString;
    private int roundNumber = 0;

    HumanPlayer humanPlayer;
    AgentPlayer agentPlayer;
    DeepIMAGINATIONInterface DeepIMAGINATION;
    FeedbackParticlesystemManager feedback;
    PlayerStateRecording recorder;
    PropManager propManager;
    ButtonController buttonController;

	// Use this for initialization
	void Start()
	{   
        //get the countdown timer
        timerSound = GameObject.Find("TimerSound").GetComponent<AudioSource>();
        //set the volume for the timer sound
        timerSound.volume = 1f;
        
        //get the poof sound
        poofSound = GameObject.Find("PoofSound").GetComponent<AudioSource>();
        //set the volume for the poof sound
        poofSound.volume = 1f;

        DeepIMAGINATION = gameObject.GetComponent<DeepIMAGINATIONInterface>();
        feedback = gameObject.GetComponent<FeedbackParticlesystemManager>();
        recorder = gameObject.GetComponent<PlayerStateRecording>();
        propManager = gameObject.GetComponent<PropManager>();
        humanPlayer = gameObject.GetComponent<HumanPlayer>();
        agentPlayer = gameObject.GetComponent<AgentPlayer>();
        buttonController = gameObject.GetComponent<ButtonController>();

        instructionText = GameObject.Find("Instructions");
        audienceInstructionText = GameObject.Find("AudienceInstructions");
        audienceTitleText = GameObject.Find("AudienceTitle");

		turnText = GameObject.Find("Turn");
		timerText = GameObject.Find("Timer");
		roundText = GameObject.Find("Round");
        roundNumberText = GameObject.Find("Round Number Text");

        instructionPanel = GameObject.Find("InstructionsPanel");

        audienceInstructionPanel = GameObject.Find("AudienceInstructionsPanel");
        audienceTitlePanel = GameObject.Find("AudienceTitlePanel");
        
        turnPanel = GameObject.Find("TurnPanel");
        timerPanel = GameObject.Find("TimerPanel");
        roundPanel = GameObject.Find("RoundPanel");
        roundNumberPanel = GameObject.Find("Round Number Panel");

        agentScoreText = GameObject.Find("Agent Score Text");
        agentScoreNumber = GameObject.Find("Agent Score Number");

        playerScoreText = GameObject.Find("Player Score Text");
        playerScoreNumber = GameObject.Find("Player Score Number");

        agentScoreTextString = "AGENT";
        playerScoreTextString = "PLAYER";

        startString = "Welcome to the Robot Improv Circus!\n\nWould you like to play the Props game?";
		instructionsString = "Take turns to improvise actions with your robot partner.\nUse the mystery props.\nYour turn will be " + playerTimeLimit + " seconds long.\n\nHave fun!";
		endString = "Thank you for playing!\n\nCome back soon!";
		playerTurnString = "YOUR TURN";
		agentTurnString = "AGENT TURN";
		roundString = "ROUND: ";
        demoTurnString = "DEMO";
        audienceTitleString = "Instructions to VR user";

        stateTypes = (StateType[])Enum.GetValues(typeof(StateType));
        stateTypeCount = stateTypes.Length;
        currentStateIndex = 0;
        //Sets current and previous state to START
        currentState = stateTypes[currentStateIndex];
        previousState = stateTypes[currentStateIndex];
        SwitchState(currentState);

        turnTypes = (TurnType[])Enum.GetValues(typeof(TurnType));
        turnTypeCount = turnTypes.Length;
        currentTurnIndex = 0;
        //Sets the current turn and previous turn to AGENT
        currentTurn = turnTypes[currentTurnIndex];
        previousTurn = turnTypes[currentTurnIndex];
    }

	// Update is called once per frame
	public void Update()
	{
		HandleKeyboardInput();

		UpdateTime();

        if (currentState == StateType.ROUND)
        {
            UpdateScoreBoard();
        }
	}

	public void UpdateTime()
	{
		if (timerText.activeSelf)
		{
			timerText.GetComponent<Text>().text = time.ToString();
		}
	}

    /// <summary>
    /// Managest switching between states
    /// States can be Start, Instructions, Round, or End
    /// </summary>
    /// <param name="state"></param>
	public void SwitchState(StateType state)
	{
		if (currentState == StateType.START)
		{
            TurnOffTimerPanel();
            instructionText.GetComponent<Text>().text = startString;
            audienceInstructionText.GetComponent<Text>().text = startString;
            audienceTitleText.GetComponent<Text>().text = audienceTitleString;
        }
		else if (currentState == StateType.INSTRUCTIONS)
		{
            TurnOffTimerPanel();
            instructionText.GetComponent<Text>().text = instructionsString;
            audienceInstructionText.GetComponent<Text>().text = instructionsString;
            audienceTitleText.GetComponent<Text>().text = audienceTitleString;
        }
        else if (currentState == StateType.DEMO)
        {
            TurnOnTimerPanel();
            instructionText.GetComponent<Text>().text = "";
            audienceInstructionText.GetComponent<Text>().text = "";
            audienceTitleText.GetComponent<Text>().text = "";

            roundText.GetComponent<Text>().text = roundString;
            agentScoreText.GetComponent<Text>().text = agentScoreTextString;
            playerScoreText.GetComponent<Text>().text = playerScoreTextString;

            propManager.ResetProp();
            propManager.GenerateProp();

            SwitchTurnDemo(currentTurn);
        }
        else if (currentState == StateType.ROUND)
        {
            ResetScoreBoard();

            propManager.ResetProp();
            propManager.GenerateProp();

            TurnOnTimerPanel();
            instructionText.GetComponent<Text>().text = "";
            audienceInstructionText.GetComponent<Text>().text = "";
            audienceTitleText.GetComponent<Text>().text = "";

            roundText.GetComponent<Text>().text = roundString;
            agentScoreText.GetComponent<Text>().text = agentScoreTextString;
            playerScoreText.GetComponent<Text>().text = playerScoreTextString;
            SwitchTurn(currentTurn);
        }
		else if (currentState == StateType.END)
		{
            ResetScoreBoard();
            TurnOffTimerPanel();
            instructionText.GetComponent<Text>().text = endString;
            audienceInstructionText.GetComponent<Text>().text = endString;
            audienceTitleText.GetComponent<Text>().text = audienceTitleString;
        }
    }

    /// <summary>
    /// Switches between turns
    /// Turns can be Player or Agent
    /// </summary>
    /// <param name="turn"></param>
    public void SwitchTurn(TurnType turn)
    {
        if (currentTurn == TurnType.PLAYER)
        {
            turnText.GetComponent<Text>().text = playerTurnString;
            humanPlayer.StartPlay();
            StartTimer(playerTimeLimit);
            recorder.StartRecording();
        }
        else if (currentTurn == TurnType.AGENT)
        {
            turnText.GetComponent<Text>().text = agentTurnString;
            //DeepIMAGINATION.GenerateAction();
            StartTimer(agentTimeLimit);
            agentPlayer.StartPlay();
            agentScore = agentScore + 10;

        }
        else if (currentTurn == TurnType.ROUNDOVER)
        {
            propManager.GenerateProp();
            ToggleTurn();
            SwitchTurn(currentTurn);
        }
    }

    public void SwitchTurnDemo(TurnType turn)
    {
        if (currentTurn == TurnType.PLAYER)
        {
            turnText.GetComponent<Text>().text = demoTurnString;
            humanPlayer.StartPlay();
        }
        else if (currentTurn == TurnType.AGENT)
        {
            turnText.GetComponent<Text>().text = demoTurnString;
            //DeepIMAGINATION.GenerateAction();
            agentPlayer.StartPlay();
        }
        else if (currentTurn == TurnType.ROUNDOVER)
        {
            propManager.GenerateProp();
            ToggleTurnDemo();
            SwitchTurnDemo(currentTurn);
        }
    }


	public void HandleKeyboardInput()
	{
		if(Input.GetKeyDown(KeyCode.RightArrow))
		{
            if (currentState == StateType.DEMO)
            {
                ToggleTurnDemo();
                SwitchTurnDemo(currentTurn);
            }
            else if (currentState == StateType.ROUND)
            {
                ToggleTurn();
                SwitchTurn(currentTurn);
            }
        }
		if(Input.GetKeyDown(KeyCode.LeftArrow))
		{
			if (currentState == StateType.DEMO)
            {
                ToggleTurnDemo();
                SwitchTurnDemo(currentTurn);
            }
            else if(currentState == StateType.ROUND)
            {
                ToggleTurn();
                SwitchTurn(currentTurn);
            }
        }

		if(Input.GetKeyDown(KeyCode.Return))
		{
            previousTurn = currentTurn;
            currentTurnIndex = 0;
            currentTurn = TurnType.ROUNDOVER;

            currentStateIndex = (currentStateIndex < stateTypeCount - 1) ? currentStateIndex + 1 : 0;
            previousState = currentState;
            currentState = stateTypes[currentStateIndex];
            SwitchState(currentState);

            if (agentPlayer.IsPlaying())
            {
                agentPlayer.StopPlay();
            }

            if (currentState != StateType.ROUND)
            {
                CancelInvoke("DecrementTimer");
            }
            TurnOffTimer();
        }
		if(Input.GetKeyDown(KeyCode.Escape))
		{
            previousTurn = currentTurn;
            currentTurnIndex = 0;
            currentTurn = TurnType.ROUNDOVER;

            currentStateIndex = (currentStateIndex < stateTypeCount - 1) ? currentStateIndex + 1 : 0;
            previousState = currentState;
            currentState = stateTypes[currentStateIndex];
            SwitchState(currentState);

            if (agentPlayer.IsPlaying())
            {
                agentPlayer.StopPlay();
            }

            if (currentState != StateType.ROUND)
            {
                CancelInvoke("DecrementTimer");
            }
        }
	}

    public void ToggleTurn()
	{
        if (currentTurn == TurnType.PLAYER)
		{
            //if the turn gets changed stop the timer
            TurnOffTimer();
            feedback.PlayApplause();
            recorder.StopRecording();
            humanPlayer.StopPlay();
            previousTurn = currentTurn;
            currentTurn = TurnType.AGENT;
        }
		else if(currentTurn == TurnType.AGENT)
		{
            feedback.PlayApplause();
            if (agentPlayer.IsPlaying())
            {
                agentPlayer.StopPlay();
            }
            previousTurn = currentTurn;
            currentTurn = TurnType.ROUNDOVER;
        }
        else if (currentTurn == TurnType.ROUNDOVER)
        {
            roundNumber++;
            roundNumberText.GetComponent<Text>().text = roundNumber.ToString();
            previousTurn = currentTurn;
            currentTurn = TurnType.PLAYER;
        }
	}

    public void ToggleTurnDemo()
    {
        if (currentTurn == TurnType.PLAYER)
        {
            feedback.PlayApplause();
            humanPlayer.StopPlay();
            previousTurn = currentTurn;
            //currentTurn = TurnType.ROUNDOVER;
            currentTurn = TurnType.AGENT;

        }
        else if (currentTurn == TurnType.AGENT)
        {
            feedback.PlayApplause();
            if (agentPlayer.IsPlaying())
            {
                agentPlayer.StopPlay();
            }
            previousTurn = currentTurn;
            //currentTurn = TurnType.PLAYER;
            currentTurn = TurnType.ROUNDOVER;
        }
        else if (currentTurn == TurnType.ROUNDOVER)
        {
            roundNumberText.GetComponent<Text>().text = roundNumber.ToString();
            previousTurn = currentTurn;
            //currentTurn = TurnType.AGENT;
            currentTurn = TurnType.PLAYER;
        }
    }

    public void StartTimer(int time)
    {
        CancelInvoke("DecrementTimer");
        this.time = time;
        InvokeRepeating("DecrementTimer", 1.0f, 1.0f);
    }

	public void DecrementTimer()
	{
		time--;
		if(time == 0)
		{
			CancelInvoke("DecrementTimer");
            propManager.ResetProp();
            poofSound.Play();
            if (currentState == StateType.ROUND)
            {
                ToggleTurn();
                SwitchTurn(currentTurn);
            }

        }
        //trigger the timer sound to play
        if (time == 5)
        {
            timerSound.Play();
        }
	}

    public void TurnOffTimer()
    {
        //if the turn gets changed stop the timer
        if (timerSound.isPlaying)
        {
            timerSound.Stop();
        }
    }

    private void UpdateScoreBoard()
    {
        playerScore = buttonController.GetPlayerScore();
        string agentScoreString = agentScore.ToString();
        agentScoreNumber.GetComponent<Text>().text = agentScoreString;
        playerScoreNumber.GetComponent<Text>().text = playerScore.ToString();

        if(buttonController.GetButtonDown() && currentTurn == TurnType.PLAYER)
        {
            recorder.StopRecording();
            recorder.StartRecording();
        }
    }

    private void ResetScoreBoard()
    {
        agentScore = 0;
        buttonController.SetPlayerScore(0);
        roundNumber = 0;
        time = 0;
        UpdateScoreBoard();
        previousTurn = currentTurn;
        currentTurn = TurnType.ROUNDOVER;
        currentTurnIndex = 0;
    }

    public void IncreaseAgentScore()
    {
        agentScore = agentScore + 10;
    }

    /// <summary>
    /// Turns off the timer and watch panels
    /// Turns on the instruction text
    /// </summary>
    public void TurnOffTimerPanel()
    {
        instructionText.SetActive(true);
        audienceInstructionText.SetActive(true);
        audienceTitleText.SetActive(true);

        instructionPanel.SetActive(true);
        audienceInstructionPanel.SetActive(true);
        audienceTitlePanel.SetActive(true);

        //agentScoreNumber.SetActive(false);
        //agentScoreNumberPanel.SetActive(false);
        //agentScoreText.SetActive(false);
        //agentScoreTextPanel.SetActive(false);

        //playerScoreNumber.SetActive(false);
        //playerScoreNumberPanel.SetActive(false);
        //playerScoreText.SetActive(false);
        //playerScoreTextPanel.SetActive(false);

        //roundNumberText.SetActive(false);
        //roundNumberPanel.SetActive(false);

        //roundText.SetActive(false);
        //roundPanel.SetActive(false);
        //turnText.SetActive(false);
        //turnPanel.SetActive(false);
        //timerText.SetActive(false);
        //timerPanel.SetActive(false);
    }

    /// <summary>
    /// Turns on the timer and watch panels
    /// Turns off the instruction text
    /// </summary>
    public void TurnOnTimerPanel()
    {
        instructionText.SetActive(false);
        audienceInstructionText.SetActive(false);
        audienceTitleText.SetActive(false);

        instructionPanel.SetActive(false);
        audienceInstructionPanel.SetActive(false);
        audienceTitlePanel.SetActive(false);

        agentScoreNumber.SetActive(true);
        //agentScoreNumberPanel.SetActive(true);
        agentScoreText.SetActive(true);
        //agentScoreTextPanel.SetActive(true);

        playerScoreNumber.SetActive(true);
        //playerScoreNumberPanel.SetActive(true);
        playerScoreText.SetActive(true);
        //playerScoreTextPanel.SetActive(true);

        roundNumberText.SetActive(true);
        //roundNumberPanel.SetActive(true);

        roundText.SetActive(true);
        //roundPanel.SetActive(true);
        turnText.SetActive(true);
        //turnPanel.SetActive(true);
        timerText.SetActive(true);
        //timerPanel.SetActive(true);
    }
}
