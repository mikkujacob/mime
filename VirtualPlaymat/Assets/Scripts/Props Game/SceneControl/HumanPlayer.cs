﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NewtonVR;

public class HumanPlayer : Player {

    PropManager propManager;
    private GameObject prop;
    private string propName;

    public bool objectPositionGenerated;

    private ParticleSystem poof;

    //Keep track of hands for buzzer collision
    //private PlayerStateDefinition state;
    
    //private NVRHand leftHand;
    //private NVRHand rightHand;
    //private Transform leftHandTransform;
    //private Transform rightHandTransform;
    //private GameObject playerObject;

    // Use this for initialization
    void Start () {
        propManager = gameObject.GetComponent<PropManager>();
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void StartPlay()
    {
        poof = propManager.GetPoof();

        prop = propManager.GetCurrentProp();
        propName = propManager.GetCurrentPropName();

        propManager.SpawnProp();

        prop.transform.GetChild(0).gameObject.GetComponent<MeshCollider>().enabled = false;
        prop.transform.GetChild(0).gameObject.GetComponent<Rigidbody>().isKinematic = true;
        prop.transform.GetChild(0).gameObject.transform.localPosition = new Vector3();
        print(prop.transform.GetChild(0).gameObject.transform.localPosition);
        prop.transform.GetChild(0).gameObject.transform.localRotation = new Quaternion();
        prop.transform.GetChild(0).gameObject.GetComponent<MeshCollider>().enabled = true;
        prop.transform.GetChild(0).gameObject.GetComponent<Rigidbody>().isKinematic = false;
    }

    public void StopPlay()
    {
        propManager.SpawnProp();
        if (objectPositionGenerated)
        {
            //prop.transform.GetChild(0).gameObject.transform.position = propSpawnLocation.position;
            //prop.transform.GetChild(0).gameObject.transform.rotation = propSpawnLocation.rotation;
        }
        else
        {
            prop.transform.GetChild(0).gameObject.GetComponent<MeshCollider>().enabled = false;
            prop.transform.GetChild(0).gameObject.GetComponent<Rigidbody>().isKinematic = true;
            prop.transform.localScale = new Vector3(1, 1, 1);
            prop.transform.GetChild(0).gameObject.transform.localPosition = new Vector3();
            prop.transform.GetChild(0).gameObject.transform.localRotation = new Quaternion();
            prop.transform.GetChild(0).gameObject.transform.localScale = new Vector3(1, 1, 1);
            prop.transform.GetChild(0).gameObject.GetComponent<MeshCollider>().enabled = true;
            prop.transform.GetChild(0).gameObject.GetComponent<Rigidbody>().isKinematic = false;
        }
        poof.Play();
        propManager.ResetProp();
    }

    public void Reset()
    {
        
    }
}


