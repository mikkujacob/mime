﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudienceSpeechBubble : MonoBehaviour {
    private Vector3 initialAgentPosition;
    private float deltaX;
    private float deltaY;
    private Vector3 initialPosition;

    private void Start()
    {
        initialAgentPosition = GameObject.FindGameObjectWithTag("Agent").transform.position;
        initialPosition = transform.position;
    }
    void LateUpdate()
    {
        deltaX = GameObject.FindGameObjectWithTag("Agent").transform.position.x - initialAgentPosition.x;
        deltaY = GameObject.FindGameObjectWithTag("Agent").transform.position.y - initialAgentPosition.y;
        transform.position = initialPosition + new Vector3(deltaX, deltaY, 0);
        //transform.position = transform.position + new Vector3(-1.6f, 4.4f, 2.0f);
    }
}
