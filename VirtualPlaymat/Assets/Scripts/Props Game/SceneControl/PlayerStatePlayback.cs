﻿using System.Collections;
using System.Collections.Generic;
using NewtonVR;
using RootMotion.FinalIK;
using UnityEngine;

public class PlayerStatePlayback : MonoBehaviour
{

	GUITextControl textControl;

	private string streamingAssetsPath;
	private List<string> gestureFileNames;
	public string recordingsListFileWithPath;
	public bool usingRecordingsListFileWithPath;

	public string recordingsCleanedListFileWithPath;
	private Dictionary<string, bool> fileCleaningDictionary;

	public RecordedGesture recording;
	private int currentRecordingIndex;
    public int skipN = 45;

	private bool isInit;
	private bool isPlaying;

	public string currentObjectName;
	private Dictionary<string, GameObject> gameObjectDictionary;
	private List<GameObject> gameObjectKeyList;
	private Transform propSpawnLocation;
	private Dictionary<string, Vector3> localScales = new Dictionary<string, Vector3> {{"SMALL", new Vector3(0.25f, 0.25f, 0.25f)}, {"MEDIUM", new Vector3(0.5f, 0.5f, 0.5f)}, {"LARGE", new Vector3(1f, 1f, 1f)}};

	public bool fixCorruptedHandData = false;

	//	public SessionDefinition session;
	//	public GestureDefinition gesture;
	//	public PlayerStateDefinition state;

	private GameObject playerObject;
	private Transform headTransform;
	private Transform pelvisTransform;
	private Transform leftHandTransform;
	private Transform rightHandTransform;
//	private Transform leftFootTransform;
//	private Transform rightFootTransform;
//	private Transform rootTransform;
	//	private NVRHand leftHand;
	//	private NVRHand rightHand;

	private GameObject IKObject;
	private VRIK IK;
	private Transform[] skeletalTransforms;

	//	private JobQueue<InsertDataJob> queue;

	/// <summary>
	/// Start this instance.
	/// </summary>
	void Start ()
	{
		isInit = false;

		streamingAssetsPath = Application.streamingAssetsPath;

		textControl = GameObject.Find ("Text").GetComponent<GUITextControl> ();

        gameObjectDictionary = new Dictionary<string, GameObject>();
        gameObjectKeyList = new List<GameObject>();
        gameObjectKeyList.AddRange(GameObject.FindGameObjectsWithTag("ObjectStateTracker"));
        foreach (GameObject gameObj in gameObjectKeyList)
        {
            gameObjectDictionary.Add(gameObj.name, gameObj);
        }

        propSpawnLocation = GameObject.Find("PropSpawnLocation").transform;

        Invoke("InitializeTracking", 0.5f);

        //		queue = new JobQueue<InsertDataJob> (2);

        currentRecordingIndex = 0;
		if (usingRecordingsListFileWithPath)
		{
			string[] lines = System.IO.File.ReadAllLines (recordingsListFileWithPath);
			gestureFileNames = new List<string> (lines [0].Split (new char[]{ ',' }));
		}
		else
		{
			gestureFileNames = new List<string> (System.IO.Directory.GetFiles (streamingAssetsPath, "*.gesture"));
		}
//		foreach (string name in gestureFileNames)
//		{
//			Debug.Log ("Recording: " + name);
//		}

		if (gestureFileNames.Count > 0) 
		{
			recording = new RecordedGesture (gestureFileNames [currentRecordingIndex], skipN);
		}

		fileCleaningDictionary = new Dictionary<string, bool> ();


		if (recordingsCleanedListFileWithPath == "")
		{
			if (usingRecordingsListFileWithPath)
			{
				recordingsCleanedListFileWithPath = recordingsListFileWithPath + "-" + System.DateTime.Now.ToString ("yyyy-MM-dd-HH-mm-ss-fff") + ".cleaned.filenames";
			}
			else
			{
				recordingsCleanedListFileWithPath = System.IO.Path.Combine (Application.streamingAssetsPath, "CleanedFileNames-" + System.DateTime.Now.ToString ("yyyy-MM-dd-HH-mm-ss-fff") + ".cleaned.filenames");
			}
		}

		Debug.Log ("Loaded " + gestureFileNames.Count + " Recordings");

		//		List<PlayerStateDefinition> stateDefs = new List<PlayerStateDefinition> ();
		//		stateDefs.Add (new PlayerStateDefinition (System.DateTime.Now.ToString (), new Vector3Definition (1, 2, 3), new Vector4Definition (1, 2, 3, 1), new Vector3Definition (1, 2, 3), new Vector4Definition (1, 2, 3, 1), new Vector3Definition (1, 2, 3), new Vector4Definition (1, 2, 3, 1), true, true, true, true, new Vector3Definition[22], new Vector4Definition[22]));
		//		stateDefs.Add (new PlayerStateDefinition (System.DateTime.Now.ToString (), new Vector3Definition (1, 2, 4), new Vector4Definition (1, 2, 3, 1), new Vector3Definition (1, 2, 3), new Vector4Definition (1, 2, 3, 1), new Vector3Definition (1, 2, 3), new Vector4Definition (1, 2, 3, 1), true, true, true, true, new Vector3Definition[22], new Vector4Definition[22]));
		//		stateDefs.Add (new PlayerStateDefinition (System.DateTime.Now.ToString (), new Vector3Definition (1, 2, 5), new Vector4Definition (1, 2, 3, 1), new Vector3Definition (1, 2, 3), new Vector4Definition (1, 2, 3, 1), new Vector3Definition (1, 2, 3), new Vector4Definition (1, 2, 3, 1), true, true, true, true, new Vector3Definition[22], new Vector4Definition[22]));
		//
		//		Debug.Log ("List JSON: " + JsonUtility.ToJson (stateDefs));
		//
		//		string stateString = JsonUtility.ToJson (new PlayerStateDefinition (System.DateTime.Now.ToString (), new Vector3Definition (3, 4, 5), new Vector4Definition (1, 2, 3, 1), new Vector3Definition (1, 2, 3), new Vector4Definition (1, 2, 3, 1), new Vector3Definition (1, 2, 3), new Vector4Definition (1, 2, 3, 1), true, true, true, true, new Vector3Definition[22], new Vector4Definition[22]));
		//		Debug.Log ("State JSON: " + stateString);
		//		PlayerStateDefinition testState = JsonUtility.FromJson<PlayerStateDefinition> (stateString);
		//		Debug.Log ("Reconstituted State JSON: " + JsonUtility.ToJson (testState));
	}

	/// <summary>
	/// Initializes the tracking.
	/// </summary>
	void InitializeTracking ()
	{
		playerObject = GameObject.FindGameObjectWithTag ("NVR Player");
//		leftHand = playerObject.transform.Find ("LeftHand").GetComponent<NVRHand> ();
//		rightHand = playerObject.transform.Find ("RightHand").GetComponent<NVRHand> ();
//		headTransform = playerObject.transform.Find ("DummyHead").transform;
//		leftHandTransform = playerObject.transform.Find ("DummyLeftHand").transform;
//		rightHandTransform = playerObject.transform.Find ("DummyRightHand").transform;

		IKObject = GameObject.FindGameObjectWithTag ("PlayerAvatar");
		IK = IKObject.GetComponent<VRIK> ();
		//Joints: [root, pelvis, spine, chest, neck, head, leftShoulder, leftUpperArm, leftForearm, leftHand, rightShoulder, rightUpperArm, rightForearm, rightHand, leftThigh, leftCalf, leftFoot, leftToes, rightThigh, rightCalf, rightFoot, rightToes]
		skeletalTransforms = IK.references.GetTransforms ();

//		rootTransform = IK.references.root;
//		headTransform = (fixCorruptedHandData) ? IK.references.head : playerObject.transform.Find ("DummyHead").transform;
//		leftHandTransform = (fixCorruptedHandData) ? IK.references.leftHand : playerObject.transform.Find ("DummyLeftHand").transform;
//		rightHandTransform = (fixCorruptedHandData) ? IK.references.rightHand : playerObject.transform.Find ("DummyRightHand").transform;

		headTransform = playerObject.transform.Find ("DummyHead").transform;
		pelvisTransform = playerObject.transform.Find ("DummyPelvis").transform;
		leftHandTransform = playerObject.transform.Find ("DummyLeftHand").transform;
		rightHandTransform = playerObject.transform.Find ("DummyRightHand").transform;
//		leftFootTransform = playerObject.transform.Find ("DummyLeftFoot").transform;
//		rightFootTransform = playerObject.transform.Find ("DummyRightFoot").transform;

		Debug.Log ("Tracking Initialized");
		textControl.SetTimedMessage ("Tracking Initialized", 1);

		foreach (GameObject gameObj in gameObjectKeyList)
		{
			gameObj.SetActive (false);
		}

		if (recording != null) 
		{
			currentObjectName = recording.GetGesture ().objectName;
			GameObject propObj = gameObjectDictionary [currentObjectName];
			propObj.transform.position = propSpawnLocation.position;
			propObj.transform.GetChild (0).gameObject.transform.position = recording.GetGesture ().objectStartingPosition.ToVector3 ();
			propObj.transform.GetChild (0).gameObject.transform.rotation = recording.GetGesture ().objectStartingRotation.ToQuaternion ();
			propObj.transform.GetChild (0).gameObject.GetComponent<Renderer> ().material.SetColor ("_Color", Random.ColorHSV (0f, 1f, 0.9f, 1f, 0.7f, 1f, 0.999f, 1f));
			propObj.transform.GetChild (0).localScale = localScales [recording.GetGesture ().objectSize];
		}
//		propObj.SetActive (true);

		if (recording != null)
		{
			textControl.SetTimedMessage ("Cued next gesture " + currentRecordingIndex + ": " + gestureFileNames [currentRecordingIndex], 2);
			Debug.Log ("Cued next gesture " + currentRecordingIndex + ": " + gestureFileNames [currentRecordingIndex]);
		}
		isInit = true;
	}

	/// <summary>
	/// Fixed update method.
	/// </summary>
	void FixedUpdate ()
	{
		if (isInit && isPlaying)
		{
			ApplyRecordedData (recording.GetNearestFrameInterpolated (System.DateTime.Now));
		}
	}

    void Update()
    {
        if (isInit)
        {
            HandleInput();
//			queue.Update ();
        }
    }

    void ApplyRecordedData (PlayerStateDefinition nextState)
	{
        //TODO Use frame to control scene.

        //Debug.Log("Recorded State: " + JsonUtility.ToJson(nextState));

        //		Vector3Definition[] jointPositions = new Vector3Definition[22];
        //		Vector4Definition[] jointRotations = new Vector4Definition[22];
        //		for (int i = 0; i < 22; i++)
        //		{
        //			if (skeletalTransforms == null || skeletalTransforms [i] == null)
        //			{
        //				jointPositions [i] = new Vector3Definition ();
        //				jointRotations [i] = new Vector4Definition ();
        //				continue;
        //			}
        //
        //			jointPositions [i] = new Vector3Definition (skeletalTransforms [i].position);
        //			jointRotations [i] = new Vector4Definition (skeletalTransforms [i].rotation);
        //		}

        //		state = new PlayerStateDefinition ();
        //		state.timestamp = System.DateTime.Now.ToString ("yyyy-MM-dd-HH-mm-ss-fff");
        //		state.headPosition = new Vector3Definition (headTransform.position);

        //Joints: [root, pelvis, spine, chest, neck, head, leftShoulder, leftUpperArm, leftForearm, leftHand, rightShoulder, rightUpperArm, rightForearm, rightHand, leftThigh, leftCalf, leftFoot, leftToes, rightThigh, rightCalf, rightFoot, rightToes]
        //		rootTransform.position = nextState.skeletalPositions [0].ToVector3 ();
        //		rootTransform.rotation = nextState.skeletalRotations [0].ToQuaternion ();

        //		IK.references.pelvis.position = nextState.skeletalPositions [0].ToVector3 ();
        //
        //		IK.references.pelvis.rotation = nextState.skeletalRotations [0].ToQuaternion ();
        //
        //		IK.references.spine.position = nextState.skeletalPositions [0].ToVector3 ();
        //
        //		IK.references.spine.rotation = nextState.skeletalRotations [0].ToQuaternion ();
        //
        //		IK.references.neck.position = nextState.skeletalPositions [0].ToVector3 ();
        //
        //		IK.references.neck.rotation = nextState.skeletalRotations [0].ToQuaternion ();
        //
        //		IK.references.leftShoulder.position = nextState.skeletalPositions [0].ToVector3 ();
        //
        //		IK.references.leftShoulder.rotation = nextState.skeletalRotations [0].ToQuaternion ();
        //
        //		IK.references.leftUpperArm.position = nextState.skeletalPositions [0].ToVector3 ();
        //
        //		IK.references.leftUpperArm.rotation = nextState.skeletalRotations [0].ToQuaternion ();
        //
        //		IK.references.leftForearm.position = nextState.skeletalPositions [0].ToVector3 ();
        //
        //		IK.references.leftForearm.rotation = nextState.skeletalRotations [0].ToQuaternion ();
        //
        //		IK.references.rightShoulder.position = nextState.skeletalPositions [0].ToVector3 ();
        //
        //		IK.references.rightShoulder.rotation = nextState.skeletalRotations [0].ToQuaternion ();
        //
        //		IK.references.rightUpperArm.position = nextState.skeletalPositions [0].ToVector3 ();
        //
        //		IK.references.rightUpperArm.rotation = nextState.skeletalRotations [0].ToQuaternion ();
        //
        //		IK.references.rightForearm.position = nextState.skeletalPositions [0].ToVector3 ();
        //
        //		IK.references.rightForearm.rotation = nextState.skeletalRotations [0].ToQuaternion ();
        //
        //		IK.references.leftThigh.position = nextState.skeletalPositions [0].ToVector3 ();
        //
        //		IK.references.leftThigh.rotation = nextState.skeletalRotations [0].ToQuaternion ();
        //
        //		IK.references.leftCalf.position = nextState.skeletalPositions [0].ToVector3 ();
        //
        //		IK.references.leftCalf.rotation = nextState.skeletalRotations [0].ToQuaternion ();
        //
        //		IK.references.leftFoot.position = nextState.skeletalPositions [0].ToVector3 ();
        //
        //		IK.references.leftFoot.rotation = nextState.skeletalRotations [0].ToQuaternion ();
        //
        //		IK.references.leftToes.position = nextState.skeletalPositions [0].ToVector3 ();
        //
        //		IK.references.leftToes.rotation = nextState.skeletalRotations [0].ToQuaternion ();
        //
        //		IK.references.rightThigh.position = nextState.skeletalPositions [0].ToVector3 ();
        //
        //		IK.references.rightThigh.rotation = nextState.skeletalRotations [0].ToQuaternion ();
        //
        //		IK.references.rightCalf.position = nextState.skeletalPositions [0].ToVector3 ();
        //
        //		IK.references.rightCalf.rotation = nextState.skeletalRotations [0].ToQuaternion ();
        //
        //		IK.references.rightFoot.position = nextState.skeletalPositions [0].ToVector3 ();
        //
        //		IK.references.rightFoot.rotation = nextState.skeletalRotations [0].ToQuaternion ();
        //
        //		IK.references.rightToes.position = nextState.skeletalPositions [0].ToVector3 ();
        //
        //		IK.references.rightToes.rotation = nextState.skeletalRotations [0].ToQuaternion ();

        headTransform.position = (fixCorruptedHandData) ? nextState.skeletalPositions [5].ToVector3 () : nextState.headPosition.ToVector3 ();
		headTransform.rotation = (fixCorruptedHandData) ? nextState.skeletalRotations [5].ToQuaternion () : nextState.headRotation.ToQuaternion ();

		pelvisTransform.position = nextState.skeletalPositions [1].ToVector3 ();
		pelvisTransform.rotation = nextState.skeletalRotations [1].ToQuaternion ();

		leftHandTransform.position = (fixCorruptedHandData) ? nextState.skeletalPositions [9].ToVector3 () : nextState.leftHandPosition.ToVector3 ();
		leftHandTransform.rotation = (fixCorruptedHandData) ? nextState.skeletalRotations [9].ToQuaternion () : nextState.leftHandRotation.ToQuaternion ();

		rightHandTransform.position = (fixCorruptedHandData) ? nextState.skeletalPositions [13].ToVector3 () : nextState.rightHandPosition.ToVector3 ();
		rightHandTransform.rotation = (fixCorruptedHandData) ? nextState.skeletalRotations [13].ToQuaternion () : nextState.rightHandRotation.ToQuaternion ();

//		leftFootTransform.position = (fixCorruptedHandData) ? nextState.skeletalPositions [16].ToVector3 () : nextState.leftHandPosition.ToVector3 ();
//		leftFootTransform.rotation = (fixCorruptedHandData) ? nextState.skeletalRotations [16].ToQuaternion () : nextState.leftHandRotation.ToQuaternion ();

//		rightFootTransform.position = (fixCorruptedHandData) ? nextState.skeletalPositions [20].ToVector3 () : nextState.rightHandPosition.ToVector3 ();
//		rightFootTransform.rotation = (fixCorruptedHandData) ? nextState.skeletalRotations [20].ToQuaternion () : nextState.rightHandRotation.ToQuaternion ();

		if (!fixCorruptedHandData)
		{
			gameObjectDictionary [currentObjectName].transform.GetChild (0).gameObject.transform.position = nextState.objectPosition.ToVector3 ();
			gameObjectDictionary [currentObjectName].transform.GetChild (0).gameObject.transform.rotation = nextState.objectRotation.ToQuaternion ();
		}

//		state.leftControllerGripButtonDown = leftHand.HoldButtonDown;
//		state.leftControllerTriggerButtonDown = leftHand.UseButtonDown;
//		state.rightControllerGripButtonDown = rightHand.HoldButtonDown;
//		state.rightControllerTriggerButtonDown = rightHand.UseButtonDown;
//
//		state.leftControllerGripButtonUp = leftHand.HoldButtonUp;
//		state.leftControllerTriggerButtonUp = leftHand.UseButtonUp;
//		state.rightControllerGripButtonUp = rightHand.HoldButtonUp;
//		state.rightControllerTriggerButtonUp = rightHand.UseButtonUp;

//		state.skeletalPositions = jointPositions;
//		state.skeletalRotations = jointRotations;
	}

	void DelayedPlayback ()
	{
		recording.Play ();

		Debug.Log ("Playback Started...");
		textControl.SetTimedMessage ("Playback Started...", 1);
		isPlaying = true;
	}

	void HandleInput ()
	{
		if (Input.GetKeyDown (KeyCode.Keypad1) || Input.GetKeyDown (KeyCode.Alpha1))
		{
			fileCleaningDictionary [gestureFileNames [currentRecordingIndex]] = true;
//			fileCleaningDictionary.Add (gestureFileNames [currentRecordingIndex], true);
			Debug.Log ("GESTURE " + currentRecordingIndex + "  IS CLEAN!");
			textControl.SetTimedMessage ("GESTURE " + currentRecordingIndex + "  IS CLEAN!", 2);
		}

		if (Input.GetKeyDown (KeyCode.Keypad3) || Input.GetKeyDown (KeyCode.Alpha3))
		{
			fileCleaningDictionary [gestureFileNames [currentRecordingIndex]] = false;
//			fileCleaningDictionary.Add (gestureFileNames [currentRecordingIndex], false);
			Debug.Log ("GESTURE " + currentRecordingIndex + "  IS JUNK!");
			textControl.SetTimedMessage ("GESTURE " + currentRecordingIndex + "  IS JUNK!", 2);
		}

		if (Input.GetKeyDown (KeyCode.Space))
		{
			if (!isPlaying)
			{
//				recording.Play ();

				gameObjectDictionary [currentObjectName].transform.GetChild (0).gameObject.transform.position = recording.GetGesture ().objectStartingPosition.ToVector3 ();
				gameObjectDictionary [currentObjectName].transform.GetChild (0).gameObject.transform.rotation = recording.GetGesture ().objectStartingRotation.ToQuaternion ();
//				gameObjectDictionary [currentObjectName].transform.GetChild (0).gameObject.GetComponent<Collider> ().enabled = false;
//				gameObjectDictionary [currentObjectName].transform.GetChild (0).gameObject.GetComponent<Rigidbody> ().isKinematic = true;
				gameObjectDictionary [currentObjectName].SetActive (true);

                //Invoke ("DelayedPlayback", 2);
                DelayedPlayback();

            }
			else
			{
				isPlaying = false;

				gameObjectDictionary [currentObjectName].SetActive (false);

				Debug.Log ("Playback Stopped...");
				textControl.SetTimedMessage ("Playback stopped...", 1);

				recording.Stop ();
			}
		}

		if (Input.GetKeyDown (KeyCode.RightArrow))
		{
			isPlaying = false;
			recording.Stop ();
			currentRecordingIndex = (currentRecordingIndex < gestureFileNames.Count - 1) ? currentRecordingIndex + 1 : 0;
			recording = new RecordedGesture (gestureFileNames [currentRecordingIndex], skipN);

			gameObjectDictionary [currentObjectName].SetActive (false);
			currentObjectName = recording.GetGesture ().objectName;
			GameObject propObj = gameObjectDictionary [currentObjectName];
			propObj.transform.position = propSpawnLocation.position;
			propObj.transform.GetChild (0).gameObject.transform.position = recording.GetGesture ().objectStartingPosition.ToVector3 ();
			propObj.transform.GetChild (0).gameObject.transform.rotation = recording.GetGesture ().objectStartingRotation.ToQuaternion ();
			propObj.transform.GetChild (0).gameObject.GetComponent<Renderer> ().material.SetColor ("_Color", Random.ColorHSV (0f, 1f, 0.9f, 1f, 0.7f, 1f, 0.999f, 1f));
			propObj.transform.GetChild (0).localScale = localScales [recording.GetGesture ().objectSize];
			propObj.SetActive (true);

			textControl.SetTimedMessage ("Playback stopped. Cued next gesture " + currentRecordingIndex + ": " + gestureFileNames [currentRecordingIndex], 1);
			Debug.Log ("Playback stopped. Cued next gesture " + currentRecordingIndex + ": " + gestureFileNames [currentRecordingIndex]);
			if (fileCleaningDictionary.ContainsKey (gestureFileNames [currentRecordingIndex]))
			{
				if (fileCleaningDictionary [gestureFileNames [currentRecordingIndex]])
				{
					Debug.Log ("File evaluated as: CLEAN");
				}
				else
				{
					Debug.Log ("File evaluated as: JUNK");
				}
			}
			else
			{
				Debug.Log ("File has not been evaluated yet!");
			}
		}

		if (Input.GetKeyDown (KeyCode.LeftArrow))
		{
			isPlaying = false;
			recording.Stop ();
			currentRecordingIndex = (currentRecordingIndex > 0) ? currentRecordingIndex - 1 : gestureFileNames.Count - 1;
			recording = new RecordedGesture (gestureFileNames [currentRecordingIndex], skipN);

			gameObjectDictionary [currentObjectName].SetActive (false);
			currentObjectName = recording.GetGesture ().objectName;
			GameObject propObj = gameObjectDictionary [currentObjectName];
			propObj.transform.position = propSpawnLocation.position;
			propObj.transform.GetChild (0).gameObject.transform.position = new Vector3 (0, 0, 0);
			propObj.transform.GetChild (0).gameObject.GetComponent<Renderer> ().material.SetColor ("_Color", Random.ColorHSV (0f, 1f, 0.9f, 1f, 0.7f, 1f, 0.999f, 1f));
			propObj.transform.GetChild (0).localScale = localScales [recording.GetGesture ().objectSize];
			propObj.SetActive (true);

			textControl.SetTimedMessage ("Playback stopped. Cued previous gesture " + currentRecordingIndex + ": " + gestureFileNames [currentRecordingIndex], 1);
			Debug.Log ("Playback stopped. Cued next gesture " + currentRecordingIndex + ": " + gestureFileNames [currentRecordingIndex]);
			if (fileCleaningDictionary.ContainsKey (gestureFileNames [currentRecordingIndex]))
			{
				if (fileCleaningDictionary [gestureFileNames [currentRecordingIndex]])
				{
					Debug.Log ("File evaluated as: CLEAN");
				}
				else
				{
					Debug.Log ("File evaluated as: JUNK");
				}
			}
			else
			{
				Debug.Log ("File has not been evaluated yet!");
			}
		}
	}

	void OnApplicationQuit ()
	{
		if (isInit && isPlaying)
		{
			isPlaying = false;

			if (recording != null)
			{
				recording.Stop ();
			}
			Debug.Log ("Playback Stopped...");
			textControl.SetTimedMessage ("Playback stopped...", 1);
		}

		if (fileCleaningDictionary != null && fileCleaningDictionary.Count > 0)
		{
			System.IO.File.WriteAllText (recordingsCleanedListFileWithPath, ToString (fileCleaningDictionary, ","));
			Debug.Log ("Wrote Cleaned Filenames To File.");
		}
		else
		{
			Debug.Log ("No Cleaned Filenames To Write.");
		}
	}

	string ToString (Dictionary<string, bool> cleanedFileDictionary, string entrySeparator)
	{
		string result = "";

		foreach (KeyValuePair<string, bool> key in cleanedFileDictionary)
		{
			if (key.Value)
			{
				result += key.Key + entrySeparator;
			}
		}
		return result.Substring (0, result.Length - entrySeparator.Length);
	}

	//	public class InsertDataJob : JobItem
	//	{
	//		string path;
	//		GestureDefinition gesture;
	//
	//		public InsertDataJob (GestureDefinition gesture, string path)
	//		{
	//			this.gesture = gesture;
	//			this.path = path;
	//		}
	//
	//		protected override void DoWork ()
	//		{
	//			System.IO.File.WriteAllText (System.IO.Path.Combine (path, System.DateTime.Now.ToString ("yyyy-MM-dd-HH-mm-ss-fff") + ".gesture"), gesture.Serialize ());
	//		}
	//
	//		public override void OnFinished ()
	//		{
	//
	//
	//		}
	//	}
}
