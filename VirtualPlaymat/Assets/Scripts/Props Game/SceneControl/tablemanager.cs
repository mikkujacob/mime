﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class tablemanager : MonoBehaviour {

    public Text sliderAval;
    public Text sliderBval;

    public Transform knobA;
    public Transform knobB;

    public Transform bottom;

    public Transform dial;

    public Image dialimage;
    private Sprite dialsprite;

    public Sprite airhorn;
    public Sprite axe;
    public Sprite barbell;
    public Sprite corndog;
    public Sprite doubletee;
    public Sprite flower;
    public Sprite hammer;
    public Sprite helix;
    public Sprite horseshoe;
    public Sprite ladle;
    public Sprite palmtree;
    public Sprite plug;
    public Sprite plunger;
    public Sprite ring;
    public Sprite spokering;
    public Sprite spokering2;
    public Sprite squid;
    public Sprite tailring;
    public Sprite tee;
    public Sprite ubase;

    private double distA;
    private double distB;
    private int rot;
    private Vector3 from;
    private Vector3 to;

	// Use this for initialization
	void Start () {
        distA = 0;
        distB = 0;
        rot = (int) dial.rotation.y;
        dialsprite = dialimage.GetComponent<Image>().sprite;
        from = new Vector3(dial.position.x,dial.position.y,dial.position.z);
	}
	
	// Update is called once per frame
	void FixedUpdate () {
        distA = clipToZero((bottom.position.z - knobA.position.z)) / .075;
        sliderAval.text = distA.ToString("F2");
        distB = clipToZero((bottom.position.z - knobB.position.z)) / .075;
        sliderBval.text = distB.ToString("F2");

        rot = (int)(dial.localEulerAngles.y);

        if (rot >= 0 && rot < 18)
        {
            dialimage.GetComponent<Image>().sprite = airhorn;
        } else if (rot >= 18 && rot < 36)
        {
            dialimage.GetComponent<Image>().sprite = axe;
        } else if (rot >= 36 && rot < 54)
        {
            dialimage.GetComponent<Image>().sprite = barbell;
        } else if (rot >= 54 && rot < 72)
        {
            dialimage.GetComponent<Image>().sprite = corndog;
        } else if (rot >= 72 && rot < 90)
        {
            dialimage.GetComponent<Image>().sprite = doubletee;
        } else if (rot >= 90 && rot < 108)
        {
            dialimage.GetComponent<Image>().sprite = flower;
        } else if (rot >= 108 && rot < 126)
        {
            dialimage.GetComponent<Image>().sprite = hammer;
        } else if (rot >= 126 && rot < 144)
        {
            dialimage.GetComponent<Image>().sprite = helix;
        } else if (rot >= 144 && rot < 162)
        {
            dialimage.GetComponent<Image>().sprite = horseshoe;
        } else if (rot >= 162 && rot < 180)
        {
            dialimage.GetComponent<Image>().sprite = ladle;
        } else if (rot >= 180 && rot < 198)
        {
            dialimage.GetComponent<Image>().sprite = palmtree;
        } else if (rot >= 198 && rot < 216)
        {
            dialimage.GetComponent<Image>().sprite = plug;
        } else if (rot >= 216 && rot < 234)
        {
            dialimage.GetComponent<Image>().sprite = plunger;
        } else if (rot >= 234 && rot < 252)
        {
            dialimage.GetComponent<Image>().sprite = ring;
        } else if (rot >= 252 && rot < 270)
        {
            dialimage.GetComponent<Image>().sprite = spokering;
        } else if (rot >= 270 && rot < 288)
        {
            dialimage.GetComponent<Image>().sprite = spokering2;
        } else if (rot >= 288 && rot < 306)
        {
            dialimage.GetComponent<Image>().sprite = squid;
        } else if (rot >= 306 && rot < 324)
        {
            dialimage.GetComponent<Image>().sprite = tailring;
        } else if (rot >= 324 && rot < 342)
        {
            dialimage.GetComponent<Image>().sprite = tee;
        } else
        {
            dialimage.GetComponent<Image>().sprite = ubase;
        }
    }

    private double clipToZero(double f)
    {
        if (f < .05) {
            return 0.0;
        } else
        {
            return f;
        }
    }
}
