﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class DeepIMAGINATIONInterface : MonoBehaviour
{

    public TextAsset graphAsset;
    LoadGesture loadGesture;
    public static FrozenGraphLoader model = new FrozenGraphLoader();
    private bool isInit = false;
    private bool generate = false;
    private System.Random random = new System.Random();

    public int nLatents = 2;
    private int nInputs = 27000;

    public AgentPlayer playback;
    private PropManager propManager;

    private Dictionary<string, float[]> propNameAffordanceLookup = new Dictionary<string, float[]> {
        {"Squid w Tentacles Partless", new float[]{0,0.3333333F,0,0,0.3333333F,0,0,0,0,0,0,0.3333333F,0.3333333F,0,0,0,0,0,0,0,0,0,0,0,0}},
        {"Lolliop w Cube Ends Partless", new float[]{0.666666F,0,0,0,0,0,0,0.3333333F,0,0.3333333F,0,0.3333333F,0,0.3333333F,0,0,0,0,0.666666F,0,0,0,0,0,0}},
        {"Giant U w Flat Base Partless", new float[]{0,0.3333333F,0,0.3333333F,0,0,0,0.3333333F,0.3333333F,0,0,0,0,0.666666F,0.3333333F,0,0,0,0.666666F,0.3333333F,0,0,0,0,0}},
        {"Big Curved Axe Partless", new float[]{0,0.3333333F,0.3333333F,0,0,0,0,0.3333333F,0.3333333F,0,0,0,0.3333333F,0.3333333F,0.3333333F,0,0,0,0.3333333F,0,0,0,0,0,0}},
        {"Sunflower Partless", new float[]{0,0.3333333F,0,0.3333333F,0,0,0,0,0,0.666666F,0,0,0.3333333F,0,0.3333333F,0,0.3333333F,0,0.666666F,0.3333333F,0,0,0,0,0}},
        {"Air Horn", new float[]{0,0,0,0.3333333F,0.3333333F,0,0,0,0,0.666666F,0,0,0,0,0,0.3333333F,0,0,0.3333333F,0,0,0,0,0,0}},
        {"Corn Dog", new float[]{0,0.666666F,0,0,0,0,0,0.3333333F,0.3333333F,0,0,0.3333333F,0,0.3333333F,0,0,0,0,0.666666F,0,0,0,0,0,0}},
        {"Giant Electric Plug", new float[]{0.3333333F,0.666666F,0,0,0,0,0,0.3333333F,0.3333333F,0.3333333F,0,0,0.3333333F,0.3333333F,0.3333333F,0,0,0,0,0.3333333F,0,0,0,0,0}},
        {"Ring w Tail End", new float[]{0,0.3333333F,0,0,0,0.3333333F,0,0,0.666666F,0,0,0,0.3333333F,0,0.666666F,0,0,0.3333333F,0,0,0,0,0.3333333F,0,0}},
        {"Ring w Inward Spokes", new float[]{0,0.3333333F,0,0,0,0.666666F,0,0.3333333F,0.666666F,0,0,0,0,1,1,0,0,0,0,0,0,0,0,0,0}},
        {"Large Ring w Outward Spokes", new float[]{0,0.3333333F,0,0,0,0.3333333F,0,0.3333333F,0,0.3333333F,0,0,0.3333333F,0,0.3333333F,0,0,0,0,0,0,0.3333333F,0,0,0}},
        {"Tube w Cones", new float[]{0,0.3333333F,0,0,0.3333333F,0,0,0.3333333F,0,0.3333333F,0,0,0.3333333F,0,0,0,0,0,0,0.3333333F,0,0,0.3333333F,0,0}},
        {"Thin Stick w Hammer End", new float[]{0,0.666666F,0,0,0,0,0,0.666666F,0,0,0,0.3333333F,0,0.3333333F,0,0,0,0,0.666666F,0,0,0,0,0,0}},
        {"Horseshoe", new float[]{0,0.3333333F,0,0,0.3333333F,0,0,0,0,0,0,0.3333333F,0.3333333F,0,0,0,0,0,0,0,0,0,0,0,0}},
        {"Helix", new float[]{0,0,0,0,0,0,0.3333333F,0,0.3333333F,0,0,0,0.3333333F,0,0,0,0,0,0,0,0,0,0.3333333F,0,0}},
        {"Giant Golf T", new float[]{0,0,0,0.3333333F,0.3333333F,0,0,0,0.3333333F,0.3333333F,0,0,0.3333333F,0,0.3333333F,0,0.3333333F,0,0.666666F,0,0,0,0,0,0}},
        {"Circle With Ring", new float[]{0,0,0,0.3333333F,0,0.3333333F,0,0,0,0.666666F,0,0,0,0,0.666666F,0,0,0,0.666666F,0,0,0,0,0,0}},
        {"Palm Tree Partless", new float[]{0,0,0,0,0.666666F,0,0,0,0.3333333F,0.3333333F,0,0.3333333F,0.3333333F,0,0,0,0,0,0.666666F,0,0,0,0,0,0}},
        {"Ladle", new float[]{0,0.3333333F,0,0.3333333F,0,0,0,0.3333333F,0.3333333F,0,0,0,0.3333333F,0,0.3333333F,0,0.3333333F,0,0.666666F,0,0,0,0,0,0}},
        {"Plunger", new float[]{0,0.666666F,0,0,0.3333333F,0,0,0,0.3333333F,0.3333333F,0,0,0,0,0,1,0,0,0.666666F,0,0,0,0,0,0}}
    };
    private Dictionary<string, float[]> propDefaultNameAffordanceLookup = new Dictionary<string, float[]> {
        {"NULL", new float[]{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}}
    };
    float[] initialRandomVariables;

    /// <summary>
    /// Start this instance.
    /// </summary>
    public void Start()
    {
        initialRandomVariables = new float[nLatents];

        playback = gameObject.GetComponent<AgentPlayer>();
        propManager = gameObject.GetComponent<PropManager>();

        graphAsset = Resources.Load("affordance-cvae-27000-2-final.ckpt") as TextAsset;
        
        float[,] param = new float[1, initialRandomVariables.Length + propDefaultNameAffordanceLookup["NULL"].Length];
        float[] values = Concatenate1DArrays<float>(initialRandomVariables, propDefaultNameAffordanceLookup["NULL"]);
        for (int i = 0; i < values.Length; i++)
        {
            param[0, i] = values[i];
        }
        Dictionary<string, dynamic> initInputs = new Dictionary<string, dynamic>() {
            { "concat_1", param},
            { "keep_prob", 1.0F }
        };
        string outputNodeName = "decoder/Reshape_1";
        isInit = model.InitializeModel(graphAsset.bytes, initInputs, outputNodeName);
        if (isInit)
        {
            Debug.Log("Model initialized");
        }
        else
        {
            Debug.Log("Model not initialized");
        }
    }

   
    /// <summary>
    /// Update this instance.
    /// </summary>
    public void Update()
    {
        if (isInit)
        {
            HandleInput();
        }

        if (isInit && generate)
        {
            GenerateAction();
        }
    }

    private string Rank2ArrayToString<T>(T[,] array, string separator)
    {
        string result = "[";
        foreach (T cell in array)
        {
            result += "" + cell.ToString() + separator;
        }
        return result.Substring(0, result.Length - separator.Length) + "]";
    }

    /// <summary>
    /// Handle input.
    /// </summary>
    private void HandleInput()
    {
        //if (Input.GetKeyDown (KeyCode.Return))
        //{
        //	generate = true;
        //}
    }

    /// <summary>
    /// Concatenates the 1D arrays.
    /// </summary>
    /// <returns>The concatenated 1D array.</returns>
    /// <param name="a">The first 1D array.</param>
    /// <param name="b">The second 1D array.</param>
    private T[] Concatenate1DArrays<T>(T[] a, T[] b)
    {
        T[] result = new T[a.Length + b.Length];
        Array.Copy(a, 0, result, 0, a.Length);
        Array.Copy(b, 0, result, a.Length, b.Length);
        return result;
    }

    /// <summary>
    /// Gets a random value from dictionary.
    /// </summary>
    /// <returns>The random value from dictionary.</returns>
    /// <param name="dictionary">Dictionary.</param>
    /// <typeparam name="TKey">The key's type parameter.</typeparam>
    /// <typeparam name="TValue">The value's type parameter.</typeparam>
    public TValue GetRandomValueFromDictionary<TKey, TValue>(IDictionary<TKey, TValue> dictionary)
    {
        List<TValue> values = Enumerable.ToList(dictionary.Values);
        int size = dictionary.Count;
        return values[random.Next(size)];
    }

    /// <summary>
    /// Gets a random key from dictionary.
    /// </summary>
    /// <returns>The random key from dictionary.</returns>
    /// <param name="dictionary">Dictionary.</param>
    /// <typeparam name="TKey">The key's type parameter.</typeparam>
    /// <typeparam name="TValue">The value's type parameter.</typeparam>
    public TKey GetRandomKeyFromDictionary<TKey, TValue>(IDictionary<TKey, TValue> dictionary)
    {
        List<TKey> keys = Enumerable.ToList(dictionary.Keys);
        int size = dictionary.Count;
        return keys[random.Next(size)];
    }

    public void GenerateActionWithRandomProp()
    {
        RecordedGesture.isGestureTooShort = true;
        while (RecordedGesture.isGestureTooShort == true)
        {
            float[] samples = new float[nLatents];
            for (int i = 0; i < nLatents; i++)
            {
                samples[i] = (float)random.NextDouble() * 6 - 3;
            }
            string propName = GetRandomKeyFromDictionary(propNameAffordanceLookup);
            Debug.Log("Prop Name: " + propName);
            float[,] param = new float[1, samples.Length + propNameAffordanceLookup[propName].Length];
            float[] values = Concatenate1DArrays<float>(samples, propNameAffordanceLookup[propName]);
            for (int i = 0; i < values.Length; i++)
            {
                param[0, i] = values[i];
            }
            string input = Rank2ArrayToString<float>(param, ", ");
            Dictionary<string, dynamic> generatorInputs = new Dictionary<string, dynamic>() {
                { "concat_1", param },
                { "keep_prob", 1.0F }
            };
            string outputNodeName = "decoder/Reshape_1";
            float[,] output = model.GenerateOutput(generatorInputs, outputNodeName) as float[,];
            Debug.Log("Result successfully obtained. Results in form: " + output.GetLength(0) + "x" + output.GetLength(1));

            playback.recording = new RecordedGesture(output, propName, true, 0);
            propManager.SetPropName(propName);

            Debug.Log("Finished loading generated gesture");
        }

        generate = false;
    }

    public void GenerateAction()
    {
        RecordedGesture.isGestureTooShort = true;
        while (RecordedGesture.isGestureTooShort == true)
        {
            float[] samples = new float[nLatents];
            for (int i = 0; i < nLatents; i++)
            {
                samples[i] = (float)random.NextDouble() * 6 - 3;
            }
            string propName = propManager.GetCurrentPropName();

            Debug.Log("Prop Name: " + propName);
            //playback.recording = new RecordedGesture(FindAction(samples, propName));

            Debug.Log("Finished loading generated gesture");
        }
        GestureDefinition gestureDefinition = playback.recording.GetGesture();
        string pan = LoadGesture.KNNLabel(gestureDefinition.coordinateIndex, 3, false, true);
        string pon = LoadGesture.KNNLabel(gestureDefinition.coordinateIndex, 3, false, false);
        GestureCoordinate cindex = gestureDefinition.coordinateIndex;
        playback.recording.GetGesture().pretendActionName = LoadGesture.KNNLabel(playback.recording.GetGesture().coordinateIndex, 3, false, true);
        playback.recording.GetGesture().pretendObjectName = LoadGesture.KNNLabel(playback.recording.GetGesture().coordinateIndex, 3, false, false);
        Debug.Log("Finished loading generated gesture");
        Debug.Log("pretendActionName: " + playback.recording.GetGesture().pretendActionName + " PretendObjectName: " + playback.recording.GetGesture().pretendObjectName);
        generate = false;
    }

    /// <summary>
    /// Generates the latent space coordinate of a given gesture.
    /// </summary>
    /// <param name="gesture">The gesture to be evaluated</param>
    /// <returns>List of coordinate values</returns>
    public List<float> GenerateLatentSpaceCoordinate(GestureDefinition gesture)
    {
        GestureVector vector = new GestureVector(gesture, 0, true, true, nInputs, 0);

        float[] inputTimeSeries = vector.To1DFloat();
        float[] affordance = propNameAffordanceLookup[gesture.objectName];

        float[,] param = new float[1, inputTimeSeries.Length + affordance.Length];
        float[] values = Concatenate1DArrays<float>(inputTimeSeries, affordance);
        for (int i = 0; i < values.Length; i++)
        {
            param[0, i] = values[i];
        }
        Dictionary<string, dynamic> generatorInputs = new Dictionary<string, dynamic>() {
            { "concat", param },
            { "keep_prob", 1.0F }
        };

        // Generating output inference
        float[,] outputMeans = model.GenerateOutput(generatorInputs, "encoder/dense/BiasAdd") as float[,];
        //Debug.Log("Result successfully obtained. Results in form: " + outputMeans.GetLength(0) + "x" + outputMeans.GetLength(1));
        
        // Finding the input's latent space value
        List<float> spaceCoordinate = new List<float>();
        foreach (float val in outputMeans)
        {
            //Debug.Log("" + val);
            spaceCoordinate.Add(val);
        }
        spaceCoordinate.AddRange(affordance);
        //Debug.Log("Finished calculating latent space coordinate");

        return spaceCoordinate;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="samples"></param>
    /// <returns></returns>
    public GestureDefinition FindAction(float[] samples, string propName)
    {
        float[,] param = new float[1, samples.Length + propNameAffordanceLookup[propName].Length];
        float[] values = Concatenate1DArrays<float>(samples, propNameAffordanceLookup[propName]);
        for (int i = 0; i < values.Length; i++)
        {
            param[0, i] = values[i];
        }

        Dictionary<string, dynamic> generatorInputs = new Dictionary<string, dynamic>() {
            { "concat_1", param },
            { "keep_prob", 1.0F }
        };
        string outputNodeName = "decoder/Reshape_1";
        float[,] output = model.GenerateOutput(generatorInputs, outputNodeName) as float[,];
        //Debug.Log("Result successfully obtained. Results in form: " + output.GetLength(0) + "x" + output.GetLength(1));
        GestureDefinition newGesture = new GestureDefinition();
        newGesture.LoadStates(output, propName, true, 0);

        // saving the coordinates
        Envelope env = new Envelope(samples[0], samples[1]);
        newGesture.coordinateIndex = new GestureCoordinate(env);

        return newGesture;
    }

    public FrozenGraphLoader GetModel()
    {
        return model;
    }
}
