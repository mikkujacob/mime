﻿using System;
using System.Collections;
using System.Collections.Generic;
using EpisodicMemory;

// This class will pick the most suitable response strategy for the partner robot to follow
internal class StrategySelectionController
{
    private CreativeArc CArc;
    private TimeSpan duration;
    private Combination combination;
    private Mimicry mimicry;
    private NoveltyGeneration novelGen;
    private SurpriseGeneration surpriseGen;
    private Transformation transformation;
    private ValueModulation valueMod;
    public ResponseStrategy[] Strategies { get; }
    private int index;
    private TemporalMemory TemporalMemory;

    // Use this for initialization
    void Start () {
	}
	
	// Update is called once per frame
	void Update () {
	}

    public StrategySelectionController(TemporalMemory temporalMemory): this(new CreativeArc(), TimeSpan.Zero, temporalMemory) {}
    
    /// <summary>
    /// 
    /// </summary>
    /// <param name="arc">The creative arc that the partner robot's actions should follow</param>
    /// <param name="sessionDuration">The duration of the partner robot's session</param>
    /// <param name="temporalMemory"></param>
    public StrategySelectionController(CreativeArc arc, TimeSpan sessionDuration, TemporalMemory temporalMemory)
    {
        this.CArc = arc;
        this.duration = sessionDuration;
        this.TemporalMemory = temporalMemory;

        combination = new Combination();
        mimicry = new Mimicry();
        novelGen = new NoveltyGeneration();
        surpriseGen = new SurpriseGeneration();
        transformation = new Transformation(temporalMemory);
        valueMod = new ValueModulation();

        Strategies = new ResponseStrategy[] {mimicry, combination, transformation};

        index = 0;
    }

    /// <summary>
    /// Picks a response strategy that will bring the current location closest to the target location within the creative space
    /// </summary>
    /// <param name="lastGesture">The last gesture the robot partner performed</param>
    /// <returns>The most suitable response strategy</returns>
    public ResponseStrategy SelectResponseStrategy(GestureDefinition lastGesture)
    {
        if (lastGesture.IsGestureTooShort && index == 0)
        {
            return Strategies[1];
        } else
        {
            return Strategies[index];
        }
    }

    /// <summary>
    /// Sets the current strategy to the one at index i within the Strategies list
    /// </summary>
    /// <param name="i">The index of the desired response strategy</param>
    public void SetStrategy(int i)
    {
        index = i;
        UnityEngine.Debug.Log("The strategy is now " + Strategies[index]);
    }

}
