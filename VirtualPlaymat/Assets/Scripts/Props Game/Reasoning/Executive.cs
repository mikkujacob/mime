﻿using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;
using System.Threading.Tasks;
using System.Collections.Concurrent;
using System;
using System.Globalization;
using EpisodicMemory;

// Represents the decision cycle for the robot partner.
public class Executive : MonoBehaviour {
    private StrategySelectionController Selector;
    private int CurrentStrategy;

    private TemporalMemory TemporalMemory;

    private BlockingCollection<GestureDefinition> InputQueue;
    public BlockingCollection<GestureDefinition> OutputQueue { get; set; }

    private DeepIMAGINATIONInterface DeepIMAGINATION;
    public RTree<GestureCoordinate> rTree { get; set; }
    private bool busy;

    /// set true to run decision cycle
    public bool IsRunning { get; set; }

    // Use this for initialization
    void Start ()
    {
        InputQueue = new BlockingCollection<GestureDefinition>();
        OutputQueue = new BlockingCollection<GestureDefinition>();

        // Instantiate DeepIMAGINATIONInterface here
        //DeepIMAGINATION = GameObject.Find("MImEGlobalObject").GetComponent<DeepIMAGINATIONInterface>();
        DeepIMAGINATION = GameObject.FindObjectOfType<DeepIMAGINATIONInterface>();

        rTree = new RTree<GestureCoordinate>();

        Restart();
    }

    // Update is called once per frame
    void Update ()
    {
        //if (!busy && isRunning)
        if (IsRunning)
        {
            TryDequeueInput();
        }
        
        InputHandler();
    }
    
    /// <summary>
    /// Adds a new gesture into the input queue for processing
    /// </summary>
    /// <param name="input">The input gesture</param>
    public void AddNewGesture(GestureDefinition input)
    {
        //GestureDefinition newGesture = input.DeepCopy();
        //InputQueue.Add(input);
        GestureDefinition clone = input.DeepCopy();
        InputQueue.Add(clone);
    }

    /// <summary>
    /// Removes the item at the front of the queue. If queue is empty, then it waits.
    /// </summary>
    /// <returns>A <c>Task</c> with the gesture input from the human</returns>
    private void TryDequeueInput()
    {
        GestureDefinition input;
        if (InputQueue.TryTake(out input, TimeSpan.FromMilliseconds(1)))
        {
            busy = true;
            Task cycle = StartDecisionCycle(input);
        }
    }

    /// <summary>
    /// Starts robot partner's decision cycle
    /// </summary>
    /// <param name="playerGesture">The collaborator's most recent gesture</param>
    private async Task StartDecisionCycle(GestureDefinition playerGesture)
    {
        await AddPerceptToMemory(playerGesture);
        var response = await FormResponse(playerGesture);
        PartnerPerform(response);
    }

    /// <summary>
    /// Adds new percept to the agent's memory
    /// <param name="gesture"> The human player's gesture. </param>
    /// </summary>
    private async Task AddPerceptToMemory(GestureDefinition playerGesture)
    {
        var t1 = Task.Run(() => ClassifyPercept(playerGesture));
        var t2 = Task.Run(() => FindCreativeSpaceLocation(playerGesture));

        await Task.WhenAll(t1, t2);

        Envelope envelope = new Envelope(t1.Result[0], t1.Result[1]);
        DateTime timestamp = StringToDateTime(playerGesture.timestamp);
        Guid guid = GenerateGuid(t1.Result, timestamp);
        TemporalMemory.InsertAtEnd(guid);
        //Debug.Log("GUID: " + guid.ToString());
        GestureCoordinate coordinate = new GestureCoordinate(envelope, guid);
        
        playerGesture.coordinateIndex = coordinate;

        // add to memory r tree
        rTree.Insert(playerGesture.coordinateIndex);
    }

    /// <summary>
    /// Decides the partner robot's next action
    /// </summary>
    private async Task<GestureDefinition> FormResponse(GestureDefinition gesture)
    {
        var strategy = Selector.SelectResponseStrategy(gesture);
        var gestures = await GenerateActions(strategy, gesture);
        //Debug.Log("Finished generating gestures. Formed " + gestures.Count + " potential gestures");
        var response = await PickAction(gestures);
        //Debug.Log("Selected response using " + response.GetGesture().objectName);
        return response;
    }

    /// <summary>
    /// Identifies percept with similarities from previously
    /// stored percepts within the agent's memory
    /// <param name="playerGesture"> The human player's gesture. </param>
    /// </summary>
    private List<float> ClassifyPercept(GestureDefinition playerGesture)
    {
        // find latent space coordinate
        List<float> coorList = DeepIMAGINATION.GenerateLatentSpaceCoordinate(playerGesture);
        return coorList;
    }

    /// <summary>
    /// Evaluates a gesture's novelty, surprise, and value
    /// to locate where the action would exist within the 
    /// 3D creative space
    /// <param name="gesture"> Action to be identified within the 3D creative space. </param>
    /// </summary>
    private Score[] FindCreativeSpaceLocation(GestureDefinition gesture)
    {
        // Evaluation code
        return new Score[3];
    }

    /// <summary>
    /// Creates candidate actions for the agent to perform.
    /// <returns> A list of suitable candidate actions. </returns>
    /// </summary>
    private async Task<List<GestureDefinition>> GenerateActions(ResponseStrategy strategy, GestureDefinition playerGesture)
    {
        // will generate actions using DeepIMAGINATION
        var responses = Task.Run(()=> strategy.GenerateGestures(playerGesture));
        //Debug.Log(responses.Result.Count + " responses generated");
        return await responses;
    }
    
    /// <summary>
    /// Selects the action whose creative space location best 
    /// matches with the target location along the creative arc
    /// <param name="gestures"> Suitable candidate actions </param>
    /// <returns> The action that's closest to the target location within the creative arc </returns>
    /// </summary>
    private async Task<GestureDefinition> PickAction(List<GestureDefinition> gestures)
    {
        // will evaluate gestures within creative space
        await Task.Delay(1);
        return gestures[0];
    }

    /// <summary>
    /// Sends gesture to action output queue.
    /// </summary>
    /// <param name="response">The gesture to be sent</param>
    private void PartnerPerform(GestureDefinition response)
    {
        // push into queue
        OutputQueue.Add(response);
        //Debug.Log("Finished decision cycle for " + response.objectName);
        busy = false;
    }

    /// <summary>
    /// Returns the next gesture within the queue. Returns NULL if queue is empty.
    /// </summary>
    /// <returns>The next gesture to perform</returns>
    public GestureDefinition ReadFromOutputQueue()
    {
        GestureDefinition gesture;
        if (OutputQueue.TryTake(out gesture, TimeSpan.FromMilliseconds(1)))
        {
            return gesture;
        } else
        {
            return null;
        }
    }
    
    /// <summary>
    /// Sets the initial values for the Decision Cycle
    /// </summary>
    public void Restart()
    {
        InputQueue.Dispose();
        OutputQueue.Dispose();
        InputQueue = new BlockingCollection<GestureDefinition>();
        OutputQueue = new BlockingCollection<GestureDefinition>();

        TemporalMemory = new TemporalMemory();
        
        rTree.Clear();
        Selector = new StrategySelectionController(TemporalMemory);
        CurrentStrategy = 0;

        busy = false;
        IsRunning = true;
    }

    //private void TestSetUp()
    //{
    //    RecordedGesture gesture = new RecordedGesture("2018-01-30-15-47-02-ms-000.gesture");
    //    gesture.GetGesture().propObject = new PropDefinition("Thin Stick w Hammer End", new float[] { 0, 0.666666F, 0, 0, 0, 0, 0, 0.666666F, 0, 0, 0, 0.3333333F, 0, 0.3333333F, 0, 0, 0, 0, 0.666666F, 0, 0, 0, 0, 0, 0 });
    //    AddNewGesture(gesture.GetGesture());

    //    IsRunning = true;
    //}

    //private void TestSetUp2()
    //{
    //    RecordedGesture gesture1 = new RecordedGesture("2018-01-30-15-47-14-ms-000.gesture");
    //    gesture1.GetGesture().propObject = new PropDefinition("Giant Electric Plug", new float[] { 0.3333333F, 0.666666F, 0, 0, 0, 0, 0, 0.3333333F, 0.3333333F, 0.3333333F, 0, 0, 0.3333333F, 0.3333333F, 0.3333333F, 0, 0, 0, 0, 0.3333333F, 0, 0, 0, 0, 0 });
    //    AddNewGesture(gesture1.GetGesture());

    //    IsRunning = true;
    //}

    /// <summary>
    /// Handles the inputs from keyboard
    /// </summary>
    private void InputHandler()
    {
        if (Input.GetKeyDown("r"))
        {
            SwitchStrategies();
        }
        //else if (Input.GetKeyDown("e"))
        //{
        //    TestSetUp();
        //}
        //else if (Input.GetKeyDown("w"))
        //{
        //    TestSetUp2();
        //}
        //else if (Input.GetKeyDown("q"))
        //{
        //    Restart();
        //}
    }

    /// <summary>
    /// Changes the current response strategy to the next one in the list
    /// </summary>
    private void SwitchStrategies()
    {
        CurrentStrategy = (CurrentStrategy + 1) % Selector.Strategies.Length;
        Selector.SetStrategy(CurrentStrategy);
    }

    /// <summary>
    /// Generates a GUID based on the RecordedGesture's latent space coordinates and timestamp
    /// </summary>
    /// <param name="coordinate">The latent space coordinate</param>
    /// <param name="time">The timestamp of the gesture</param>
    /// <returns>A new GUID</returns>
    private Guid GenerateGuid(List<float> coordinate, DateTime time)
    {
        byte[] means = FloatToByte(coordinate.GetRange(0, 2).ToArray());
        var timeBytes = BitConverter.GetBytes(time.Ticks);
        byte[] guid = new byte[16];

        for (int i = 0; i < means.Length; i++)
        {
            guid[i] = means[i];
        }

        for (int i = means.Length; i < guid.Length; i++)
        {
            guid[i] = timeBytes[i - means.Length];
        }

        return new Guid(guid);
    }

    /// <summary>
    /// Converts a string representation of a date into a DateTime object.
    /// The format of the string must match "yyyy-MM-dd-HH-mm-ss-fff"
    /// </summary>
    /// <param name="timestamp">The string with the proper date/time format</param>
    /// <returns>A DateTime object form of the string</returns>
    private DateTime StringToDateTime(string timestamp)
    {
        string format = "yyyy-MM-dd-HH-mm-ss-fff";
        DateTime time = DateTime.ParseExact(timestamp, format, CultureInfo.InvariantCulture);
        return time;
    }

    /// <summary>
    /// Changes a float array into a byte array
    /// </summary>
    /// <param name="floatArr">The array to change to bytes</param>
    /// <returns>An array of bytes</returns>
    private byte[] FloatToByte(float[] floatArr)
    {
        byte[] byteArr = new byte[floatArr.Length * 4];
        for (int i = 0; i < floatArr.Length; i++)
        {
            byte[] tempBytes = BitConverter.GetBytes(floatArr[i]);
            byteArr[i * 4 + 0] = tempBytes[0];
            byteArr[i * 4 + 1] = tempBytes[1];
            byteArr[i * 4 + 2] = tempBytes[2];
            byteArr[i * 4 + 3] = tempBytes[3];
        }

        return byteArr;
    }
}
