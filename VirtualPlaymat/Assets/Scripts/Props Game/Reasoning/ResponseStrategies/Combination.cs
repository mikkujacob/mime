﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

// Tells the agent to generate gestures by interpolating multiple points within the latency space
internal class Combination : ResponseStrategy
{
    private System.Random random;

    public Combination() : base() {
        random = new System.Random();
    }

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

    }
    
    public override List<GestureDefinition> GenerateGestures(GestureDefinition gesture)
    {
        List<GestureCoordinate> gestureCoordinates = rTree.Search(gesture.coordinateIndex.Envelope).ToList<GestureCoordinate>();
        List<GestureDefinition> returnedGestures = new List<GestureDefinition>();
        string propName = gesture.propObject.Name;

        List<float[]> coorList = ChangeToFloatList(gestureCoordinates);
        bool first = true;
        GestureDefinition response = new GestureDefinition
        {
            IsGestureTooShort = true
        };

        for (int i = 0; i < coorList.Count; i++)
        {
            while (response.IsGestureTooShort || first)
            {
                List<float[]> randomCoor = GenerateRandomCoordinates(1);

                float[] samples = FindCentroid(coorList[i], randomCoor[0]);
                //Debug.Log("The input gesture is (" + coorList[i][0] + ", " + coorList[i][1]
                //    + "). The random gesture is (" + randomCoor[0][0] + ", " + randomCoor[0][1] + ")");

                //Debug.Log("The resulting gesture is ( " + samples[0] + ", " + samples[1] + ")");
                response = DeepIMAGINATION.FindAction(samples, propName);

                first = false;
            }
            returnedGestures.Add(response);
        }
        return returnedGestures;
    }

    private List<float[]> GenerateRandomCoordinates(int amountToGenerate)
    {
        List<float[]> coordinates = new List<float[]>();
        for (int i = 0; i < amountToGenerate; i++)
        {
            float[] coordinate = new float[nLatents];
            for (int j = 0; j < nLatents; j++)
            {
                coordinate[j] = (float)random.NextDouble() * 6 - 3;
            }
            coordinates.Add(coordinate);
        }

        return coordinates;
    }

    private float[] FindCentroid(float[] c1, float[] c2)
    {
        float[] weights = new float[] {0.5f, 0.5f};
        List<float[]> coordinates = new List<float[]>();

        coordinates.Add(c1);
        coordinates.Add(c2);

        return Interpolate(coordinates, weights);
    }

    private float[] Interpolate(List<float[]> coordinates, float[] weights)
    {
        float m1 = 0;
        float m2 = 0;

        for(int i = 0; i < coordinates.Count; i++)
        {
            m1 += coordinates[i][0] * weights[i];
            m2 += coordinates[i][1] * weights[i];
        }

        return new float[] { m1, m2 };
    }
}
