﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System;

// Tells agent to generate the same gesture as the user
internal class Mimicry : ResponseStrategy
{
    public Mimicry() : base() { }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    
    public override List<GestureDefinition> GenerateGestures(GestureDefinition gesture)
    {
        List<GestureCoordinate> gestureCoordinate = rTree.Search(gesture.coordinateIndex.Envelope).ToList<GestureCoordinate>();
        List<GestureDefinition> returnedGestures = new List<GestureDefinition>();
        string propName = gesture.propObject.Name;

        float[] coordinate = new float[] {(float) gestureCoordinate[0].Envelope.CenterX,
            (float) gestureCoordinate[0].Envelope.CenterY};

        float[] samples = new float[nLatents];
        for (int i = 0; i < nLatents; i++)
        {
            samples[i] = coordinate[i];
        }

        //Debug.Log("The resulting gesture is ( " + samples[0] + ", " + samples[1] + ")");
        GestureDefinition response = DeepIMAGINATION.FindAction(samples, propName);
        Envelope env = new Envelope(samples[0], samples[1]);
        response.coordinateIndex = new GestureCoordinate(env);
        returnedGestures.Add(response);

        return returnedGestures;
    }
}
