﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using EpisodicMemory;
using System;
using System.Numerics;
using Vector2 = System.Numerics.Vector2;

// Tells agent to generate a response based a response to a similar gesture in the past.
internal class Transformation : ResponseStrategy
{
    private readonly TemporalMemory TemporalMemory;
    private readonly int k = 1;
    private string propName;
    private List<GestureDefinition> responseGestures;
    private System.Random random;
    private float max = 2.0f;
    private float min = 0.5f;

    public Transformation(TemporalMemory temporalMemory) : base()
    {
        TemporalMemory = temporalMemory;
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

    }

    public override List<GestureDefinition> GenerateGestures(GestureDefinition gesture)
    {
        responseGestures = new List<GestureDefinition>();

        float[] inputGesture = CoordinateToFloat(gesture.coordinateIndex);
        Vector2 v2Gesture = new Vector2(inputGesture[0], inputGesture[1]);

        //Debug.Log("The base gesture is ( " + v2Gesture.X + ", " + v2Gesture.Y + ")");

        propName = gesture.objectName;
        
        GestureCoordinate coordinate = gesture.coordinateIndex;
        int timestamp = TemporalMemory.getTemporalIndex(coordinate.Guid);
        TemporalMemory.Node t1 = TemporalMemory.GetKthNodePrev(timestamp, 2);
        TemporalMemory.Node t2 = TemporalMemory.GetKthNodePrev(timestamp, 1);
        Vector2 t1t2 = FindVector(t1.id, t2.id);

        GenerateFromPrevious2(v2Gesture, t1t2);
        GenerateFromSimilarResponse(gesture, v2Gesture, k);
        GenerateFromMagnitudeTransform(v2Gesture, t1t2);
        GenerateFromRotationTransform(v2Gesture, t1t2);
        GenerateFrom180Rotation(v2Gesture, t1t2);

        Debug.Log(responseGestures.Count);

        return responseGestures;
    }

    /// <summary>
    /// Looks at previous two actions and generates a new gesture based
    /// on their vector relationship
    /// </summary>
    /// <param name="v2Gesture">The gesture to be transformed</param>
    private void GenerateFromPrevious2(Vector2 v2Gesture, Vector2 t1t2)
    {
        Vector2 responseCoor = Vector2.Add(v2Gesture, t1t2);

        //Debug.Log("Generating from previous 2 responses");
        //Debug.Log("The resulting gesture is ( " + responseCoor.X + ", " + responseCoor.Y + ")");
        GestureDefinition response = DeepIMAGINATION.FindAction(
            new float[] { responseCoor.X, responseCoor.Y }, propName);
        responseGestures.Add(response);
    }

    /// <summary>
    /// Looks at the most similar action to gesture and the action after it
    /// to generate a new gesture based on the two previous actions' vector 
    /// relationship
    /// </summary>
    /// <param name="gesture">The gesture to be transformed</param>
    /// <param name="k">The amount of gestures to generate</param>
    private void GenerateFromSimilarResponse(GestureDefinition gesture, Vector2 v2Gesture, int k)
    {
        List<ISpatialData> similarGestures = rTree.KNearestNeighbor(gesture.coordinateIndex, k);

        foreach (GestureCoordinate coordinate in similarGestures)
        {
            int timestamp = TemporalMemory.getTemporalIndex(coordinate.Guid);
            TemporalMemory.Node node = TemporalMemory.GetKthNodeNext(timestamp, 1);
            
            Vector2 responseCoor = ApplyTranslation(coordinate.Guid, node.id, v2Gesture);
            //Debug.Log("Generating from Similar Response");
            //Debug.Log("The resulting gesture is ( " + responseCoor.X + ", " + responseCoor.Y + ")");

            GestureDefinition response = DeepIMAGINATION.FindAction(
                new float[] { responseCoor.X, responseCoor.Y }, propName);
            responseGestures.Add(response);
        }
    }

    private void GenerateFromMagnitudeTransform(Vector2 v2Gesture, Vector2 t1t2)
    {
        float scalar = (float) random.NextDouble() * (max - min) + min;
        Vector2 scaledVector = Vector2.Multiply(t1t2, scalar);
        Vector2 responseCoor = Vector2.Add(v2Gesture, scaledVector);

        GestureDefinition response = DeepIMAGINATION.FindAction(
                new float[] { responseCoor.X, responseCoor.Y }, propName);
        responseGestures.Add(response);
    }

    private void GenerateFromRotationTransform(Vector2 v2Gesture, Vector2 t1t2)
    {
        float degree = (float) random.NextDouble() * 360;
        Vector2 direction = Vector2Extension.Rotate(t1t2, degree);
        Vector2 responseCoor = Vector2.Add(v2Gesture, direction);

        GestureDefinition response = DeepIMAGINATION.FindAction(
                new float[] { responseCoor.X, responseCoor.Y }, propName);
        responseGestures.Add(response);
    }

    private void GenerateFrom180Rotation(Vector2 v2Gesture, Vector2 t1t2)
    {
        Vector2 direction = Vector2Extension.Rotate(t1t2, 180);
        Vector2 responseCoor = Vector2.Add(v2Gesture, direction);

        GestureDefinition response = DeepIMAGINATION.FindAction(
                new float[] { responseCoor.X, responseCoor.Y }, propName);
        responseGestures.Add(response);
    }

    /// <summary>
    /// Finds the vector between the points represented by g1 and g2
    /// And applies it to the current gesture to generate a new one
    /// </summary>
    /// <param name="g1">The first gesture</param>
    /// <param name="g2">The second gesture</param>
    /// <returns>The new gesture generated from the current gesture and vector</returns>
    private Vector2 ApplyTranslation(Guid g1, Guid g2, Vector2 t3)
    {
        float[] t1 = FindCoordinates(g1);
        float[] t2 = FindCoordinates(g2);
        Vector2 t1t2 = FindVector(t1, t2);

        Vector2 t4 = Vector2.Add(t3, t1t2);
        return t4;
    }

    /// <summary>
    /// Returns the directional vector from the current gesture to the next one
    /// </summary>
    /// <param name="current">The coordinates of the current gesture</param>
    /// <param name="next">The coordinates of the gesture that occur after the current gesture</param>
    /// <returns>A vector representing the direction between the two gestures</returns>
    private Vector2 FindVector(float[] current, float[] next)
    {
        float x = next[0] - current[0];
        float y = next[1] - current[1];

        //Debug.Log("T1 is (" + current[0] + "," + current[1] + ")");
        //Debug.Log("T2 is (" + next[0] + "," + next[1] + ")");

        return new Vector2(x, y);
    }

    private Vector2 FindVector(Guid g1, Guid g2)
    {
        float[] c1 = FindCoordinates(g1);
        float[] c2 = FindCoordinates(g2);

        return FindVector(c1, c2);
    }

    /// <summary>
    /// Returns the coordinates represented by the guid
    /// </summary>
    /// <param name="guid">The guid of a GestureCoordinate</param>
    /// <returns>Two floats representing the gesture's latent space coordinates</returns>
    private float[] FindCoordinates(Guid guid)
    {
        byte[] bytes = guid.ToByteArray();
        float m1 = BitConverter.ToSingle(bytes, 0);
        float m2 = BitConverter.ToSingle(bytes, 4);

        return new float[] { m1, m2 };
    }
}
