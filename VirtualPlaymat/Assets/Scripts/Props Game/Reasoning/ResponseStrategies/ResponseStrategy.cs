﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System;

// Guides how the agent will navigate through the creative space
internal class ResponseStrategy
{
    protected DeepIMAGINATIONInterface DeepIMAGINATION;
    protected RTree<GestureCoordinate> rTree;
    protected FrozenGraphLoader model;
    protected int nLatents;
    protected PropManager propManager;

    // Use this for initialization
    void Start () { 
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public ResponseStrategy()
    {
        DeepIMAGINATION = UnityEngine.Object.FindObjectOfType<DeepIMAGINATIONInterface>();
        rTree = UnityEngine.Object.FindObjectOfType<Executive>().rTree;
        model = DeepIMAGINATION.GetModel();
        nLatents = DeepIMAGINATION.nLatents;
        propManager = UnityEngine.Object.FindObjectOfType<PropManager>();
    }

    /// <summary>
    /// Generates candidate actions based off of a single input.
    /// </summary>
    /// <param name="gesture">The gesture to base actions off of</param>
    /// <returns>A list of potential response gestures</returns>
    public virtual List<GestureDefinition> GenerateGestures(GestureDefinition gesture)
    {
        return new List<GestureDefinition>();
    }
    
    protected List<float[]> ChangeToFloatList(List<GestureCoordinate> list)
    {
        List<float[]> returnList = new List<float[]>();

        foreach (GestureCoordinate item in list)
        {
            float[] coordinate = CoordinateToFloat(item);
            returnList.Add(coordinate);
        }
        return returnList;
    }

    protected float[] CoordinateToFloat(GestureCoordinate item)
    {
        float[] coordinate = new float[] {(float) item.Envelope.CenterX,
                (float) item.Envelope.CenterY};
        return coordinate;
    }
}
