﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

// Tells agent to generate gestures from a different latency space than the current one
internal class NoveltyGeneration : ResponseStrategy {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

    }

    public override List<GestureDefinition> GenerateGestures(GestureDefinition gesture)
    {
        return new List<GestureDefinition>();
    }
}
