﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

// Tells agent to generate gestures aimed toward increasing or decreasing value scores
internal class ValueModulation: ResponseStrategy
{

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

    }

    public override List<GestureDefinition> GenerateGestures(GestureDefinition gesture)
    {
        return new List<GestureDefinition>();
    }
}
