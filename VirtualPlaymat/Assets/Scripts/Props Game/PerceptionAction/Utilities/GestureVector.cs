﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GestureVector
{
    public enum VectorType { FourJointPosRotTwoButtonSequence, FiveJointPosRotSequence }

    public List<float> list;
    public GestureDefinition gesture;
    public VectorType type = VectorType.FourJointPosRotTwoButtonSequence;
    private float[,] _vector;
    public float[,] vector
    {
        get
        {
            if (!isVectorClean)
            {
                _vector = new float[list.Count, 1];

                for (int index = 0; index < list.Count; index++)
                {
                    _vector[index, 0] = list[index];
                }

                isVectorClean = true;
            }

            return _vector;
        }
    }
    private bool isVectorClean = false;

    public GestureVector() : this(new List<float>(), null) {}

    public GestureVector(GestureVector vector) : this(vector.list, vector.gesture) {}

    public GestureVector(GestureDefinition gesture, int skipN = 0, bool reMap = false, bool isPadded = false, int length = -1, float paddingValue = -1)
    {
        this.gesture = new GestureDefinition(gesture);
        this.list = ParseGesture(this.gesture, skipN, reMap, isPadded, length, paddingValue);
    }

    public GestureVector(List<float> list, GestureDefinition gesture)
    {
        this.list = new List<float>(list);
        this.gesture = new GestureDefinition(gesture);

        isVectorClean = false;
    }

    public List<float> ParseGesture(GestureDefinition gesture, int skipN, bool reMap, bool isPadded, int length, float paddingValue)
    {
        List<float> list = new List<float>();
        int skipCount = 0;
        bool leftControllerDown = false;
        bool rightControllerDown = false;
        int frameLength = 1;

        for (int index = 0; index < gesture.playerStates.Count && (!isPadded || index < length); index++)
        {
            if (gesture.playerStates[index].leftControllerGripButtonDown)
            {
                leftControllerDown = true;
            }
            else if (gesture.playerStates[index].leftControllerGripButtonUp)
            {
                leftControllerDown = false;
            }

            if (gesture.playerStates[index].rightControllerGripButtonDown)
            {
                rightControllerDown = true;
            }
            else if (gesture.playerStates[index].rightControllerGripButtonUp)
            {
                rightControllerDown = false;
            }

            if(skipN > 0)
            {
                if (skipCount != 0)
                {
                    skipCount = (skipCount + 1) % (skipN + 1);
                    continue;
                }
                else
                {
                    skipCount++;
                }
            }

            PlayerStateDefinition state = gesture.playerStates[index];

            if (type == VectorType.FiveJointPosRotSequence)
            {
                //JointData per frame is formatted as: HeadPosition, HeadRotation, LeftHandPosition, LeftHandRotation, RightHandPosition, RightHandRotation, PelvisPosition, PelvisRotation, objectPosition, objectRotation

                frameLength = 35;
                if (reMap)
                {
                    //Head
                    list.Add(Remap(state.headPosition.x, -2, 2, 0, 1)); list.Add(Remap(state.headPosition.y, -2, 2, 0, 1)); list.Add(Remap(state.headPosition.z, -2, 2, 0, 1));
                    list.Add(Remap(state.headRotation.x, -2, 2, 0, 1)); list.Add(Remap(state.headRotation.y, -2, 2, 0, 1)); list.Add(Remap(state.headRotation.z, -2, 2, 0, 1)); list.Add(Remap(state.headRotation.w, -2, 2, 0, 1));
                    //Left Hand
                    list.Add(Remap(state.leftHandPosition.x, -2, 2, 0, 1)); list.Add(Remap(state.leftHandPosition.y, -2, 2, 0, 1)); list.Add(Remap(state.leftHandPosition.z, -2, 2, 0, 1));
                    list.Add(Remap(state.leftHandRotation.x, -2, 2, 0, 1)); list.Add(Remap(state.leftHandRotation.y, -2, 2, 0, 1)); list.Add(Remap(state.leftHandRotation.z, -2, 2, 0, 1)); list.Add(Remap(state.leftHandRotation.w, -2, 2, 0, 1));
                    //Right Hand
                    list.Add(Remap(state.rightHandPosition.x, -2, 2, 0, 1)); list.Add(Remap(state.rightHandPosition.y, -2, 2, 0, 1)); list.Add(Remap(state.rightHandPosition.z, -2, 2, 0, 1));
                    list.Add(Remap(state.rightHandRotation.x, -2, 2, 0, 1)); list.Add(Remap(state.rightHandRotation.y, -2, 2, 0, 1)); list.Add(Remap(state.rightHandRotation.z, -2, 2, 0, 1)); list.Add(Remap(state.rightHandRotation.w, -2, 2, 0, 1));
                    //Pelvis
                    list.Add(Remap(state.skeletalPositions[1].x, -2, 2, 0, 1)); list.Add(Remap(state.skeletalPositions[1].y, -2, 2, 0, 1)); list.Add(Remap(state.skeletalPositions[1].z, -2, 2, 0, 1));
                    list.Add(Remap(state.skeletalRotations[1].x, -2, 2, 0, 1)); list.Add(Remap(state.skeletalRotations[1].y, -2, 2, 0, 1)); list.Add(Remap(state.skeletalRotations[1].z, -2, 2, 0, 1)); list.Add(Remap(state.skeletalRotations[1].w, -2, 2, 0, 1));
                    //Object
                    list.Add(Remap(state.objectPosition.x, -2, 2, 0, 1)); list.Add(Remap(state.objectPosition.y, -2, 2, 0, 1)); list.Add(Remap(state.objectPosition.z, -2, 2, 0, 1));
                    list.Add(Remap(state.objectRotation.x, -2, 2, 0, 1)); list.Add(Remap(state.objectRotation.y, -2, 2, 0, 1)); list.Add(Remap(state.objectRotation.z, -2, 2, 0, 1)); list.Add(Remap(state.objectRotation.w, -2, 2, 0, 1));
                }
                else
                {
                    //Head
                    list.Add(state.headPosition.x); list.Add(state.headPosition.y); list.Add(state.headPosition.z);
                    list.Add(state.headRotation.x); list.Add(state.headRotation.y); list.Add(state.headRotation.z); list.Add(state.headRotation.w);
                    //Left Hand
                    list.Add(state.leftHandPosition.x); list.Add(state.leftHandPosition.y); list.Add(state.leftHandPosition.z);
                    list.Add(state.leftHandRotation.x); list.Add(state.leftHandRotation.y); list.Add(state.leftHandRotation.z); list.Add(state.leftHandRotation.w);
                    //Right Hand
                    list.Add(state.rightHandPosition.x); list.Add(state.rightHandPosition.y); list.Add(state.rightHandPosition.z);
                    list.Add(state.rightHandRotation.x); list.Add(state.rightHandRotation.y); list.Add(state.rightHandRotation.z); list.Add(state.rightHandRotation.w);
                    //Pelvis
                    list.Add(state.skeletalPositions[1].x); list.Add(state.skeletalPositions[1].y); list.Add(state.skeletalPositions[1].z);
                    list.Add(state.skeletalRotations[1].x); list.Add(state.skeletalRotations[1].y); list.Add(state.skeletalRotations[1].z); list.Add(state.skeletalRotations[1].w);
                    //Object
                    list.Add(state.objectPosition.x); list.Add(state.objectPosition.y); list.Add(state.objectPosition.z);
                    list.Add(state.objectRotation.x); list.Add(state.objectRotation.y); list.Add(state.objectRotation.z); list.Add(state.objectRotation.w);
                }
            }
            else if (type == VectorType.FourJointPosRotTwoButtonSequence)
            {
                //JointData per frame is formatted as: HeadPosition, HeadRotation, LeftHandPosition, LeftHandRotation, RightHandPosition, RightHandRotation, PelvisPosition, PelvisRotation, LeftControllerGripButton, RightControllerGripButton

                frameLength = 30;
                if (reMap)
                {
                    //Head
                    list.Add(Remap(state.headPosition.x, -2, 2, 0, 1)); list.Add(Remap(state.headPosition.y, -2, 2, 0, 1)); list.Add(Remap(state.headPosition.z, -2, 2, 0, 1));
                    list.Add(Remap(state.headRotation.x, -2, 2, 0, 1)); list.Add(Remap(state.headRotation.y, -2, 2, 0, 1)); list.Add(Remap(state.headRotation.z, -2, 2, 0, 1)); list.Add(Remap(state.headRotation.w, -2, 2, 0, 1));
                    //Left Hand
                    list.Add(Remap(state.leftHandPosition.x, -2, 2, 0, 1)); list.Add(Remap(state.leftHandPosition.y, -2, 2, 0, 1)); list.Add(Remap(state.leftHandPosition.z, -2, 2, 0, 1));
                    list.Add(Remap(state.leftHandRotation.x, -2, 2, 0, 1)); list.Add(Remap(state.leftHandRotation.y, -2, 2, 0, 1)); list.Add(Remap(state.leftHandRotation.z, -2, 2, 0, 1)); list.Add(Remap(state.leftHandRotation.w, -2, 2, 0, 1));
                    //Right Hand
                    list.Add(Remap(state.rightHandPosition.x, -2, 2, 0, 1)); list.Add(Remap(state.rightHandPosition.y, -2, 2, 0, 1)); list.Add(Remap(state.rightHandPosition.z, -2, 2, 0, 1));
                    list.Add(Remap(state.rightHandRotation.x, -2, 2, 0, 1)); list.Add(Remap(state.rightHandRotation.y, -2, 2, 0, 1)); list.Add(Remap(state.rightHandRotation.z, -2, 2, 0, 1)); list.Add(Remap(state.rightHandRotation.w, -2, 2, 0, 1));
                    //Pelvis
                    list.Add(Remap(state.skeletalPositions[1].x, -2, 2, 0, 1)); list.Add(Remap(state.skeletalPositions[1].y, -2, 2, 0, 1)); list.Add(Remap(state.skeletalPositions[1].z, -2, 2, 0, 1));
                    list.Add(Remap(state.skeletalRotations[1].x, -2, 2, 0, 1)); list.Add(Remap(state.skeletalRotations[1].y, -2, 2, 0, 1)); list.Add(Remap(state.skeletalRotations[1].z, -2, 2, 0, 1)); list.Add(Remap(state.skeletalRotations[1].w, -2, 2, 0, 1));
                }
                else
                {
                    //Head
                    list.Add(state.headPosition.x); list.Add(state.headPosition.y); list.Add(state.headPosition.z);
                    list.Add(state.headRotation.x); list.Add(state.headRotation.y); list.Add(state.headRotation.z); list.Add(state.headRotation.w);
                    //Left Hand
                    list.Add(state.leftHandPosition.x); list.Add(state.leftHandPosition.y); list.Add(state.leftHandPosition.z);
                    list.Add(state.leftHandRotation.x); list.Add(state.leftHandRotation.y); list.Add(state.leftHandRotation.z); list.Add(state.leftHandRotation.w);
                    //Right Hand
                    list.Add(state.rightHandPosition.x); list.Add(state.rightHandPosition.y); list.Add(state.rightHandPosition.z);
                    list.Add(state.rightHandRotation.x); list.Add(state.rightHandRotation.y); list.Add(state.rightHandRotation.z); list.Add(state.rightHandRotation.w);
                    //Pelvis
                    list.Add(state.skeletalPositions[1].x); list.Add(state.skeletalPositions[1].y); list.Add(state.skeletalPositions[1].z);
                    list.Add(state.skeletalRotations[1].x); list.Add(state.skeletalRotations[1].y); list.Add(state.skeletalRotations[1].z); list.Add(state.skeletalRotations[1].w);
                }
                //Controller Buttons Pressed
                list.Add(leftControllerDown ? 1 : 0);
                list.Add(rightControllerDown ? 1 : 0);
            }
            else
            {
                return null;
            }
        }

        if (isPadded && length > gesture.playerStates.Count * frameLength)
        {
            int remainingLength = length - gesture.playerStates.Count * frameLength;

            for (int index = 0; index < remainingLength; index++)
            {
                list.Add(paddingValue);
            }
        }

        isVectorClean = false;
        return list;
    }

    private float Remap(float oldValue, float oldMin, float oldMax, float newMin, float newMax)
    {
        return (newMax - newMin) * (oldValue - oldMin) / (oldMax - oldMin) + newMin;
    }

    public float[] To1DFloat()
    {
        float[,] ndArray = vector;
        float[] array = new float[ndArray.GetLength(0)];
        int index = 0;
        foreach (float val in ndArray)
        {
            array[index++] = val;
        }

        return array;
    }
}