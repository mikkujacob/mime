﻿using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudienceSkeletonJSONInput : MonoBehaviour
{
    private OpenPoseInputsDefinition input;
    public string inputDirectory;
    public string filenameTemplate = "_keypoints.json";
    public int filenameNumericPrecision = 12;
    private int currentFrame;
    private bool isFinished = false;

    // Use this for initialization
    void Start()
    {
        currentFrame = 0;
    }

    // Update is called once per frame
    void Update()
    {
        try
        {
            HandleKeyboardInput();

            if(!isFinished)
            {
                string currentFrameString = currentFrame.ToString("D" + filenameNumericPrecision);
                Debug.Log("Frame: " + currentFrameString);
                string json = File.ReadAllText(Path.Combine(inputDirectory, currentFrameString + filenameTemplate));
                input = JsonUtility.FromJson<OpenPoseInputsDefinition>(json);
                Debug.Log(input.ToString());
                File.Delete(Path.Combine(inputDirectory, currentFrameString + filenameTemplate));
                currentFrame++;
            }
        }
        catch(Exception)
        {
            
        }
    }

    void HandleKeyboardInput()
    {
        if(Input.GetKeyDown(KeyCode.Escape))
        {
            isFinished = true;
            Debug.Log("Done reading files!");
        }
    }
}
