﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GestureSaverTester : MonoBehaviour {

	public GestureSaver gestureSaver;
	public bool clickToRun = false;

	// Use this for initialization
	void Start () {

	}

	// Update is called once per frame
	void Update () {
		if (clickToRun == true)
		{
			clickToRun = false;
			gestureSaver.StartRecording();

		}
	}
}
