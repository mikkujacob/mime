﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Linq;
using UnityEditor;
using System;
//using Microsoft.VisualBasic;

public class SceneLoader : MonoBehaviour {

	public bool useCustomTextFile = false;
	public string textFilePath;


	// Use this for initialization
	void Awake () {

		if (!useCustomTextFile)
		{
			UnityEngine.Object textfile = Resources.Load("SceneSaverTextFile");
			textFilePath = AssetDatabase.GetAssetPath(textfile);
		}
		string[] allLines = File.ReadAllLines(@textFilePath);

		//index,name,positionX,positionY,positionZ,rotationX,rotationY,rotationZ,rotationW,pre-fab path,trackingType

  	foreach (string line in allLines) {
		//string line = allLines[0];
  		string[] data = line.Split(',');
			/*for (int i = 0; i < data.Length; i++) {
				Debug.Log(data[i]);
			}*/

			string name = data[1];

			float positionX  = float.Parse(data[2]);
			float positionY  = float.Parse(data[3]);
			float positionZ  = float.Parse(data[4]);

			Vector3 position = new Vector3 (positionX, positionY, positionZ);
			Debug.Log("[position]: " + position);

			float rotationX = float.Parse(data[5]);
			float rotationY = float.Parse(data[6]);
			float rotationZ = float.Parse(data[7]);
			float rotationW = float.Parse(data[8]);

			Quaternion rotation = new Quaternion (rotationX, rotationY, rotationZ, rotationW);
			Debug.Log("[rotation]: " + rotation);


			float scaleX  = float.Parse(data[9]);
			float scaleY  = float.Parse(data[10]);
			float scaleZ  = float.Parse(data[11]);

			Vector3 scale = new Vector3 (scaleX, scaleY, scaleZ);
			Debug.Log("[Scale]: " + scale);

			string prefabPath = data[12];

			TrackingType trackingType = (TrackingType) Enum.Parse(typeof(TrackingType), data[13]);

			//Object prefab = Resources.Load("PreFabs/TheInfoTool");
			UnityEngine.Object prefab = Resources.Load(prefabPath);

			GameObject newObject = (GameObject) GameObject.Instantiate(prefab, position, rotation);
			newObject.transform.localScale = scale;
			newObject.name = name;

			if (trackingType == TrackingType.Object)
			{
				//GameObject.Destroy(newObject.GetComponent<ObjectStateTracker>());
				newObject.tag = "ObjectStateTracker";
			}





		}


	}
	enum TrackingType
	{
		Object,
		Kinect,
		None
	}
}
