﻿using UnityEngine;

namespace Crosstales.RTVoice.Util
{
    /// <summary>Various helper functions.</summary>
    public abstract class Helper : Common.Util.BaseHelper
    {
        #region Variables

        private static readonly string[] appleFemales =
        {
            "Alice",
            "Alva",
            "Amelie",
            "Anna",
            "Carmit",
            "Catherine",
            "Damayanti",
            "Ellen",
            "Fiona",
            "Helena",
            "Ioana",
            "Joana",
            "Kanya",
            "Karen",
            "Kyoko",
            "Laura",
            "Lekha",
            "Li-mu",
            "Luciana",
            "Marie",
            "Mariska",
            "Martha",
            "Mei-Jia",
            "Melina",
            "Milena",
            "Moira",
            "Monica",
            "Nicky",
            "Nora",
            "O-ren",
            "Paulina",
            "Samantha",
            "Sara",
            "Satu",
            "Sin-ji",
            "Tessa",
            "Ting-Ting",
            "Veena",
            "Victoria",
            "Yelda",
            "Yu-shu",
            "Yuna",
            "Zosia",
            "Zuzana"
        };

        private static readonly string[] appleMales =
        {
            "Aaron",
            "Alex",
            "Arthur",
            "Daniel",
            "Diego",
            "Fred",
            "Gordon",
            "Hattori",
            "Jorge",
            "Juan",
            "Luca",
            "Maged",
            "Martin",
            "Thomas",
            "Xander",
            "Yuri"
        };

        private static readonly string[] wsaFemales =
        {
            "Ayumi",
            "Haruka",
            "Caroline",
            "Catherine",
            "Elsa",
            "Hazel",
            "Susan",
            "Heami",
            "Hedda",
            "Katja",
            "Heera",
            "Heidi",
            "Helena",
            "Laura",
            "Helia",
            "Helle",
            "Herena",
            "Hoda",
            "Hortence",
            "Julie",
            "Huihui",
            "Yaoyao",
            "Irina",
            "Kalpana",
            "Linda",
            "Maria",
            "Paulina",
            "Sabina",
            "Tracy",
            "Yating",
            "Hanhan",
            "Zira"
        };

        private static readonly string[] wsaMales =
        {
            "Adam",
            "An",
            "Andika",
            "Andrei",
            "Asaf",
            "Bart",
            "Bengt",
            "Claude",
            "Cosimo",
            "Daniel",
            "Danny",
            "David",
            "Mark",
            "Filip",
            "Frank",
            "George",
            "Hemant",
            "Ichiro",
            "Ivan",
            "James",
            "Jon",
            "Kangkang",
            "Karsten",
            "Lado",
            "Matej",
            "Naayf",
            "Pablo",
            "Pattara",
            "Paul",
            "Pavel",
            "Raul",
            "Ravi",
            "Richard",
            "Rizwan",
            "Shaun",
            "Stefan",
            "Stefanos",
            "Szabolcs",
            "Tolga",
            "Valluvar",
            "Vit",
            "Zhiwei"
        };

        #endregion


        #region Static properties

        /// <summary>Checks if the current platform has built-in TTS.</summary>
        /// <returns>True if the current platform has built-in TTS.</returns>
        public static bool hasBuiltInTTS
        {
            get
            {
                return isWindowsPlatform || isMacOSPlatform || isAndroidPlatform || isIOSPlatform || isWSAPlatform || isLinuxPlatform;
            }
        }

        /// <summary>The current provider type.</summary>
        /// <returns>Current provider type.</returns>
        public static Model.Enum.ProviderType CurrentProviderType
        {
            get
            {
                if (Speaker.isMaryMode)
                    return Model.Enum.ProviderType.MaryTTS;

                if (isWindowsPlatform && !Speaker.isESpeakMode)
                    return Model.Enum.ProviderType.Windows;

                if (isAndroidPlatform)
                    return Model.Enum.ProviderType.Android;

                if (isIOSPlatform)
                    return Model.Enum.ProviderType.iOS;

                if (isWSAPlatform)
                    return Model.Enum.ProviderType.WSA;

                if (isMacOSPlatform && !Speaker.isESpeakMode)
                    return Model.Enum.ProviderType.macOS;

                return Model.Enum.ProviderType.Linux;
            }
        }

        #endregion


        #region Static methods

        /// <summary>Determines if an AudioSource has an active clip.</summary>
        /// <param name="source">AudioSource to check.</param>
        /// <returns>True if the AudioSource has an active clip.</returns>
        public static bool hasActiveClip(AudioSource source) //TODO move to Common
        {
            /*
            Debug.Log("source.clip: " + source.clip);
            Debug.Log("source.loop: " + source.loop);
            Debug.Log("source.isPlaying: " + source.isPlaying);
            Debug.Log("source.timeSamples: " + source.timeSamples);
            Debug.Log("source.clip.samples: " + source.clip.samples);
            */

            return source != null && source.clip != null &&
                ((!source.loop && source.timeSamples > 0 && source.timeSamples < source.clip.samples - 256) ||
                source.loop ||
                source.isPlaying);
        }

        /// <summary>Converts a string to a Gender.</summary>
        /// <param name="gender">Gender as text.</param>
        /// <returns>Gender from the given string.</returns>
        public static Model.Enum.Gender StringToGender(string gender)
        {

            if ("male".CTEquals(gender) || "m".CTEquals(gender))
            {
                return Model.Enum.Gender.MALE;
            }

            if ("female".CTEquals(gender) || "f".CTEquals(gender))
            {
                return Model.Enum.Gender.FEMALE;
            }

            return Model.Enum.Gender.UNKNOWN;
        }

        /// <summary>Converts an Apple voice name to a Gender.</summary>
        /// <param name="voiceName">Voice name.</param>
        /// <returns>Gender from the given Apple voice name.</returns>
        public static Model.Enum.Gender AppleVoiceNameToGender(string voiceName)
        {
            if (!string.IsNullOrEmpty(voiceName))
            {
                foreach (string female in appleFemales)
                {
                    if (voiceName.CTContains(female))
                    {
                        return Model.Enum.Gender.FEMALE;
                    }
                }

                foreach (string male in appleMales)
                {
                    if (voiceName.CTContains(male))
                    {
                        return Model.Enum.Gender.MALE;
                    }
                }
            }

            return Model.Enum.Gender.UNKNOWN;
        }

        /// <summary>Converts an WSA voice name to a Gender.</summary>
        /// <param name="voiceName">Voice name.</param>
        /// <returns>Gender from the given WSA voice name.</returns>
        public static Model.Enum.Gender WSAVoiceNameToGender(string voiceName)
        {
            if (!string.IsNullOrEmpty(voiceName))
            {
                foreach (string female in wsaFemales)
                {
                    if (voiceName.CTContains(female))
                    {
                        return Model.Enum.Gender.FEMALE;
                    }
                }

                foreach (string male in wsaMales)
                {
                    if (voiceName.CTContains(male))
                    {
                        return Model.Enum.Gender.MALE;
                    }
                }
            }

            return Model.Enum.Gender.UNKNOWN;
        }

        /// <summary>Cleans a given text to contain only letters or digits.</summary>
        /// <param name="text">Text to clean.</param>
        /// <param name="removeTags">Removes tags from text (default: true, optional).</param>
        /// <param name="clearSpaces">Clears multiple spaces from text (default: true, optional).</param>
        /// <param name="clearLineEndings">Clears line endings from text (default: true, optional).</param>
        /// <returns>Clean text with only letters and digits.</returns>
        public static string CleanText(string text, bool removeTags = true, bool clearSpaces = true, bool clearLineEndings = true)
        {
            string result = text;

            if (removeTags)
            {
                result = ClearTags(result);
            }

            if (clearSpaces)
            {
                result = ClearSpaces(result);
            }

            if (clearLineEndings)
            {
                result = ClearLineEndings(result);
            }

            //Debug.Log (result);

            return result;
        }

        /// <summary>Marks the current word or all spoken words from a given text array.</summary>
        /// <param name="speechTextArray">Array with all text fragments</param>
        /// <param name="wordIndex">Current word index</param>
        /// <param name="markAllSpokenWords">Mark the spoken words (default: false, optional)</param>
        /// <param name="markPrefix">Prefix for every marked word (default: green, optional)</param>
        /// <param name="markPostfix">Postfix for every marked word (default: green, optional)</param>
        /// <returns>Marked current word or all spoken words.</returns>
        public static string MarkSpokenText(string[] speechTextArray, int wordIndex, bool markAllSpokenWords = false, string markPrefix = "<color=green><b>", string markPostfix = "</b></color>")
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder();

            if (speechTextArray == null)
            {
                Debug.LogWarning("The given 'speechTextArray' is null!");
            }
            else
            {
                if (wordIndex < 0 || wordIndex > speechTextArray.Length - 1)
                {
                    Debug.LogWarning("The given 'wordIndex' is invalid: " + wordIndex);
                }
                else
                {
                    for (int ii = 0; ii < wordIndex; ii++)
                    {

                        if (markAllSpokenWords)
                            sb.Append(markPrefix);
                        sb.Append(speechTextArray[ii]);
                        if (markAllSpokenWords)
                            sb.Append(markPostfix);
                        sb.Append(" ");
                    }

                    sb.Append(markPrefix);
                    sb.Append(speechTextArray[wordIndex]);
                    sb.Append(markPostfix);
                    sb.Append(" ");

                    for (int ii = wordIndex + 1; ii < speechTextArray.Length; ii++)
                    {
                        sb.Append(speechTextArray[ii]);
                        sb.Append(" ");
                    }
                }
            }

            return sb.ToString();
        }

        #endregion

    }
}
// © 2015-2018 crosstales LLC (https://www.crosstales.com)